/**
 *
 *       Copyright 2019 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */
extern "C" {
#include <craton/user.h>
} /* extern "C" */

#include "testlib/test.h"

int main(int argc, char *argv[])
{
    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);
    return 0;
}
