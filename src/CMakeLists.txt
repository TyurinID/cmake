#
#
#       Copyright 2019 Sreda Software Solutions. All rights reserved.
#
#       The copyright notice above does not evidence any actual or
#       intended publication of such source code. The code contains
#       Sreda Software Solutions Confidential Proprietary Information.
#
#


include_directories(
        ${PROJECT_SOURCE_DIR}/include
        ${PROJECT_SOURCE_DIR}/src
        ${PROJECT_SOURCE_DIR}/test
)

add_definitions(${LOG_OPTIONS})

if(ADA_PLATFORM STREQUAL "posix")
    set(LINK_LIB_PTHREAD pthread)
endif()

if(ADA_PLATFORM STREQUAL "unex")
endif()

########################### Standart Abstract Layer Library ##########################

if (ADA_PLATFORM STREQUAL "posix")
set(SOURCE_LIB_MUTEX_STD_C common/std/ada_mutex.cpp)
set(SOURCE_LIB_TIME_STD_C common/std/ada_time.cpp)
set(SOURCE_LIB_COND_STD_C common/std/ada_cond_var.cpp)
set(SOURCE_LIB_TASK_STD_C common/std/ada_task.cpp)
set(SOURCE_LIB_TIMER_STD_C common/std/ada_timer.cpp)
set(LINK_LIB_PTHREAD pthread)
endif (ADA_PLATFORM STREQUAL "posix")

if (ADA_PLATFORM STREQUAL "unex")
set(SOURCE_LIB_MUTEX_STD_C common/std/ada_mutex_unex.cpp)
set(SOURCE_LIB_TIME_STD_C common/std/ada_time_unex.cpp)
set(SOURCE_LIB_COND_STD_C common/std/ada_cond_var_unex.cpp)
set(SOURCE_LIB_TASK_STD_C common/std/ada_task_unex.cpp)
set(SOURCE_LIB_TIMER_STD_C common/std/ada_timer_unex.cpp)
endif (ADA_PLATFORM STREQUAL "unex")

set(SOURCE_LIB_STD_C
    common/std/ada_dynamic_queue.cpp
    common/std/ada_sleep.cpp
    common/ada_common.cpp
    utils/utils.cpp
    ${SOURCE_LIB_MUTEX_STD_C}
    ${SOURCE_LIB_TIME_STD_C}
    ${SOURCE_LIB_TIMER_STD_C}
    ${SOURCE_LIB_COND_STD_C}
    ${SOURCE_LIB_TASK_STD_C})
add_library(ada_std STATIC ${SOURCE_LIB_STD_C})
target_link_libraries(ada_std log ${LINK_LIB_PTHREAD})

########################### Standart Abstract Layer Library ##########################/

########################### Log and Error Library ####################################
add_library(log STATIC utils/log/log.c)
target_include_directories(log INTERFACE
    ${PROJECT_SOURCE_DIR}/src/utils/log
    ${PROJECT_SOURCE_DIR}/src/utils)
target_link_libraries(log)
########################### Log and Error Library ####################################/



add_library(testl STATIC testlib/test.cpp)
target_link_libraries(testl gcov)

if(ADA_PLATFORM STREQUAL "unex")
    include("CMakeLists.unex.cmake.in")
else()
    include("CMakeLists.linux.cmake.in")
endif()

