/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/*
 * @file    ada_common.h
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Common types
 *
 */

#include <ada_common.h>

AdaMapTurn AdaSignalsToMapTurn(AdaInVehSignals signals)
{
    switch (signals.turnLights)
    {
        case AdaInVehTurnSignals::LEFT:
        {
            return AdaMapTurn::ADA_MAP_TURN_LEFT;
        }

        case AdaInVehTurnSignals::RIGHT:
        {
            return AdaMapTurn::ADA_MAP_TURN_RIGHT;
        }

        default:
        case AdaInVehTurnSignals::OFF:
        case AdaInVehTurnSignals::ALARM:
        {
            return AdaMapTurn::ADA_MAP_TURN_FORWARD;
        }
    }
}
