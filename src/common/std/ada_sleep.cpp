/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_sleep.cpp
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Sleep class
 *
 *
 */

#include <std/ada_sleep.h>
#include <time.h>
#include <assert.h>
#include <utils/utils.h>

#include <utils/log/log_modules.h>
#define LOG_MODULE_ID LOG_MOD_STD
#define LOG_MODULE_LEVEL LOG_DEF_LEVEL_STD
#include <utils/log/log_api.h>

#ifdef __CRATON__
#include <tx_api.h>
#include <climits>
#endif

#define SECOND_IN_USECONDS  (1000000)

AdaSleep::AdaSleep()
{

}

void AdaSleep::uSleep(uint64_t uSec)
{
    /* On UNEX nanosleep() will fail if called with more than 1s interval.
     * Use UNEX native sleep function */
#ifdef __CRATON__
    UINT rc;
    uint64_t ticks;

    ticks = (uSec * TX_TICK_RATE) / MICRO_PER_UNIT;

    while (ticks >= ULONG_MAX)
    {
        rc = tx_thread_sleep(ULONG_MAX);
        ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);
        ticks -= ULONG_MAX;
    }

    rc = tx_thread_sleep(static_cast<ULONG>(ticks));
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);
#else

    struct timespec ts;

    ts.tv_sec = uSec / 1000000;
    ts.tv_nsec = (uSec % 1000000) * 1000;
    ADA_ASSERT_ALWAYS(0 == nanosleep(&ts, NULL));
#endif
}
