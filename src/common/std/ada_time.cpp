/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_time.cpp
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Time class
 *
 *  Allows geting current time in milliseconds from 1970 January 1st
 *
 */

#include <sys/time.h>
#include <time.h>

#include <assert.h>
#include <utils/utils.h>
#include <std/ada_time.h>
#include <cstdio>
#include <inttypes.h>

#define SIX_MONTHS_MINUTES  (259200)

AdaTime::AdaTime()
{

}

void AdaTime::timestampBigEndian(uint8_t *pTimestamp)
{
    if (pTimestamp == NULL)
    {
        return;
    }

    struct timespec spec;
    assert(0 == clock_gettime(CLOCK_REALTIME, &spec));
    uint32_t timeLow = static_cast<uint32_t>((spec.tv_sec * 1000 + spec.tv_nsec / 1000000));
    uint32_t timeHigh = static_cast<uint32_t>((spec.tv_sec * 1000 + spec.tv_nsec / 1000000) >> 32);
    uint32BigEncode(timeHigh, pTimestamp);
    uint32BigEncode(timeLow, &pTimestamp[4]);
}

void AdaTime::getTime(AdaTimeStruct *pTime_)
{
    struct timespec spec;
    assert(0 == clock_gettime(CLOCK_REALTIME, &spec));
    pTime_->min = static_cast<uint32_t>(spec.tv_sec / 60);
    pTime_->ms = static_cast<uint32_t>(spec.tv_nsec / 1000000);
}

uint64_t AdaTime::getMs()
{
    struct timespec spec;
    assert(0 == clock_gettime(CLOCK_REALTIME, &spec));
    return ( static_cast<uint64_t>(spec.tv_sec) * 1000 +  static_cast<uint64_t>(spec.tv_nsec) /
             1000000 );
}

uint32_t AdaTime::getS()
{
    struct timespec spec;
    assert(0 == clock_gettime(CLOCK_REALTIME, &spec));
    return ( static_cast<uint32_t>(spec.tv_sec) );
}

uint64_t AdaTime::getSteadyMs()
{
    struct timespec spec;
    assert(0 == clock_gettime(CLOCK_MONOTONIC, &spec));
    return ( static_cast<uint64_t>(spec.tv_sec) * 1000 +  static_cast<uint64_t>(spec.tv_nsec) /
             1000000 );
}

/**
 * Converts time from minutes of the year + milliseconds in current minute to
 * milliseconds since Epoch
 * @param[in]    moy        minutes of current year
 * @param[in]    ms         milliseconds in current minute
 * @return      milliseconds since Epoch
 */
uint64_t AdaTime::convertToMs(int32_t moy, uint16_t ms)
{
    time_t seconds = time(NULL);
    assert(seconds != -1);
    struct tm timeCurrent = {};
    assert(NULL != gmtime_r(&seconds, &timeCurrent));

    if (timeCurrent.tm_mon == 0 && moy > SIX_MONTHS_MINUTES)
    {
        timeCurrent.tm_year = timeCurrent.tm_year - 1;
    }
    else
    {
        timeCurrent.tm_year = timeCurrent.tm_year;
    }

    timeCurrent.tm_min = moy;
    timeCurrent.tm_isdst = 0;
    timeCurrent.tm_hour = 0;
    timeCurrent.tm_mon = 0;
    timeCurrent.tm_sec = 0;
    timeCurrent.tm_mday = 1;
    seconds = mktime(&timeCurrent);
    assert(seconds != -1);
    return (uint64_t(seconds + timeCurrent.tm_gmtoff) * 1000 + uint64_t(ms));
}

#define MS_IN_HOUR  (3600000)

/**
 * Converts time from tenths seconds in current or next hour
 * (according to timestampMs) to milliseconds since Epoch
 * @param[in]   tenthsSecInHour     tenths seconds in current or next hour
 * @param[in]   timestampMs
 * @return      milliseconds since Epoch
 */
uint64_t AdaTime::convertToMs(uint16_t tenthsSecInHour, uint64_t timestampMs)
{
    time_t seconds = time_t(timestampMs / 1000);
    struct tm timeCurrent = {};
    assert(NULL != gmtime_r(&seconds, &timeCurrent));
    uint32_t tenthsSecondsCurrent = uint32_t(timeCurrent.tm_min * 600 + timeCurrent.tm_sec * 10);

    if (tenthsSecondsCurrent > tenthsSecInHour)
    {
        timestampMs += MS_IN_HOUR;
    }

    return (timestampMs / 1000) * 1000 - uint64_t(tenthsSecondsCurrent) * 100 + uint64_t(
        tenthsSecInHour) * 100;
}

/*
 * Print current time in format [HH:MM:SS:ms] without \n
 */
void AdaTime::printMs()
{
    uint64_t timeMs = getMs();
    uint16_t ms = uint16_t(timeMs % 1000);
    time_t time = time_t(timeMs / 1000);
    struct tm timeCurrent = {};
    assert(NULL != gmtime_r(&time, &timeCurrent));
    fprintf(stderr, "[%02" PRIi32 ":%02" PRIi32 ":%02" PRIi32 ".%03" PRIu16 "]",
            timeCurrent.tm_hour, timeCurrent.tm_min, timeCurrent.tm_sec, ms);
}

void AdaTime::sprintMs(char *pBuf, uint32_t len, uint64_t timestampMs)
{
    uint64_t timeMs = (timestampMs == 0) ? getMs() : timestampMs;
    uint16_t ms = uint16_t(timeMs % 1000);
    time_t time = time_t(timeMs / 1000);
    struct tm timeCurrent = {};

    gmtime_r(&time, &timeCurrent);
    snprintf(pBuf, len, "[%02" PRIi32 ":%02" PRIi32 ":%02" PRIi32 ".%03" PRIu16 "]",
             timeCurrent.tm_hour, timeCurrent.tm_min, timeCurrent.tm_sec, ms);
}

/*
 * Print current time in format [HH:MM:SS:ms] with \n
 */
void inline AdaTime::printLnMs()
{
    printMs();
    fprintf(stderr, "\n");
}

uint64_t AdaTime::getUs()
{
    struct timespec spec;
    assert(0 == clock_gettime(CLOCK_REALTIME, &spec));
    return ( static_cast<uint64_t>(spec.tv_sec) * 1000000 +  static_cast<uint64_t>(spec.tv_nsec) /
             1000 );
}
