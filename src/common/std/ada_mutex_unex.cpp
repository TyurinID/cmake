/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_mutex.cpp
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Mutex class implementation
 *
 *
 */
#include <std/ada_mutex.h>

#include <tx_posix.h>

#include <assert.h>

class AdaMutexPrivate
{
public:
    explicit AdaMutexPrivate()
    {
    }
    TX_MUTEX mutex;
};


AdaMutex::AdaMutex() : mPriv(NULL)
{
    mPriv = new AdaMutexPrivate();
    assert(0 == tx_mutex_create(&mPriv->mutex, (char*)"mutex", TX_INHERIT));
}

AdaMutex::~AdaMutex()
{
    tx_mutex_delete(&mPriv->mutex);
    delete mPriv;
}

void AdaMutex::lock()
{
    assert(0 == tx_mutex_get(&mPriv->mutex, TX_WAIT_FOREVER));
}

void AdaMutex::unlock()
{
    assert(0 == tx_mutex_put(&mPriv->mutex));
}
