/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ADA_fs.cpp
 * @author  Pavel Matyunin <pavel.matyunin@sredasolutions.com>
 *
 * @brief   FileIO API Implementation
 *
 *
 */

#include <std/ada_fs.h>
#include <std/ada_mutex.h>
#include <utils/ada_error.h>

#include <math.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/stat.h>

#include <utils/utils.h>

#ifdef __CRATON__
#include <unex/craton_fs.h>
#include <tx_api.h>
#include <tx_posix.h>
#endif

/**
 * Elementary constructor to initialize an object.
 */
AdaFs::AdaFs()
{
    _fileId          = 0;
    _status          = 0;
    _filePos         = 0;
    _fragmented      = 0;
    _pFd             = NULL;
    _pMode           = NULL;
    _pFileActualName = NULL;
    _pFileActualPath = NULL;

    return;
}

/*
 * Function to check ability to read from and/or write to the exact file
 * @param   forceOpen            - ignore fragmented operations (default = 0)
 * @return  ADA_SUCCESS
 * @return  ADA_ERR_NO_MEMORY    - RAM memory (possibly) exceeded
 * @return  ADA_ERR_FS_IS_A_DIR  - target file is an existing directory
 * @return  ADA_ERR_FS_NO FILE   - target file doesn't exist (for read mode only)
 * @return  ADA_ERR_FS_MAX_FILES - no space left on device (real space or any other FS restrictions)
 * @return  ADA_ERR_FS_GENERIC   - any other not specified error (no access, etc)
 */
AdaError AdaFs::safeOpen(uint8_t forceOpen)
{
    AdaError res;
    char *pTrueName = NULL;
    size_t trueNameLength = _fileActualNameLength + 1;

    pTrueName = (char*)calloc(trueNameLength, sizeof(char));

    if (!pTrueName)
    {
        return ADA_ERR_NO_MEMORY;
    }

    snprintf(pTrueName, trueNameLength + 1, "%s", _pFileActualName);

    struct stat status;

    if (stat(pTrueName, &status) != -1)
    {
        if (status.st_mode & S_IFDIR)
        {
            free(pTrueName);
            return ADA_ERR_FS_IS_A_DIR;
        }
    }

    _pFd = fopen(pTrueName, "r+");

    if (!_pFd)
    {
        if (errno == ENOENT)
        {
            if (strpbrk(_pMode, ADA_FS_MODE_READ))
            {
                free(pTrueName);
                return ADA_ERR_FS_NO_FILE;
            }
        }
        else if (errno == EISDIR)
        {
            free(pTrueName);
            return ADA_ERR_FS_IS_A_DIR;
        }
        else
        {
            free(pTrueName);
            return ADA_ERR_FS_GENERIC;
        }
    }
    else
    {
        fclose(_pFd);

        if (((!strpbrk(_pMode, ADA_FS_MODE_READ)) || (strpbrk(_pMode, "+"))) && (forceOpen == 0))
        {
            free(pTrueName);
            res = findLast();

            if (res != ADA_SUCCESS)
            {
                return res;
            }

            trueNameLength += 1 + ADA_FS_FILENAME_TAILMASK_SIZE;
            pTrueName = (char*)calloc(trueNameLength, sizeof(char));

            if (!pTrueName)
            {
                return ADA_ERR_NO_MEMORY;
            }

            snprintf(pTrueName, trueNameLength + 1, "%s.%0*d", _pFileActualName,
                     ADA_FS_FILENAME_TAILMASK_SIZE, _fileId);
        }
    }

    _pFd = fopen(pTrueName, _pMode);

    free(pTrueName);

    if (!_pFd)
    {
        res = (errno == ENOSPC) ? ADA_ERR_FS_MAX_FILES : ADA_ERR_FS_GENERIC;
        return res;
    }

    _filePos = 0;
    return ADA_SUCCESS;
}

/*
 * Function to find the last file fragment in current directory
 * @return  ADA_SUCCESS
 * @return  ADA_ERR_FS_MAX_FILES - maximum amount of files for current mask exceeded
 * @return  ADA_ERR_FS_GENERIC   - any other not specified error (no access, etc)
 */
AdaError AdaFs::findLast()
{
    DIR *pDir = NULL;

    size_t nameLength = 0;
    uint8_t isFragment = 0;
    uint16_t fragmentNum = 0;
    struct dirent *pDirFileName = NULL;

    pDir = opendir(".");

    if (!pDir)
    {
        return ADA_ERR_FS_GENERIC;
    }

    while ((pDirFileName = readdir(pDir)))
    {
        if (!strstr(pDirFileName->d_name, _pFileActualName))
        {
            continue;
        }

        nameLength = strlen(pDirFileName->d_name);

        if (nameLength <= ADA_FS_FILENAME_TAILMASK_SIZE + 1)
        {
            continue;
        }

        isFragment = pDirFileName->d_name[nameLength - ADA_FS_FILENAME_TAILMASK_SIZE - 1] == '.';

        if (!isFragment)
        {
            continue;
        }

        fragmentNum =
            (uint16_t)atoi(&pDirFileName->d_name[nameLength - ADA_FS_FILENAME_TAILMASK_SIZE]);

        if (fragmentNum >= _fileId)
        {
            _fileId = (uint16_t)(fragmentNum + 1);

            if (_fileId > pow(10, ADA_FS_FILENAME_TAILMASK_SIZE))
            {
                return ADA_ERR_FS_MAX_FILES;
            }
        }
    }

    closedir(pDir);
    return ADA_SUCCESS;
}

/**
 * @brief Function to open one specific file
 * @note file modes (1) - ADA_FS_MODE_READ(or/and)WRITE - read or write in plaintext mode
 * @note file modes (2) - (one of mode in (1)) (space) ADA_FS_MODE_BINARY - in binary mode
 * @param   pFilePath                - path to the file
 * @param   pFileName                - file name
 * @param   pFileMode                - file mode
 * @param   fragmenting              - split writing data to fixed-size files
 * @param   forceOpen                - ignore fragmented operations (default = 0)
 * @return  ADA_SUCCESS
 * @return  ADA_ERR_FS_OBJECT_EXISTS - object has been initialized and it needs to be closed first
 * @return  ADA_ERR_NO_MEMORY        - RAM memory (possibly) exceeded
 * @return  ADA_ERR_FS_GENERIC       - any other not specified error (no access, etc)
 * @return  ADA_ERR_FS_MAX_FILES     - maximum amount of files for current mask exceeded
 * @return  ADA_ERR_INVALID_PARAM    - incorrect parameters
 */
AdaError AdaFs::open(const char *pFilePath,
                     const char *pFileName,
                     const char *pFileMode,
                     uint8_t fragmenting,
                     uint8_t forceOpen)
{
    _mutex.lock();

    if (_status == 1)
    {
        _mutex.unlock();
        return ADA_ERR_FS_OBJECT_EXISTS;
    }

    if ((!pFilePath) || (!pFileName) || (!pFileMode))
    {
        _mutex.unlock();
        return ADA_ERR_INVALID_PARAM;
    }

    if ((pFileName[0] == '\0') || (pFileMode[0] == '\0'))
    {
        _mutex.unlock();
        return ADA_ERR_INVALID_PARAM;
    }

    uint32_t j = 0;
    mode_t folderRights;
    char dirpath[255];
    char *pCwdPath = getcwd(dirpath, sizeof(dirpath));
    char *pOriginPath = strdup(pCwdPath);
    AdaError res;
    uint16_t isClosedPath = 0;
    int status;

    _fragmented            = fragmenting;
    _fileModeLength        = (uint8_t)strlen(pFileMode);
    _fileActualPathLength  = strlen(pFilePath);
    _fileActualNameLength  =
        (uint16_t)(strlen(pFileName) + 1 + ADA_FS_FILENAME_TAILMASK_SIZE);
    _pMode           = strdup(pFileMode);

    if (_fileActualPathLength > 0)
    {
        _pFileActualPath = strdup(pFilePath);
        isClosedPath = (pFilePath[_fileActualPathLength - 1] == '/');
    }

    _pFileActualName = (char*)calloc(_fileActualNameLength + 1, sizeof(char));
    snprintf(_pFileActualName, _fileActualNameLength, "%s", pFileName);

    if ((!_pMode) || ((!_pFileActualPath) && (_fileActualPathLength > 0)) || (!_pFileActualName))
    {
        destroyAllocs();
        _mutex.unlock();
        return ADA_ERR_NO_MEMORY;
    }

    if (isClosedPath)
    {
        _pFileActualPath[_fileActualPathLength - 1] = 0;
        _fileActualPathLength = _fileActualPathLength - isClosedPath;
    }

#ifdef __CRATON__

    /*
     * Because "X:/" length = 3
     * Logic - MicroSD - by default, Internal Flash - when we need it.
     */
    if (_fileActualPathLength >= 3)
    {
        chdir(memcmp(pFilePath, ADA_FS_INTERNAL_ROOT, 3) == 0 ?
              ADA_FS_INTERNAL_ROOT :
              ADA_FS_MICROSD_ROOT);
    }
    else
    {
        chdir(ADA_FS_MICROSD_ROOT);
    }

    folderRights = 0;
#else

    if (_fileActualPathLength > 0)
    {
        if (pFilePath[0] == '/')
        {
            status = chdir("/");
            UNUSED_VARIABLE(status);
        }
    }

    folderRights = S_IRWXU | S_IRWXG | S_IRWXO;
#endif

    if (_fileActualPathLength > 0)
    {
        uint32_t i = 1;

#ifdef __CRATON__

        if (_fileActualPathLength >= 3)
        {
            i = (memcmp(&pFilePath[1], ":/", 2) == 0) ? 3 : 1;
        }

#else

        if (_fileActualPathLength >= 2)
        {
            i = (memcmp(&pFilePath, "./", 2) == 0) ? 2 : 1;
        }

#endif

        for (; i < (uint32_t)_fileActualPathLength - 1; i++)
        {
            if (_pFileActualPath[i] == '/')
            {
                _pFileActualPath[i] = '\0';

                if (!strpbrk(_pMode, ADA_FS_MODE_READ))
                {
                    mkdir(&_pFileActualPath[j], folderRights);
                }

                status = chdir(&_pFileActualPath[j]);
                UNUSED_VARIABLE(status);
                _pFileActualPath[i] = '/';
                j = i + 1;
            }
        }

        if (!strpbrk(_pMode, ADA_FS_MODE_READ))
        {
            mkdir(&_pFileActualPath[j], folderRights);
        }

        status = chdir(&_pFileActualPath[j]);
        UNUSED_VARIABLE(status);
    }

    res = safeOpen(forceOpen);
    status = chdir(pOriginPath);
    UNUSED_VARIABLE(status);
    free(pOriginPath);

    if (res != ADA_SUCCESS)
    {
        destroyAllocs();
        _mutex.unlock();
        return res;
    }

    _status   =   1;
    _mutex.unlock();
    return ADA_SUCCESS;
}

/*
 * Function to close existing file and start a new one
 * @return see safeOpen()
 */
AdaError AdaFs::next()
{
    AdaError res;

    if (_pFd)
    {
        fclose(_pFd);
        _pFd = NULL;
    }

    uint16_t strOffset = (uint16_t)(_fileActualNameLength - ADA_FS_FILENAME_TAILMASK_SIZE);
    _fileId++;

    snprintf(&_pFileActualName[strOffset], ADA_FS_FILENAME_TAILMASK_SIZE + 1,
             "%0*d", ADA_FS_FILENAME_TAILMASK_SIZE, _fileId);

    uint32_t j = 0;
    char dirpath[255];
    char *pOriginPath = NULL;
    int status;

    pOriginPath = getcwd(dirpath, sizeof(dirpath));

#ifdef __CRATON__

    if (_fileActualPathLength >= 3)
    {
        chdir(memcmp(_pFileActualPath, ADA_FS_INTERNAL_ROOT, 3) == 0 ?
              ADA_FS_INTERNAL_ROOT :
              ADA_FS_MICROSD_ROOT);
    }
    else
    {
        chdir(ADA_FS_MICROSD_ROOT);
    }

#endif

    if (_fileActualPathLength > 0)
    {
        uint32_t i = 1;

#ifdef __CRATON__

        if (_fileActualPathLength >= 3)
        {
            i = (memcmp(&_pFileActualPath[1], ":/", 2) == 0) ? 3 : 1;
        }

#else

        if (_fileActualPathLength >= 2)
        {
            i = (memcmp(&_pFileActualPath, "./", 2) == 0) ? 2 : 1;
        }

#endif

        for (; i < (uint32_t)_fileActualPathLength - 1; i++)
        {
            if (_pFileActualPath[i] == '/')
            {
                _pFileActualPath[i] = '\0';
                status = chdir(&_pFileActualPath[j]);
                UNUSED_VARIABLE(status);
                _pFileActualPath[i] = '/';
                j = i + 1;
            }
        }

        status = chdir(&_pFileActualPath[j]);
        UNUSED_VARIABLE(status);
    }

    res = safeOpen();

    status = chdir(pOriginPath);
    UNUSED_VARIABLE(status);
    return res;
}

/**
 * @brief Function to sync IO on device
 * @return  ADA_SUCCESS
 * @return  ADA_ERR_FS_IO             - IO-operation error (storage memory exceeded)
 * @return  ADA_ERR_FS_GENERIC        - any other not specified error (no access, etc)
 */
AdaError AdaFs::sync()
{
    AdaError res;
    int32_t rc = fsync(fileno(_pFd));

    if (rc == 0)
    {
        res = ADA_SUCCESS;
    }
    else if (errno == ENOSPC)
    {
        res = ADA_ERR_FS_IO;
    }
    else
    {
        res = ADA_ERR_FS_GENERIC;
    }

    return res;
}

/**
 * @brief Function to get size of currently opened file
 * @return File size in bytes
 */
uint32_t AdaFs::getSize()
{
    uint32_t prevPos;
    uint32_t size;

    if ((strpbrk(_pMode, ADA_FS_MODE_READ)) || (strpbrk(_pMode, "+")))
    {
        prevPos = (uint32_t)ftell(_pFd);
        fseek(_pFd, 0L, SEEK_END);
        size = (uint32_t)ftell(_pFd);
        fseek(_pFd, prevPos, SEEK_SET);
    }
    else
    {
        size = _filePos;
    }

    return size;
}

/**
 * @brief Function to read data from a single file
 * @param[out]   pData                - pointer to data set
 * @param[out]   pSize                - size of data set in bytes
 * @param[in]    offset               - read file from offset (default - 0 (= from beginning))
 * @param[in]    maxSize              - maximum size of data set in bytes (default - 0 (= to EOF)).
 * @return  ADA_SUCCESS
 * @return  ADA_ERR_FS_NO_OBJECT      - object is not ready to use
 * @return  ADA_ERR_FS_NOT_APPLICABLE - target file was opened in write-only mode
 * @return  ADA_ERR_INVALID_PARAM     - not a valid parameter
 * @return  ADA_ERR_FS_IO             - IO-operation error (???)
 */
AdaError AdaFs::read(uint8_t *pData, uint32_t *pSize, uint32_t offset, uint32_t maxSize)
{
    _mutex.lock();

    if (_status != 1)
    {
        _mutex.unlock();
        return ADA_ERR_FS_NO_OBJECT;
    }

    if (strpbrk(_pMode, ADA_FS_MODE_WRITE))
    {
        _mutex.unlock();
        return ADA_ERR_FS_NOT_APPLICABLE;
    }

    /*
     * TODO - Let me know if we need to allocate memory here.
     * For now implying it's an error. P.M.
     */
    if ((!pData) || (!pSize))
    {
        _mutex.unlock();
        return ADA_ERR_INVALID_PARAM;
    }

    AdaError rc = ADA_SUCCESS;
    int32_t res;
    uint32_t size = getSize();

    if (size == 0)
    {
        *pSize = 0;
        _mutex.unlock();
        return ADA_SUCCESS;
    }

    if (offset >= size)
    {
        *pSize = 0;
        _mutex.unlock();
        return ADA_ERR_INVALID_PARAM;
    }

    if (maxSize == 0)
    {
        *pSize = size - offset;
    }
    else
    {
        *pSize = MIN(size - offset, maxSize);
    }

    fseek(_pFd, offset, SEEK_SET);
    res = (int32_t)fread(pData, sizeof(uint8_t), (size_t)*pSize, _pFd);

    if (res == 0)
    {
        rc = ADA_ERR_FS_IO;
        *pSize = 0;
    }

    _mutex.unlock();
    return rc;
}

/**
 * @brief Function to write data to file
 * @param   pData                     - data set
 * @param   size                      - size of data set in bytes
 * @return  ADA_SUCCESS
 * @return  ADA_ERR_FS_NO_OBJECT      - object is not ready to use
 * @return  ADA_ERR_FS_NOT_APPLICABLE - target file was opened in read-only mode
 * @return  ADA_ERR_FS_MAX_FILES      - maximum amount of files for current mask exceeded
 * @return  ADA_ERR_FS_IO             - IO-operation error (storage memory exceeded)
 * @return  ADA_ERR_FS_GENERIC        - any other not specified error (no access, etc)
 */
AdaError AdaFs::write(const uint8_t *pData, uint32_t size)
{
    _mutex.lock();

    if (_status != 1)
    {
        _mutex.unlock();
        return ADA_ERR_FS_NO_OBJECT;
    }

    if (strpbrk(_pMode, ADA_FS_MODE_READ))
    {
        _mutex.unlock();
        return ADA_ERR_FS_NOT_APPLICABLE;
    }

    int32_t res;
    AdaError rc = ADA_SUCCESS;
    uint32_t cond1, cond2;
    uint32_t maxFiles = (uint32_t)pow(10, ADA_FS_FILENAME_TAILMASK_SIZE);

    fseek(_pFd, 0L, SEEK_END);

    cond2 = _fragmented ?
            ADA_FS_MAX_FILESIZE_MB :
            4 * (uint32_t)ADA_FS_UNIT_KILO * (uint32_t)ADA_FS_UNIT_MEGA;
    cond1 = _filePos + size > cond2;

    if (cond1)
    {
        if ((uint32_t)(_fileId + 1) >= maxFiles)
        {
            inClose();
            _mutex.unlock();
            return ADA_ERR_FS_MAX_FILES;
        }

        rc = next();

        while (rc != ADA_SUCCESS)
        {
            rc = next();

            if ((rc == ADA_ERR_FS_GENERIC) || (rc == ADA_ERR_FS_MAX_FILES))
            {
                inClose();
                _mutex.unlock();
                return rc;
            }
        }
    }

    res = (int32_t)fwrite(pData, 1, (size_t)size, _pFd);

    _filePos += (uint32_t)res;

    if ((uint32_t)res < size)
    {
        inClose();
        _mutex.unlock();
        return ADA_ERR_FS_IO;
    }

    cond1 = _filePos / ADA_FS_SYNC_PERIOD_SIZE;
    cond2 = (_filePos + (uint32_t)res) / ADA_FS_SYNC_PERIOD_SIZE;

    if (cond1 != cond2)
    {
        rc = sync();
        fflush(_pFd);
    }

    _mutex.unlock();
    return rc;
}

/**
 * @brief Function to rewind file pointer. Should be used when we need to rewrite file
 * @return  ADA_SUCCESS
 * @return  ADA_ERR_FS_NO_OBJECT      - object is not ready to use
 * @return  ADA_ERR_FS_GENERIC        - any other not specified error (no access, etc)
 */
AdaError AdaFs::rewind()
{
    _mutex.lock();

    if (_status != 1)
    {
        _mutex.unlock();
        return ADA_ERR_FS_NO_OBJECT;
    }

    int32_t res;
    AdaError rc = ADA_SUCCESS;

    res = fseek(_pFd, 0L, SEEK_SET);

    if (res)
    {
        rc = ADA_ERR_FS_GENERIC;
    }
    else
    {
        _filePos = 0;
    }

    _mutex.unlock();
    return rc;
}

/*
 * Function to free object's pointers
 */
void AdaFs::destroyAllocs()
{
    if (_pFd)
    {
        fclose(_pFd);
        _pFd = NULL;
    }

    if (_pMode)
    {
        free(_pMode);
        _pMode = NULL;
    }

    if (_pFileActualPath)
    {
        free(_pFileActualPath);
        _pFileActualPath = NULL;
    }

    if (_pFileActualName)
    {
        free(_pFileActualName);
        _pFileActualName = NULL;
    }
}

/*
 * Function to close a file (to avoid deadlock)
 */
void AdaFs::inClose()
{
    _status      =   0;
    _fileId      =   0;
    _filePos     =   0;
    _fragmented  =   0;

    sync();
    destroyAllocs();
}

/**
 * @brief Function to finish any operations with object
 * @return  ADA_SUCCESS
 * @return  ADA_ERR_FS_NO_OBJECT - object is not ready to use
 */
AdaError AdaFs::close()
{
    _mutex.lock();

    if (_status != 1)
    {
        _mutex.unlock();
        return ADA_ERR_FS_NO_OBJECT;
    }

    inClose();
    _mutex.unlock();
    return ADA_SUCCESS;
}

AdaFs::~AdaFs()
{
    close();
    return;
}
