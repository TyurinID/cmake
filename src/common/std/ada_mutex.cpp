/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_mutex.cpp
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Mutex class implementation
 *
 *
 */
#include <std/ada_mutex.h>

#include <errno.h>
#include <stdio.h>
#include <assert.h>

class AdaMutexPrivate
{
public:
    explicit AdaMutexPrivate()
    {
    }
    pthread_mutex_t mutex;
};

AdaMutex::AdaMutex() : mPriv(NULL)
{
    mPriv = new AdaMutexPrivate();
    assert(0 == pthread_mutex_init(&mPriv->mutex, NULL));
}

AdaMutex::~AdaMutex()
{
    pthread_mutex_destroy(&mPriv->mutex);
    delete mPriv;
}

void AdaMutex::lock()
{
    assert(0 == pthread_mutex_lock(&mPriv->mutex));
}

void AdaMutex::unlock()
{
    assert(0 == pthread_mutex_unlock(&mPriv->mutex));
}
