/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 *******************************************************************************
 *
 *  @file ada_stat_queue.cpp
 *  @author Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *  @brief AdaStatQueue class implementation
 *
 *  Buffer stores fixed number of elements in heap and fill them cyclicly
 *
 *******************************************************************************
 */

#include <std/ada_stat_queue.h>
#include <cstring>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>

#define ADA_STD_STAT_Q_BUFFER_MAX_SIZE_BYTE    ((600) * (1024))  /* about 600 kByte */


template <typename T>
AdaStatQueue<T>::AdaStatQueue(uint32_t queueSize)
    : _pBuffer(NULL)
{
    if (queueSize < 1)
    {
        fprintf(stderr, "Queue size cannot be 0\n");
        exit(1);
    }

    if (queueSize * sizeof(T) > ADA_STD_STAT_Q_BUFFER_MAX_SIZE_BYTE)
    {
        fprintf(stderr,
                "Memory limit for queue is exceeded: %" PRIu32 " * %" PRIu32 " > %" PRIu32 "\n",
                queueSize, static_cast<uint32_t>(sizeof(T)),
                static_cast<uint32_t> (ADA_STD_STAT_Q_BUFFER_MAX_SIZE_BYTE));
        exit(1);
    }

    _pBuffer = new T[queueSize]();

    if (!_pBuffer)
    {
        fprintf(stderr, "Not enough memory\n");
        exit(1);
    }

    _queueSize = queueSize;
    _pushCnt = 0;
    _popCnt = 0;
    _numElems = 0;
}

template <typename T>
AdaStatQueue<T>::~AdaStatQueue()
{
    delete[] _pBuffer;
}


/*
 * Function waits for space in queue and push data to it
 * @param[in]    elem           data to be pushed into queue
 * @param[in]    timeoutUs      waiting time in us (set 0 in case of infinite waiting)
 */
template <typename T>
AdaError AdaStatQueue<T>::waitAndPush(const T &elem, uint32_t timeoutUs)
{
    /* If where are several providers mutexPush is needed */
    _condVar.lockMutex();

    while (_numElems >= _queueSize)
    {
        if (timeoutUs != 0)
        {
            AdaError ret = _condVar.timedWait(timeoutUs);

            if (ret == ADA_ERR_TIMEOUT)
            {
                _condVar.unlockMutex();
                return ADA_ERR_TIMEOUT;  /* timeout */
            }
            else if (ret == ADA_ERR_GENERIC)
            {
                _condVar.unlockMutex();
                fprintf(stderr, "Cond var error");
                return ADA_ERR_GENERIC;
            }
            else if (ret != ADA_SUCCESS)
            {
                _condVar.unlockMutex();
                return ret;
            }
        }
        else
        {
            _condVar.wait();
        }
    }

    _pBuffer[_pushCnt] = elem;

    if (++_pushCnt >= _queueSize)
    {
        _pushCnt = 0;
    }

    _numElems++;
    _condVar.signal();
    _condVar.unlockMutex();

    return ADA_SUCCESS;
}

template <typename T>
AdaError AdaStatQueue<T>::push(const T &elem)
{
    AdaError ret = ADA_SUCCESS;

    /* If where are several providers mutexPush is needed */
    _condVar.lockMutex();

    if (_numElems >= _queueSize)
    {
        ret = ADA_ERR_IS_FULL;
    }
    else
    {
        _pBuffer[_pushCnt] = elem;

        if (++_pushCnt >= _queueSize)
        {
            _pushCnt = 0;
        }

        _numElems++;
        _condVar.signal();
        ret = ADA_SUCCESS;
    }

    _condVar.unlockMutex();

    return ret;
}

/*
 * Function gets pointer to element in queue according to its number in queue
 * @param[in]    pElem          pointer for storing element pointer
 * @param[in]    elemCnt        elemnt number in queue (0 - first pushed)
 */
template <typename T>
AdaError AdaStatQueue<T>::get(T **pElem, uint32_t elemCnt)
{
    _condVar.lockMutex();

    if (elemCnt >= _numElems)
    {
        _condVar.unlockMutex();
        return ADA_ERR_STD_NO_ELEMENT;
    }

    _condVar.unlockMutex();

    elemCnt = (_popCnt + elemCnt) % _queueSize;

    *pElem = &_pBuffer[elemCnt];
    return ADA_SUCCESS;
}

/*
 * Function gets number of elements stored in queue
 * @param[in]    num          pointer for storing number of elements
 */
template <typename T>
AdaError AdaStatQueue<T>::getElemNum(uint32_t &num)
{
    _condVar.lockMutex();
    num = _numElems;
    _condVar.unlockMutex();
    return ADA_SUCCESS;
}

/*
 * Function waits for some data in queue and gets number of elements
 * @param[in]    num          pointer for storing number of elements
 * @param[in]    timeoutUs      waiting time in us (set 0 in case of infinite waiting)
 */
template <typename T>
AdaError AdaStatQueue<T>::waitAndGetElemNum(uint32_t &num, uint32_t timeoutUs)
{
    _condVar.lockMutex();

    while (_numElems == 0)
    {
        if (timeoutUs != 0)
        {
            AdaError ret = _condVar.timedWait(timeoutUs);

            if (ret == ADA_ERR_TIMEOUT)
            {
                _condVar.unlockMutex();
                return ADA_ERR_TIMEOUT;
            }
            else if (ret == ADA_ERR_GENERIC)
            {
                _condVar.unlockMutex();
                fprintf(stderr, "Cond var error");
                return ADA_ERR_GENERIC;
            }
            else if (ret != ADA_SUCCESS)
            {
                _condVar.unlockMutex();
                return ret;
            }
        }
        else
        {
            _condVar.wait();
        }
    }

    num = _numElems;
    _condVar.unlockMutex();

    return ADA_SUCCESS;
}

/*
 * Function pops FIFO element from queue
 * @param[in]    pElem      pointer to element for storing
 * @return      ADA_SUCCESS
 * @return      ADA_ERR_STD_EMPTY_QUEUE
 */
template <typename T>
AdaError AdaStatQueue<T>::pop(T *pElem)
{
    if (pElem == nullptr)
    {
        return ADA_ERR_INVALID_PARAM;
    }

    /* If where are several consumers mutexPop is needed*/
    _condVar.lockMutex();

    if (_numElems)
    {
        *pElem = _pBuffer[_popCnt];

        if (++_popCnt >= _queueSize)
        {
            _popCnt = 0;
        }

        _numElems--;
        _condVar.signal();
        _condVar.unlockMutex();
        return ADA_SUCCESS;
    }

    _condVar.unlockMutex();
    return ADA_ERR_STD_EMPTY_QUEUE;
}


/*
 * Function deletes FIFO element from queue
 */
template <typename T>
AdaError AdaStatQueue<T>::pop()
{
    /* If where are several consumers mutexPop is needed*/
    _condVar.lockMutex();

    if (_numElems)
    {
        if (++_popCnt >= _queueSize)
        {
            _popCnt = 0;
        }

        _numElems--;
        _condVar.signal();
        _condVar.unlockMutex();
        return ADA_SUCCESS;
    }

    _condVar.unlockMutex();
    return ADA_ERR_STD_EMPTY_QUEUE;
}

template<typename T>
void AdaStatQueue<T>::clear()
{
    _condVar.lockMutex();

    for (uint32_t i = 0; i < _queueSize; i++)
    {
        if (_numElems)
        {
            if (++_popCnt >= _queueSize)
            {
                _popCnt = 0;
            }

            _numElems--;
        }
        else
        {
            break;
        }
    }

    _condVar.signal();
    _condVar.unlockMutex();
}
