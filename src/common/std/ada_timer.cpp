/*
 *
 *       Copyright 2019 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_timer.cpp
 * @author  Igor Bykov <igor.bykov@sredasolutions.com>
 *
 * @brief   Timer class implementation
 *
 */


#include <utils/log/log_modules.h>
#define LOG_MODULE_ID LOG_MOD_STD
#define LOG_MODULE_LEVEL LOG_DEF_LEVEL_STD
#include <utils/log/log_api.h>

#include <cinttypes>

#include <std/ada_timer.h>

class Timer : public AdaTimer {
public:
    Timer() {
    }
    virtual ~Timer();

    AdaError activate(bool val);
    AdaError change(uint32_t startDelayMs, uint32_t periodMs);

    uint32_t getPeriodMs() const
    {
        return _periodMs;
    }

    void free() {
        delete this;
    }
private:
    uint32_t _periodMs;
};

AdaError AdaTimer::create(AdaTimer ** /*pInst*/, const char * /*name*/, TimerCallBack /*timerCb*/,
                          void * /*arg*/,
                          uint32_t /*startDelayMs*/,
                          uint32_t /*periodMs*/, bool /*autoActivate*/)
{
    return ADA_ERR_NOT_INITED;
}

Timer::~Timer()
{
}

AdaError Timer::activate(bool /*v*/)
{
    return ADA_ERR_NOT_INITED;
}

AdaError Timer::change(uint32_t /*startDelayMs*/, uint32_t /*periodMs*/)
{
    return ADA_ERR_NOT_INITED;
}
