/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ADA_cond_var.cpp
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Condition variables class implementation
 *
 */

#include <std/ada_cond_var.h>
#include <std/ada_cond_var.h>
#include <utils/utils.h>

#include <utils/log/log_modules.h>
#define LOG_MODULE_ID LOG_MOD_STD
#define LOG_MODULE_LEVEL LOG_DEF_LEVEL_STD
#include <utils/log/log_api.h>

#include <time.h>
#include <errno.h>
#include <assert.h>

#include <tx_api.h>
#include <tx_port.h>

class AdaCondVarPrivate
{
public:
    explicit AdaCondVarPrivate()
    {
    }

    TX_MUTEX     mutex;
    TX_SEMAPHORE semaphore;
};

AdaCondVar::AdaCondVar(const char *pName) : mPriv(NULL)
{
    UINT rc;
    const char *pSemName = "AdaCond";

    if (pName != nullptr)
    {
        pSemName = pName;
    }

    mPriv = new AdaCondVarPrivate();
    ADA_ASSERT_ALWAYS(mPriv != nullptr);

    rc = tx_semaphore_create(&mPriv->semaphore, (char *)pSemName, 0);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

    rc = tx_mutex_create(&mPriv->mutex, (char *)pSemName, TX_INHERIT);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);
}

AdaCondVar::~AdaCondVar()
{
    UINT rc;

    rc = tx_mutex_delete(&mPriv->mutex);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

    rc = tx_semaphore_delete(&mPriv->semaphore);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

    delete mPriv;
}

void AdaCondVar::lockMutex()
{
    UINT rc;

    rc = tx_mutex_get(&mPriv->mutex, TX_WAIT_FOREVER);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);
}

void AdaCondVar::unlockMutex()
{
    UINT rc;

    rc = tx_mutex_put(&mPriv->mutex);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);
}

void AdaCondVar::wait()
{
    UINT rc;
    UINT prevThreshold;
    UINT dummy;
    TX_THREAD *pThread;

    pThread = tx_thread_identify();

    /* Disable preemption of our thread */
    rc = tx_thread_preemption_change(pThread, 0, &prevThreshold);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

    rc = tx_mutex_put(&mPriv->mutex);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

    rc = tx_semaphore_get(&mPriv->semaphore, TX_WAIT_FOREVER);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS || rc == TX_WAIT_ABORTED);

    rc = tx_semaphore_prioritize(&mPriv->semaphore);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

    rc = tx_mutex_get(&mPriv->mutex, TX_WAIT_FOREVER);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

    rc = tx_thread_preemption_change(pThread, prevThreshold, &dummy);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);
}

/*
 * @return 0 in case of success
 * @return -1 in case of timeout
 * @return -2 in case of err
 */
AdaError AdaCondVar::timedWait(uint32_t useconds)
{
    UINT rc;
    UINT rcSem;
    UINT prevThreshold;
    UINT dummy;
    ULONG waitOption;
    ULONG timeMs;
    TX_THREAD *pThread;

    pThread = tx_thread_identify();

    /* Disable preemption of our thread */
    rc = tx_thread_preemption_change(pThread, 0, &prevThreshold);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

    rc = tx_mutex_put(&mPriv->mutex);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

    timeMs = useconds / MICRO_PER_MILLI;
    timeMs = (timeMs != 0) ? timeMs : 1;

    waitOption = ROUND_UP(timeMs, (MILLI_PER_UNIT / TX_TICK_RATE));

    rcSem = tx_semaphore_get(&mPriv->semaphore, waitOption);
    ADA_ASSERT_ALWAYS(rcSem == TX_SUCCESS || rcSem == TX_NO_INSTANCE);

    if (rcSem == TX_NO_INSTANCE)
    {
        rc = tx_mutex_get(&mPriv->mutex, TX_WAIT_FOREVER);
        assert(rc == TX_SUCCESS);

        rc = tx_thread_preemption_change(pThread, prevThreshold, &dummy);
        ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

        return ADA_ERR_TIMEOUT;
    }

    rc = tx_semaphore_prioritize(&mPriv->semaphore);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

    rc = tx_mutex_get(&mPriv->mutex, TX_WAIT_FOREVER);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

    rc = tx_thread_preemption_change(pThread, prevThreshold, &dummy);
    ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

    return ADA_SUCCESS;
}

void AdaCondVar::signal()
{
    if (mPriv->semaphore.tx_semaphore_suspended_count)
    {
        UINT rc;

        rc = tx_semaphore_prioritize(&mPriv->semaphore);
        ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);

        rc = tx_semaphore_put(&mPriv->semaphore);
        ADA_ASSERT_ALWAYS(rc == TX_SUCCESS);
    }
}

void AdaCondVar::broadcast()
{
    TX_THREAD *pThread;
    UINT rc;
    ULONG semCount;
    UINT prevThreshold;
    UINT dummy;

    semCount = mPriv->semaphore.tx_semaphore_suspended_count;

    if (!semCount)
    {
        return;
    }

    pThread = tx_thread_identify();

    /* Disable preemption of our thread */
    rc = tx_thread_preemption_change(pThread, 0, &prevThreshold);
    ADA_ASSERT_ALWAYS(rc != TX_SUCCESS);

    while (semCount)
    {
        rc = tx_semaphore_prioritize(&mPriv->semaphore);
        ADA_ASSERT_ALWAYS(rc != TX_SUCCESS);

        rc = tx_semaphore_put(&mPriv->semaphore);
        ADA_ASSERT_ALWAYS(rc != TX_SUCCESS);
        semCount--;
    }

    /* Re-enable thread preemption */
    rc = tx_thread_preemption_change(pThread, prevThreshold, &dummy);
    ADA_ASSERT_ALWAYS(rc != TX_SUCCESS);
}
