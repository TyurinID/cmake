/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_time.cpp
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Time class
 *
 *  Allows geting current time in milliseconds from 1970 January 1st
 *
 */

#include <time.h>
#include <sys/time.h>

#include <assert.h>
#include <utils/utils.h>
#include <std/ada_time.h>
#include <cstdio>
#include <inttypes.h>

#include <tx_posix.h>

#define SIX_MONTHS_MINUTES  (259200)

AdaTime::AdaTime()
{

}

void AdaTime::timestampBigEndian(uint8_t *pTimestamp)
{
    if (pTimestamp == NULL)
    {
        return;
    }

    struct timeval timing;
    gettimeofday(&timing, NULL);
    uint32_t timeLow = (uint32_t)(timing.tv_sec * 1000 + timing.tv_usec / 1000);
    uint32_t timeHigh =
        (uint32_t)(( (uint64_t)timing.tv_sec * 1000 + (uint64_t)timing.tv_usec / 1000) >> 32);
    uint32BigEncode(timeHigh, pTimestamp);
    uint32BigEncode(timeLow, &pTimestamp[4]);
}

void AdaTime::getTime(AdaTimeStruct *pTime_)
{
    struct timeval timing;
    gettimeofday(&timing, NULL);
    pTime_->min = (uint32_t)(timing.tv_sec / 60);
    pTime_->ms = (uint32_t)(timing.tv_usec / 1000);
}

uint64_t AdaTime::getMs()
{
    uint64_t res;
    struct timeval timing;
    gettimeofday(&timing, NULL);
    res = (uint64_t)timing.tv_sec * 1000 +  (uint64_t)timing.tv_usec / 1000;
    return res;
}

uint32_t AdaTime::getS()
{
    uint32_t res;
    struct timeval timing;
    gettimeofday(&timing, NULL);
    res = (uint32_t)timing.tv_sec;
    return res;
}

uint64_t AdaTime::getSteadyMs()
{
    return (uint64_t)tx_time_get();
}


/*
 * Converts time from minutes of the year + milliseconds in current minute to
 * milliseconds since Epoch
 * @param[in]    moy        minutes of current year
 * @param[in]    ms         milliseconds in current minute
 * @return      milliseconds since Epoch
 */
uint64_t AdaTime::convertToMs(int32_t moy, uint16_t ms)
{
    time_t seconds = time(NULL);
    assert(seconds != -1);
    struct tm timeCurrent = {};
    assert(NULL != gmtime_r(&seconds, &timeCurrent));

    if (timeCurrent.tm_mon == 0 && moy > SIX_MONTHS_MINUTES)
    {
        timeCurrent.tm_year = timeCurrent.tm_year - 1;
    }
    else
    {
        timeCurrent.tm_year = timeCurrent.tm_year;
    }

    timeCurrent.tm_min = moy;
    timeCurrent.tm_isdst = 0;
    timeCurrent.tm_hour = 0;
    timeCurrent.tm_mon = 0;
    timeCurrent.tm_sec = 0;
    timeCurrent.tm_mday = 1;
    seconds = mktime(&timeCurrent);
    assert(seconds != -1);
    return (uint64_t(seconds) * 1000 + uint64_t(ms)); /* In UTC we do not need shift  + timeCurrent.tm_gmtoff */
}

#define MS_IN_HOUR  (3600000)

/*
 * Converts time from tenths seconds in current or next hour
 * (according to timestampMs) to milliseconds since Epoch
 * @param[in]   tenthsSecInHour     tenths seconds in current or next hour
 * @param[in]   timestampMs
 * @return      milliseconds since Epoch
 */
uint64_t AdaTime::convertToMs(uint16_t tenthsSecInHour, uint64_t timestampMs)
{
    time_t seconds = time_t(timestampMs / 1000);
    struct tm timeCurrent = {};
    assert(NULL != gmtime_r(&seconds, &timeCurrent));
    uint32_t tenthsSecondsCurrent = uint32_t(timeCurrent.tm_min * 600 + timeCurrent.tm_sec * 10);

    if (tenthsSecondsCurrent > tenthsSecInHour)
    {
        timestampMs += MS_IN_HOUR;
    }

    return (timestampMs / 1000) * 1000 - uint64_t(tenthsSecondsCurrent) * 100 + uint64_t(
        tenthsSecInHour) * 100;
}


/*
 * Print current time in format [HH:MM:SS:ms] without \n
 */
void AdaTime::printMs()
{
    uint64_t timeMs = getMs();
    uint16_t ms = uint16_t(timeMs % 1000);
    time_t time = time_t(timeMs / 1000);
    struct tm timeCurrent = {};
    assert(NULL != gmtime_r(&time, &timeCurrent));
    fprintf(stderr, "[%02i:%02i:%02i:%03" PRIu16 "]",
            timeCurrent.tm_hour, timeCurrent.tm_min, timeCurrent.tm_sec, ms);
}

/*
 * Copy current time in format [HH:MM:SS:ms] to pBuf
 * param[in]    pBuf    pBuf has to have at least 15 bytes length
 */
void AdaTime::sprintMs(char *pBuf, uint32_t len, uint64_t timestampMs)
{
    uint64_t timeMs = (timestampMs == 0) ? getMs() : timestampMs;
    uint16_t ms = uint16_t(timeMs % 1000);
    time_t time = time_t(timeMs / 1000);
    struct tm timeCurrent = {0};

    gmtime_r(&time, &timeCurrent);
    snprintf(pBuf, len, "[%02i:%02i:%02i.%03" PRIu16 "]",
             timeCurrent.tm_hour, timeCurrent.tm_min, timeCurrent.tm_sec, ms);
}

/*
 * Print current time in format [HH:MM:SS:ms] with \n
 */
void inline AdaTime::printLnMs()
{
    printMs();
    fprintf(stderr, "\n");
}

uint64_t AdaTime::getUs()
{
    uint64_t res;
    struct timeval timing;
    gettimeofday(&timing, NULL);
    res = (uint64_t)timing.tv_sec * 1000000 +  (uint64_t)timing.tv_usec;
    return res;
}
