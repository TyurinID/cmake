/**
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 *******************************************************************************
 *
 *  @file ada_task.cpp
 *  @author Dmitry Larchenko <dmitry.larchenko@sredasolutions.com>
 *  @brief Task interface
 *
 *  Defines AdaTask class
 *
 *******************************************************************************
 */

#include <std/ada_task.h>
#include <std/ada_sleep.h>

#ifdef __CRATON__
extern "C"
{
#include <tx_posix.h>
}
#else
#include <pthread.h>
#endif

#include <std/ada_cond_var.h>
#include <utils/log/log_modules.h>
#define LOG_MODULE_ID       LOG_MOD_STD
#define LOG_MODULE_LEVEL    LOG_DEF_LEVEL_STD
#include <utils/log/log_api.h>

#include <cstdio>

#define ADA_TASK_DEFAULT_STACK_SIZE (1024 * 200)
#define ADA_TASK_START_TIMEOUT_US   (2000000)

class AdaTaskPrivate
{
public:
    explicit AdaTaskPrivate() : _thread(0), _runnable(NULL), _isRunning(false)
    {

    }


    pthread_t       _thread;
    AdaTaskRunnable *_runnable;
    bool            _isRunning;
    AdaCondVar      _condVar;
    const char      *_pName;
};

AdaTask::AdaTask(AdaTaskRunnable *pRunnable, const char *pTaskName, AdaTaskPriority priority)
    : _pPriv(NULL)
{
    UNUSED_PARAMETER(priority);

    _pPriv = new AdaTaskPrivate();
    _pPriv->_runnable = pRunnable;
    _pPriv->_pName = pTaskName;
}

AdaTask::~AdaTask()
{
    if (isRunning())
    {
        join();
    }

    delete _pPriv;
}

AdaError AdaTask::start()
{
    if (!isRunning())
    {
        pthread_attr_t thread_attr;
        int status = 0;
        size_t stackSize = 0;

        status = pthread_attr_init(&thread_attr);

        if (status != 0)
        {
            LOG_ERROR("Could not init attrs");
            return ADA_ERR_GENERIC;
        }

        status = pthread_attr_setstacksize(&thread_attr, ADA_TASK_DEFAULT_STACK_SIZE);

        if (status != 0)
        {
            LOG_ERROR("Could not set stacksize");
            return ADA_ERR_GENERIC;
        }

        status = pthread_create(&_pPriv->_thread, &thread_attr, AdaTask::run, this);

        if (status != 0)
        {
            LOG_ERROR("Could not create thread");
            return ADA_ERR_GENERIC;
        }

        /* Waiting for thread running */
        _pPriv->_condVar.lockMutex();

        while (_pPriv->_isRunning == false)
        {
            AdaError ret = _pPriv->_condVar.timedWait(ADA_TASK_START_TIMEOUT_US);

            if (ret == ADA_ERR_TIMEOUT)
            {
                _pPriv->_condVar.unlockMutex();
                LOG_ERROR("Start timeout!");
                return ADA_ERR_TIMEOUT;
            }
            else if (ret == ADA_ERR_GENERIC)
            {
                _pPriv->_condVar.unlockMutex();
                LOG_ERROR("UNKNOWN ERROR!");
                return ADA_ERR_GENERIC;
            }

            ADA_CHECK_ERROR_ALWAYS(ret);
        }

        _pPriv->_condVar.unlockMutex();

        status = pthread_attr_getstacksize(&thread_attr, &stackSize);

        if (status == 0)
        {
            if (_pPriv->_pName != nullptr)
            {
                LOG_INFO("Start [%s] [%lx] with stack size [%zu]", _pPriv->_pName, _pPriv->_thread,
                         stackSize);
            }
        }
        else
        {
            LOG_ERROR("Could not get stacksize!");
        }
    }

    return ADA_SUCCESS;
}


bool AdaTask::isRunning()
{
    _pPriv->_condVar.lockMutex();
    bool ret = _pPriv->_isRunning;
    _pPriv->_condVar.unlockMutex();
    return ret;
}

void AdaTask::join()
{
    int failed;

    failed = pthread_join(_pPriv->_thread, NULL);

    if (failed)
    {
        /* On UNEX platform pthread_join() may return error for running thread */
        while (isRunning())
        {
            AdaSleep::uSleep(100);
        }
    }
}

void *AdaTask::run(void *pArg)
{
    AdaTask *pTask = (AdaTask*) pArg;
    void *pRet;

    pTask->_pPriv->_condVar.lockMutex();
    pTask->_pPriv->_isRunning = true;
    pTask->_pPriv->_condVar.signal();
    pTask->_pPriv->_condVar.unlockMutex();

    /* Make sure that start() finished before we begin
     * TODO: use condVar for that purpose */
/*    AdaSleep::mSleep(1); */
    pRet = pTask->_pPriv->_runnable(pArg);

    LOG_INFO("Task [%s] [%lx] stopped", pTask->_pPriv->_pName, pTask->_pPriv->_thread);

    pTask->_pPriv->_condVar.lockMutex();
    pTask->_pPriv->_isRunning = false;
    pTask->_pPriv->_condVar.signal();
    pTask->_pPriv->_condVar.unlockMutex();


    return pRet;
}
