/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_dynamic_queue.cpp
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Dynamic Queue implementation
 *
 *     Queue carries data with certain size.
 */

#include <cstring>
#include <std/ada_dynamic_queue.h>
#include <cstdio>

#include <utils/log/log_modules.h>
#define LOG_MODULE_ID LOG_MOD_STD
#define LOG_MODULE_LEVEL LOG_DEF_LEVEL_STD
#include <utils/log/log_api.h>

#define QUEUE_ELEMENTS_MIN_NUM      (1)
#define QUEUE_ELEMENTS_MAX_NUM      (500)

#define DATA_MAX_SIZE       (1000000)  /* bytes */

/*
 * Queue element class
 */
class QueueElement
{
public:
    explicit QueueElement(uint32_t size_, const uint8_t *pData_)
        : list({
    }), dataSize(size_), pData(NULL)
    {
        listInitEntry(&list);
        pData = new uint8_t[dataSize];

        if (pData == NULL)
        {
            fprintf(stderr, "DynQueue Element: not enough memory\n");
        }
        else
        {
            memcpy(pData, pData_, dataSize);
        }
    }
    ~QueueElement()
    {
        delete[] pData;
    }
    ListHead list;
    uint32_t dataSize;
    uint8_t  *pData;
private:
    QueueElement(const QueueElement&);                 /* Prevent copy-construction */
    QueueElement& operator=(const QueueElement&);      /* Prevent assignment */
};

/*
 * Constructor
 */
AdaDynQueue::AdaDynQueue(uint32_t elementsMaxNum)
    : _queueHead({
}), _elementsMaxNum(1), _elementsNum(0)
{
    if (QUEUE_ELEMENTS_MIN_NUM > elementsMaxNum)
    {
        _elementsMaxNum = QUEUE_ELEMENTS_MIN_NUM;
    }
    else if (QUEUE_ELEMENTS_MAX_NUM < elementsMaxNum)
    {
        _elementsMaxNum = QUEUE_ELEMENTS_MAX_NUM;
    }
    else
    {
        _elementsMaxNum = elementsMaxNum;
    }

    listInitHead(&_queueHead);
}

/*
 * Destructor
 */
AdaDynQueue::~AdaDynQueue()
{
    ListHead *current;
    ListHead *tmp;
    listForEachEntrySafe(&_queueHead, current, tmp)
    {
        QueueElement *pElementDel = (QueueElement *)current;
        delete pElementDel;
    }
}



/**
 * @brief Function waits for free space in queue and pushes data
 * @param[in]    pData      pointer to data to be pushed into queue
 * @param[in]    size       data size
 * @param[in]    timeoutMs  waiting time in ms (set 0 in case of infinite waiting)
 * @return      @ref AdaError
 */
AdaError AdaDynQueue::waitAndPush(uint32_t size, const uint8_t *pData, uint32_t timeoutMs)
{
    QueueElement *pNewElement;

    if (pData == NULL || size > DATA_MAX_SIZE || size == 0)
    {
        return ADA_ERR_INVALID_PARAM;
    }

    _condVar.lockMutex();

    while (_elementsNum >= _elementsMaxNum)
    {
        if (timeoutMs != 0)
        {
            AdaError ret = _condVar.timedWait(timeoutMs * 1000);

            if (ret == ADA_ERR_TIMEOUT)
            {
                _condVar.unlockMutex();
                return ADA_ERR_TIMEOUT;  /* timeout */
            }

            ADA_CHECK_ERROR_ALWAYS(ret);
        }
        else
        {
            _condVar.wait();
        }
    }

    pNewElement = new QueueElement(size, pData);

    if (pNewElement == NULL)
    {
        _condVar.unlockMutex();
        return ADA_ERR_NO_MEMORY;
    }

    listInitEntry(&pNewElement->list);
    listPushBack(&_queueHead, &pNewElement->list);

    _elementsNum++;
    _condVar.signal();
    _condVar.unlockMutex();
    return ADA_SUCCESS;
}


/**
 * @brief If there is free space in queue function pushes data to queue otherwise returns ADA_ERR_IS_FULL
 * @param[in]    size_      data size
 * @param[in]    pData_     pointer to data to be pushed into queue
 * @return      @ref AdaError
 */
AdaError AdaDynQueue::push(uint32_t size, const uint8_t *pData)
{
    QueueElement *pNewElement;

    if (pData == NULL || size > DATA_MAX_SIZE || size == 0)
    {
        return ADA_ERR_INVALID_PARAM;
    }

    _condVar.lockMutex();

    if (_elementsNum >= _elementsMaxNum)
    {
        _condVar.unlockMutex();
        return ADA_ERR_IS_FULL;
    }

    pNewElement = new QueueElement(size, pData);

    if (pNewElement == NULL)
    {
        _condVar.unlockMutex();
        return ADA_ERR_NO_MEMORY;
    }

    listInitEntry(&pNewElement->list);
    listPushBack(&_queueHead, &pNewElement->list);

    _elementsNum++;
    _condVar.signal();
    _condVar.unlockMutex();
    return ADA_SUCCESS;
}


/**
 * @brief Function waits for data in queue and pops it.
 * @param[out]   pBuf       pointer to buffer for data storing
 * @param[in]    pSize      pointer to buffer size, will be overwritten by number of
 *                          poped bytes
 * @param[in]    timeMs     waiting time in ms (set 0 in case of infinite waiting)
 * @return      @ref AdaError
 */
AdaError AdaDynQueue::waitAndPop(uint8_t *pBuf, uint32_t *pSize, uint32_t timeMs)
{
    return waitAndGetGeneral(pBuf,
                             pSize,
                             timeMs,
                             false);
}


/**
 * @brief Function checks for available data in queue and pops it.
 * @param[out]   pBuf       pointer to buffer for data storing
 * @param[in]    pSize      pointer to buffer size, will be overwritten by number of
 *                          poped bytes
 * @return      @ref AdaError
 */
AdaError AdaDynQueue::pop(uint8_t *pBuf, uint32_t *pSize)
{
    return getGeneral(pBuf,
                      pSize,
                      false);
}


/**
 * @brief Function waits for data in queue and get it without copying.
 * @param[out]   pBuf       pointer to buffer for storing address to data
 * @param[in]    pSize      pointer will be overwritten by number of gotten bytes
 * @param[in]    timeMs     waiting time in ms (set 0 in case of infinite waiting)
 * @return      @ref AdaError
 */
AdaError AdaDynQueue::waitAndGet(uint8_t **pBuf, uint32_t *pSize, uint32_t timeMs)
{
    return waitAndGetGeneral(pBuf,
                             pSize,
                             timeMs,
                             true);
}


/**
 * @brief Function checks for available data in queue and get it without copying.
 * @param[out]   pBuf       pointer to buffer for storing address to data
 * @param[in]    pSize      pointer will be overwritten by number of gotten bytes
 * @return      @ref AdaError
 */
AdaError AdaDynQueue::get(uint8_t **pBuf, uint32_t *pSize)
{
    return getGeneral(pBuf,
                      pSize,
                      true);
}


/**
 * @brief Function to obtain the number of currently stored elements
 * @return      number of elements
 */
uint32_t AdaDynQueue::getElementsNum()
{
    _condVar.lockMutex();
    uint32_t num = _elementsNum;
    _condVar.unlockMutex();
    return num;
}


/**
 * @brief Function waits for data in queue and returns its size.
 * @param[in]    timeoutMs     waiting time in ms (set 0 in case of infinite waiting)
 * @return  data size in bytes
 * @return  0  - waiting timeout
 * @return  -1 - error
 */
int32_t AdaDynQueue::waitAndGetDataSize(uint32_t timeoutMs)
{
    _condVar.lockMutex();

    while (_elementsNum == 0)
    {
        if (timeoutMs != 0)
        {
            AdaError ret = _condVar.timedWait(timeoutMs * 1000);

            if (ret == ADA_ERR_TIMEOUT)
            {
                _condVar.unlockMutex();
                return 0;
            }

            ADA_CHECK_ERROR_ALWAYS(ret);
        }
        else
        {
            _condVar.wait();
        }
    }

    int32_t ret = -1;
    ListHead *pList;
    pList = listNext (&_queueHead, &_queueHead);

    if (pList != NULL)
    {
        QueueElement *pElement = (QueueElement *)pList;
        ret = static_cast<int32_t>(pElement->dataSize);
    }

    _condVar.unlockMutex();
    return ret;
}


/**
 * @brief Function checks for available data in queue and returns its size.
 * @return  data size in bytes
 * @return  0 - no data is available
 */
uint32_t AdaDynQueue::getDataSize()
{
    uint32_t ret = 0;
    ListHead *pList;
    _condVar.lockMutex();
    pList = listNext (&_queueHead, &_queueHead);

    if (pList != NULL)
    {
        QueueElement *pElement = (QueueElement *)pList;
        ret = pElement->dataSize;
    }

    _condVar.unlockMutex();
    return ret;
}

/**
 * @brief Function waits for data in queue and delete it.
 * @param[in]   timeoutMs     waiting time in ms (set 0 in case of infinite waiting)
 * @return      @ref AdaError
 */
AdaError AdaDynQueue::waitAndDelete(uint32_t timeoutMs)
{
    _condVar.lockMutex();

    while (0 == _elementsNum)
    {
        if (timeoutMs != 0)
        {
            AdaError ret = _condVar.timedWait(timeoutMs * 1000);

            if (ret == ADA_ERR_TIMEOUT)
            {
                _condVar.unlockMutex();
                return ADA_ERR_TIMEOUT;
            }

            ADA_CHECK_ERROR_ALWAYS(ret);
        }
        else
        {
            _condVar.wait();
        }
    }

    ListHead *pList;
    pList = listNext (&_queueHead, &_queueHead);

    if (pList == NULL)
    {
        _condVar.unlockMutex();
        return ADA_ERR_GENERIC;
    }

    QueueElement *pElement = (QueueElement *)pList;

    listDelete(pList);
    delete pElement;

    if (_elementsNum)
    {
        _elementsNum--;
    }

    _condVar.signal();
    _condVar.unlockMutex();
    return ADA_SUCCESS;
}

/**
 * @brief If there is data in queue function deletes upper element otherwise returns ADA_ERR_STD_EMPTY_QUEUE
 * @return      @ref AdaError
 */
AdaError AdaDynQueue::del()
{
    _condVar.lockMutex();

    if (!_elementsNum)
    {
        _condVar.unlockMutex();
        return ADA_ERR_STD_EMPTY_QUEUE;
    }

    ListHead *pList;
    pList = listNext (&_queueHead, &_queueHead);

    if (pList == NULL)
    {
        _condVar.unlockMutex();
        return ADA_ERR_GENERIC;
    }

    QueueElement *pElement = (QueueElement *)pList;

    listDelete(pList);
    delete pElement;

    if (_elementsNum)
    {
        _elementsNum--;
    }

    _condVar.signal();
    _condVar.unlockMutex();
    return ADA_SUCCESS;
}


void AdaDynQueue::setElementsMaxNum(uint32_t num)
{
    _condVar.lockMutex();

    if (_elementsNum > num)
    {
        _condVar.unlockMutex();
        return;
    }

    _condVar.unlockMutex();

    if (QUEUE_ELEMENTS_MIN_NUM > num)
    {
        _elementsMaxNum = QUEUE_ELEMENTS_MIN_NUM;
    }
    else if (QUEUE_ELEMENTS_MAX_NUM < num)
    {
        _elementsMaxNum = QUEUE_ELEMENTS_MAX_NUM;
    }
    else
    {
        _elementsMaxNum = num;
    }
}

/*
 * Function waits for data in queue and pops / get it.
 * @param[out]   pBuf       pointer to buffer for storing data or address to data
 * @param[in]    pSize      pointer to buffer size, will be overwritten by number of
 *                          poped bytes
 * @param[in]    timeMs     waiting time in ms (set 0 in case of infinite waiting)
 * @param[in]    shouldGet  set true if data in queue should be saved
 * @return      ADA_SUCCESS - success
 * @return      ADA_ERR_TIMEOUT   - waiting timeout
 * @return      ADA_ERR_INVALID_PARAM  - incorrect data params
 * @return      ADA_ERR_GENERIC  - list pointer is NULL
 * @return      ADA_ERR_INVALID_LENGTH  - buffer is not large enough
 */
AdaError AdaDynQueue::waitAndGetGeneral(void *pBuf, uint32_t *pSize, uint32_t timeMs,
                                        bool shouldGet)
{
    uint32_t dataSize;

    if (pBuf == NULL)
    {
        return ADA_ERR_INVALID_PARAM;
    }

    _condVar.lockMutex();

    while (_elementsNum == 0)
    {
        if (timeMs != 0)
        {
            AdaError ret = _condVar.timedWait(timeMs * 1000);

            if (ret == ADA_ERR_TIMEOUT)
            {
                _condVar.unlockMutex();
                return ADA_ERR_TIMEOUT;
            }

            ADA_CHECK_ERROR_ALWAYS(ret);
        }
        else
        {
            _condVar.wait();
        }
    }

    ListHead *pList;
    pList = listNext (&_queueHead, &_queueHead);

    if (pList == NULL)
    {
        _condVar.unlockMutex();
        return ADA_ERR_GENERIC;
    }

    QueueElement *pElement = (QueueElement *)pList;
    dataSize = pElement->dataSize;

    if (shouldGet)
    {
        *(uint8_t **)pBuf = pElement->pData;
        *pSize = dataSize;
    }
    else
    {
        if (dataSize > *pSize)
        {
            _condVar.unlockMutex();
            return ADA_ERR_INVALID_LENGTH;
        }

        memcpy(pBuf, pElement->pData, dataSize);
        *pSize = dataSize;

        listDelete(pList);
        delete pElement;

        if (_elementsNum)
        {
            _elementsNum--;
        }
    }

    _condVar.signal();
    _condVar.unlockMutex();
    return ADA_SUCCESS;
}

/*
 * Function checks for available data in queue and pops / get it.
 * @param[out]   pBuf       pointer to buffer for storing data or address to data
 * @param[in]    pSize      pointer to buffer size, will be overwritten by number of
 *                          poped bytes in case of success
 * @param[in]    shouldGet  set true if data in queue should be saved
 * @return      ADA_SUCCESS - success
 * @return      ADA_ERR_STD_EMPTY_QUEUE   - queue is empty
 * @return      ADA_ERR_INVALID_PARAM  - incorrect data params
 * @return      ADA_ERR_GENERIC  - list pointer is NULL
 * @return      ADA_ERR_INVALID_LENGTH  - buffer is not large enough
 */
AdaError AdaDynQueue::getGeneral(void *pBuf, uint32_t *pSize, bool shouldGet)
{
    uint32_t dataSize;

    if (pBuf == NULL)
    {
        return ADA_ERR_INVALID_PARAM;
    }

    _condVar.lockMutex();

    if (!_elementsNum)
    {
        _condVar.unlockMutex();
        return ADA_ERR_STD_EMPTY_QUEUE;
    }

    ListHead *pList;
    pList = listNext (&_queueHead, &_queueHead);

    if (pList == NULL)
    {
        _condVar.unlockMutex();
        return ADA_ERR_GENERIC;
    }

    QueueElement *pElement = (QueueElement *)pList;
    dataSize = pElement->dataSize;

    if (shouldGet)
    {
        *(uint8_t **)pBuf = pElement->pData;
        *pSize = dataSize;
    }
    else
    {
        if (dataSize > *pSize)
        {
            _condVar.unlockMutex();
            return ADA_ERR_INVALID_LENGTH;
        }

        memcpy(pBuf, pElement->pData, dataSize);
        *pSize = dataSize;

        listDelete(pList);
        delete pElement;

        if (_elementsNum)
        {
            _elementsNum--;
        }
    }

    _condVar.signal();
    _condVar.unlockMutex();
    return ADA_SUCCESS;
}
