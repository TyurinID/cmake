/*
 *
 *       Copyright 2019 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_timer_unex.cpp
 * @author  Igor Bykov <igor.bykov@sredasolutions.com>
 *
 * @brief   Timer class implementation
 *
 */


#include <utils/log/log_modules.h>
#define LOG_MODULE_ID LOG_MOD_STD
#define LOG_MODULE_LEVEL LOG_DEF_LEVEL_STD
#include <utils/log/log_api.h>

#include <tx_port.h>
#include <cinttypes>
#include <utils/log/log_api.h>

#include <std/ada_timer.h>
#include <tx_api.h>

class Timer : public AdaTimer {
public:
    Timer() {
    }
    virtual ~Timer();

    AdaError init(const char *name, TimerCallBack timerCb, void *arg, uint32_t startDelayMs,
                  uint32_t periodMs,
                  bool autoActivate);

    AdaError activate(bool val);
    AdaError change(uint32_t startDelayMs, uint32_t periodMs);

    uint32_t getPeriodMs() const
    {
        return _periodMs;
    }

    void free() {
        delete this;
    }
private:
    bool ms2Ticks(uint32_t ms, uint32_t *pTicks);
    uint32_t _periodMs;
    TX_TIMER _timer;
};

AdaError AdaTimer::create(AdaTimer **pInst, const char *name, TimerCallBack timerCb, void *arg,
                          uint32_t startDelayMs,
                          uint32_t periodMs, bool autoActivate)
{
    if (pInst == nullptr || timerCb == nullptr || periodMs == 0)
    {
        return ADA_ERR_INVALID_PARAM;
    }


    Timer *pTimer = new Timer();
    AdaError err = pTimer->init(name, timerCb, arg, startDelayMs, periodMs, autoActivate);

    if (err != ADA_SUCCESS)
    {
        delete pTimer;
        return err;
    }

    *pInst = pTimer;

    return ADA_SUCCESS;
}

Timer::~Timer()
{
    tx_timer_delete(&_timer);
}

bool Timer::ms2Ticks(uint32_t ms, uint32_t *pTicks)
{
    uint32_t ticks = ms;

    if (ticks)
    {
        ticks *= TX_TICK_RATE / 1000;

        if (ticks == 0)
        {
            return false;
        }
    }

    *pTicks = ticks;

    return true;
}

AdaError Timer::init(const char *pName, TimerCallBack timerCb, void *arg, uint32_t startDelayMs,
                     uint32_t periodMs,
                     bool autoActivate)
{
    uint32_t startDelayTicks;

    if (ms2Ticks(startDelayMs, &startDelayTicks) == false)
    {
        LOG_ERROR("Value of startDelay (%" PRIu32 ") is too small (not supported by system)",
                  startDelayMs);
        return ADA_ERR_INVALID_PARAM;
    }

    uint32_t periodTicks;

    if (ms2Ticks(periodMs, &periodTicks) == false)
    {
        LOG_ERROR("Value of timerPeriod (%" PRIu32 ") is too small (not supported by system)",
                  periodMs);
        return ADA_ERR_INVALID_PARAM;
    }

    /* FIXME tx_timer_create() failed if startDelayTicks == 0 */
    if (startDelayTicks == 0)
    {
        startDelayTicks = 1;
    }

    uint32_t status = tx_timer_create(&_timer,
                                      const_cast<char *>(pName),
                                      reinterpret_cast<void (*)(ULONG)>(timerCb),
                                      reinterpret_cast<ULONG>(arg),
                                      startDelayTicks, periodTicks, autoActivate);

    if (status != 0)
    {
        LOG_ERROR("tx_timer_create() failed with code: 0x%" PRIx32, status);
        return ADA_ERR_GENERIC;
    }

    _periodMs = periodMs;

    return ADA_SUCCESS;
}

AdaError Timer::activate(bool v)
{
    uint32_t ret = v ? tx_timer_activate(&_timer) : tx_timer_deactivate(&_timer);

    if (ret != 0)
    {
        LOG_ERROR("tx_timer_%sactivate() failed with code: 0x%" PRIx32, v ? "" : "de", ret);
        return ADA_ERR_GENERIC;
    }

    return ADA_SUCCESS;
}


AdaError Timer::change(uint32_t startDelayMs, uint32_t periodMs)
{
    uint32_t startDelayTicks;

    if (ms2Ticks(startDelayMs, &startDelayTicks) == false)
    {
        LOG_ERROR("Value of startDelay (%" PRIu32 ") is too small (not supported by system)",
                  startDelayMs);
        return ADA_ERR_INVALID_PARAM;
    }

    uint32_t periodTicks;

    if (ms2Ticks(periodMs, &periodTicks) == false)
    {
        LOG_ERROR("Value of timerPeriod (%" PRIu32 ") is too small (not supported by system)",
                  periodMs);
        return ADA_ERR_INVALID_PARAM;
    }

    uint32_t ret = tx_timer_change(&_timer, startDelayTicks, periodTicks);

    if (ret != 0)
    {
        LOG_ERROR("tx_timer_change() failed with code: 0x%" PRIx32, ret);
    }

    return ADA_SUCCESS;
}
