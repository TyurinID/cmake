/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/*
 * @file    ada_cond_var.cpp
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Condition variables class implementation
 *
 *
 */

#include <std/ada_cond_var.h>

#include <pthread.h>
#include <time.h>
#include <assert.h>
#include <errno.h>

#include <utils/log/log_modules.h>
#define LOG_MODULE_ID LOG_MOD_STD
#define LOG_MODULE_LEVEL LOG_DEF_LEVEL_STD
#include <utils/log/log_api.h>

class AdaCondVarPrivate
{
public:
    explicit AdaCondVarPrivate()
    {
    }
    pthread_cond_t  condVar;
    pthread_mutex_t mutex;
    struct timespec time;
    int64_t         temp;
};

AdaCondVar::AdaCondVar(const char*) : mPriv(NULL)
{
    pthread_mutexattr_t mutexAttr;

    mPriv = new AdaCondVarPrivate();
    ADA_ASSERT_ALWAYS(mPriv != nullptr);

    pthread_mutexattr_init(&mutexAttr);
    /* SEI CERT POS04-C: avoid using PTHREAD_MUTEX_NORMAL type mutex locks */
    pthread_mutexattr_settype(&mutexAttr, PTHREAD_MUTEX_ERRORCHECK);
    assert(0 == pthread_mutex_init(&mPriv->mutex, &mutexAttr));
    assert(0 == pthread_cond_init(&mPriv->condVar, NULL));

    mPriv->time.tv_sec = 0;
    mPriv->time.tv_nsec = 1000000;
    mPriv->temp = 0;
}

AdaCondVar::~AdaCondVar()
{
    pthread_mutex_destroy(&mPriv->mutex);
    pthread_cond_destroy(&mPriv->condVar);
    delete mPriv;
}

void AdaCondVar::lockMutex()
{
    int fail;

    fail = pthread_mutex_lock(&mPriv->mutex);

    if (fail)
    {
        LOG_FATAL("Mutex lock error: %d", fail);
        ADA_NEVER_REACHED_ALWAYS();
    }
}

void AdaCondVar::unlockMutex()
{
    int fail;

    fail = pthread_mutex_unlock(&mPriv->mutex);

    if (fail)
    {
        if (fail == EPERM)
        {
            LOG_FATAL("Mutex unlock error: %d: The current thread does not own the mutex.", fail);
        }
        else
        {
            LOG_FATAL("Mutex unlock error: %d", fail);
        }

        ADA_NEVER_REACHED_ALWAYS();
    }
}

void AdaCondVar::wait()
{
    assert(0 == pthread_cond_wait(&mPriv->condVar, &mPriv->mutex));
}

/*
 * Block until either the condition variable becomes available, or timeout
 * expires
 * @param[in]   useconds   - timeout in microseconds
 * @return Ada_SUCCESS in case of success
 * @return Ada_ERR_TIMEOUT in case of timeout
 * @return Ada_ERR_GENERIC in case of err
 */
AdaError AdaCondVar::timedWait(uint32_t useconds)
{
    assert(clock_gettime(CLOCK_REALTIME, &mPriv->time) == 0);
    mPriv->temp = mPriv->time.tv_nsec + (int64_t)useconds * 1000;
    mPriv->time.tv_sec = mPriv->time.tv_sec + mPriv->temp / 1000000000;
    mPriv->time.tv_nsec = mPriv->temp % 1000000000;

    int ret = pthread_cond_timedwait(&mPriv->condVar, &mPriv->mutex, &mPriv->time);

    if (ret == 0)
    {
        return ADA_SUCCESS;
    }
    else if (ret == ETIMEDOUT)
    {
        return ADA_ERR_TIMEOUT;
    }

    return ADA_ERR_GENERIC;
}

void AdaCondVar::signal()
{
    assert(0 == pthread_cond_signal(&mPriv->condVar));
}

void AdaCondVar::broadcast()
{
    assert(0 == pthread_cond_broadcast(&mPriv->condVar));
}
