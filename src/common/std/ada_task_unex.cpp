/**
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 *******************************************************************************
 *
 *  @file ada_task_unex.cpp
 *  @author Dmitry Larchenko <dmitry.larchenko@sredasolutions.com>
 *  @brief Task interface implementation for UNEX platform
 *
 *  Defines AdaTask class
 *
 *******************************************************************************
 */

#include <std/ada_task.h>
#include <std/ada_sleep.h>

#ifndef __CRATON__
#error "Invalid platform"
#endif
extern "C"
{
#include <tx_api.h>
}

#include <std/ada_cond_var.h>
#include <utils/log/log_modules.h>
#define LOG_MODULE_ID       LOG_MOD_STD
#define LOG_MODULE_LEVEL    LOG_DEF_LEVEL_STD
#include <utils/log/log_api.h>

#include <cstdio>

#define ADA_TASK_DEFAULT_STACK_SIZE (1024 * 200)
#define ADA_TASK_START_TIMEOUT_US   (2000000)

/* Default pthread priority on UNEX is 33 */
#define ADA_TASK_PRIORITY_UNEX_DEFAULT   (33)

/* Syslog works with priority 61 */
#define ADA_TASK_PRIORITY_UNEX_LOW   (61)

static void unexTaskMain(ULONG arg);

class AdaTaskPrivate
{
public:
    explicit AdaTaskPrivate() : _runnable(NULL), _isRunning(false)
    {

    }

    TX_THREAD       _thread;
    AdaTaskRunnable *_runnable;
    uint8_t         *_pStack;
    bool            _isRunning;
    AdaCondVar      _condVar;
    const char      *_pName;
    UINT            _priority;
};

AdaTask::AdaTask(AdaTaskRunnable *pRunnable, const char *pTaskName, AdaTaskPriority priority)
    : _pPriv(NULL)
{
    _pPriv = new AdaTaskPrivate();
    ADA_ASSERT_ALWAYS(_pPriv != nullptr);

    _pPriv->_pStack = new uint8_t[ADA_TASK_DEFAULT_STACK_SIZE];
    ADA_ASSERT_ALWAYS(_pPriv->_pStack != nullptr);

    _pPriv->_runnable = pRunnable;
    _pPriv->_pName = pTaskName;

    switch (priority)
    {
        case ADA_TASK_PRIORITY_HIGHEST:
        {
            _pPriv->_priority = ADA_TASK_PRIORITY_UNEX_DEFAULT - 3;
            break;
        }

        case ADA_TASK_PRIORITY_HIGH:
        {
            _pPriv->_priority = ADA_TASK_PRIORITY_UNEX_DEFAULT - 2;
            break;
        }

        case ADA_TASK_PRIORITY_MEDIUM_HIGH:
        {
            _pPriv->_priority = ADA_TASK_PRIORITY_UNEX_DEFAULT - 1;
            break;
        }

        case ADA_TASK_PRIORITY_MEDIUM:
        {
            _pPriv->_priority = ADA_TASK_PRIORITY_UNEX_DEFAULT;
            break;
        }

        case ADA_TASK_PRIORITY_MEDIUM_LOW:
        {
            _pPriv->_priority = ADA_TASK_PRIORITY_UNEX_LOW - 1;
            break;
        }

        case ADA_TASK_PRIORITY_LOW:
        {
            _pPriv->_priority = ADA_TASK_PRIORITY_UNEX_LOW;
            break;
        }

        case ADA_TASK_PRIORITY_LOWEST:
        {
            _pPriv->_priority = ADA_TASK_PRIORITY_UNEX_LOW + 1;
            break;
        }
    }
}

AdaTask::~AdaTask()
{
    if (isRunning())
    {
        join();
    }

    delete _pPriv;
}

AdaError AdaTask::start()
{
    if (isRunning())
    {
        return ADA_SUCCESS;
    }

    ULONG trv;

    if (_pPriv->_pName == nullptr)
    {
        _pPriv->_pName = "AdaTask";
    }

    trv = tx_thread_create(&_pPriv->_thread, (CHAR*) _pPriv->_pName,
                           unexTaskMain, (ULONG) this,
                           _pPriv->_pStack, ADA_TASK_DEFAULT_STACK_SIZE,
                           _pPriv->_priority, _pPriv->_priority,
                           TX_NO_TIME_SLICE, TX_AUTO_START);

    if (trv != TX_SUCCESS)
    {
        LOG_ERROR("Could not start thread [%lu]", trv);
        return ADA_ERR_GENERIC;
    }

    /* Waiting for thread running */
    _pPriv->_condVar.lockMutex();

    while (_pPriv->_isRunning == false)
    {
        AdaError ret = _pPriv->_condVar.timedWait(ADA_TASK_START_TIMEOUT_US);

        if (ret == ADA_ERR_TIMEOUT)
        {
            _pPriv->_condVar.unlockMutex();
            LOG_ERROR("Start timeout!");
            return ADA_ERR_TIMEOUT;
        }
        else if (ret == ADA_ERR_GENERIC)
        {
            _pPriv->_condVar.unlockMutex();
            LOG_ERROR("UNKNOWN ERROR!");
            return ADA_ERR_GENERIC;
        }

        ADA_CHECK_ERROR_ALWAYS(ret);
    }

    _pPriv->_condVar.unlockMutex();


    LOG_INFO("Started [%s] [%lx] stack len:[%lu] pri:[%u]",
             _pPriv->_pName, _pPriv->_thread.tx_thread_id, _pPriv->_thread.tx_thread_stack_size,
             _pPriv->_priority);

    return ADA_SUCCESS;
}


bool AdaTask::isRunning()
{
    _pPriv->_condVar.lockMutex();
    bool ret = _pPriv->_isRunning;
    _pPriv->_condVar.unlockMutex();
    return ret;
}

void AdaTask::join()
{
    UINT ret = TX_FEATURE_NOT_ENABLED;
    int32_t retryCounter = 1000;

    _pPriv->_condVar.lockMutex();

    while (_pPriv->_isRunning)
    {
        AdaError status;

        status = _pPriv->_condVar.timedWait(10 * 1000000);

        if (status == ADA_ERR_TIMEOUT)
        {
            LOG_WARNING("Join() timeout for [%s]", _pPriv->_pName);
        }
        else if (status != ADA_SUCCESS)
        {
            ADA_NEVER_REACHED_ALWAYS();
        }

    }

    _pPriv->_condVar.unlockMutex();

    while (ret != TX_SUCCESS)
    {
        retryCounter--;

        if (retryCounter == 0)
        {
            LOG_FATAL("tx_thread_delete() retry counter exceeded");
            break;
        }

        AdaSleep::uSleep(100);
        ret = tx_thread_delete(&_pPriv->_thread);
    }
}

void *AdaTask::run(void *pArg)
{
    AdaTask *pTask = static_cast<AdaTask*>(pArg);
    void *pRet;

    pTask->_pPriv->_condVar.lockMutex();
    pTask->_pPriv->_isRunning = true;
    pTask->_pPriv->_condVar.signal();
    pTask->_pPriv->_condVar.unlockMutex();

    /* Make sure that start() finished before we begin
     * TODO: use condVar for that purpose */
    AdaSleep::mSleep(1);
    pRet = pTask->_pPriv->_runnable(pArg);

    LOG_INFO("Task [%s] [%lx] stopped", pTask->_pPriv->_pName, pTask->_pPriv->_thread.tx_thread_id);

    pTask->_pPriv->_condVar.lockMutex();
    pTask->_pPriv->_isRunning = false;
    pTask->_pPriv->_condVar.signal();
    pTask->_pPriv->_condVar.unlockMutex();


    return pRet;
}

static void unexTaskMain(ULONG arg)
{
    AdaTask *pTask = (AdaTask*) arg;

    AdaTask::run(pTask);
}
