/**
 *
 *       Copyright 2019 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */
#include "testlib/test.h"

/**
 * @file    test.cpp
 * @author  Ivan Ivanov <ivan.ivanov@sredasolutions.com>
 *
 * @brief   Just an example
 *
 */

int testf(void) {
    return 1;
}
