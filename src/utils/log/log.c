/**
 *
 *       Copyright 2017 - 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file:   log.c
 * @author: Dmitry Larchenko <dmitry.larchenko@sredasolutions.com>
 * @date: May 2, 2017
 *
 * Implementation of logging module
 */

#include <utils/log/log_modules.h>
#define LOG_MODULE_ID LOG_MOD_LOG
#define LOG_MODULE_LEVEL LOG_DEF_LEVEL_LOG
#include <utils/log/log_api.h>
#include <stddef.h>
#include <stdlib.h>
#ifdef __CRATON__
#include <craton/syslog.h>
#endif

const char *logLevelFmt[] = LOG_LEVEL_FMT;
STATIC_ASSERT(ARRAY_SIZE(logLevelFmt) == LOG_LEVEL_NUM, \
              "LOG_LEVEL_FMT has incorrect size. Should be equal to LOG_LEVEL_NUM");

const char *logModuleStr[] = LOG_MODULE_STRINGS;
STATIC_ASSERT(ARRAY_SIZE(logModuleStr) == LOG_MOD_NUM,
              "LOG_MODULE_STRINGS has incorrect size. Should be equal to LOG_MOD_NUM");

static const char *sErrorDescription[] = ADA_ERROR_DESCRIPTION;
STATIC_ASSERT(ARRAY_SIZE(sErrorDescription) == ADA_ERROR_TOTAL_NUM,
              "ADA_ERROR_DESCRIPTION has invalid size");

static const char *sErrorEnumVal[] = ADA_ERROR_ENUMVAL;
STATIC_ASSERT(ARRAY_SIZE(sErrorEnumVal) == ADA_ERROR_TOTAL_NUM,
              "ADA_ERROR_ENUMVAL has invalid size");

static const char *sErrorStdDescription[] = ADA_ERROR_STD_DESCRIPTION;
STATIC_ASSERT(ARRAY_SIZE(sErrorStdDescription) == ADA_ERROR_STD_TOTAL_NUM,
              "ADA_ERROR_STD_DESCRIPTION has invalid size");

static const char *sErrorStdEnumVal[] = ADA_ERROR_STD_ENUMVAL;
STATIC_ASSERT(ARRAY_SIZE(sErrorStdEnumVal) == ADA_ERROR_STD_TOTAL_NUM,
              "ADA_ERROR_STD_ENUMVAL has invalid size");


static void genericAdaLogCb(void *pUserCtx, const char *pMsg, const uint32_t size,
                            LogLevelId level);

LogCtx _gLogCtx =
{
#ifdef __CRATON__
    .logCb     = logSendToSyslog,
#else
    .logCb     = genericAdaLogCb,
#endif
    .pUserCtx  = NULL,
    .level     = LOG_DEFAULT_LEVEL,
};

/*
 * Text log hex dump implementation
 */
void logHexdump(LogCtx *pLogCtx, LogModuleId module, LogLevelId level,
                const char *pFunc,
                uint32_t line, const uint8_t *pData, uint32_t length)
{
    char hexLine[LOG_HEXDUMP_COLS * 3 + 1]  = {0};
    char asciiLine[LOG_HEXDUMP_COLS + 1] = {0};
    uint32_t i;

    while (length > 0)
    {
        const uint32_t lineBytes = MIN(length, LOG_HEXDUMP_COLS);

        for (i = 0; i < lineBytes; i++)
        {
            LOG_DO_SNPRINTF(&hexLine[i * 3], 4, "%02x ", pData[i]);

            if (((char*) pData)[i] >= ' ' && ((char*) pData)[i] <= '~')
            {
                asciiLine[i] = ((char*) pData)[i];
            }
            else
            {
                asciiLine[i] = '.';
            }
        }

        for (; i < LOG_HEXDUMP_COLS; i++)
        {
            hexLine[i * 3] = ' ';
            hexLine[i * 3 + 1] = ' ';
            hexLine[i * 3 + 2] = ' ';
            asciiLine[i] = ' ';
        }

        /* At this point we assume that the last byte in asciiLine and hexLine
         * is \0 (because we always write fixed-length (=N-1) strings  into
         * zero-initialized buffer (of size N)) */

        LOG_PRINT_ARGS(pLogCtx, level, pFunc, line, module,
                       "%p %s | %s",
                       pData, hexLine, asciiLine);
        pData = &pData[lineBytes];
        length -= lineBytes;
    }
}
/* End of text log hex dump implementation */


const char *logErrorDescription(AdaError errorCode)
{
    if ( /*errorCode >= ADA_ERROR_BASE && */ errorCode < ADA_ERR_LAST)
    {
        return sErrorDescription[errorCode - ADA_ERROR_BASE];
    }

    if (errorCode >= ADA_ERROR_STD_BASE && errorCode < ADA_ERR_STD_LAST)
    {
        return sErrorStdDescription[errorCode - ADA_ERROR_STD_BASE];
    }

    return "<no description>";
}

const char *logErrorEnumValue(AdaError errorCode)
{
    if ( /*errorCode >= ADA_ERROR_BASE && */ errorCode < ADA_ERR_LAST)
    {
        return sErrorEnumVal[errorCode - ADA_ERROR_BASE];
    }

    if (errorCode >= ADA_ERROR_STD_BASE && errorCode < ADA_ERR_STD_LAST)
    {
        return sErrorStdEnumVal[errorCode - ADA_ERROR_STD_BASE];
    }

    return "<no enum value>";
}

void logStopExecution(void)
{
    fprintf(stderr, "logStopExecution()\n");
    abort();
}

void logStub(const void *pUnused, ...)
{
    UNUSED_PARAMETER(pUnused);
}

static void genericAdaLogCb(void *pUserCtx, const char *pMsg, const uint32_t size, LogLevelId level)
{
    UNUSED_PARAMETER(pUserCtx);
    UNUSED_PARAMETER(size);
    UNUSED_PARAMETER(level);

    fprintf(stderr, "%s\n", pMsg);
}

#ifdef __CRATON__

#undef LOG_WARINING
#undef LOG_NOTICE
#undef LOG_INFO
#undef LOG_DEBUG

void logSendToSyslog(void *pUserCtx, const char *pMsg, const uint32_t size,
                     LogLevelId level)
{
    syslog_level_t syslogLevel;

    UNUSED_PARAMETER(pUserCtx);
    UNUSED_PARAMETER(size);
    UNUSED_PARAMETER(genericAdaLogCb);

    switch (level)
    {
        case LOG_LEVEL_WARNING:
        {
            syslogLevel = LOG_WARNING;
            break;
        }

        case LOG_LEVEL_NOTICE:
        {
            syslogLevel = LOG_NOTICE;
            break;
        }

        case LOG_LEVEL_INFO:
        {
            syslogLevel = LOG_INFO;
            break;
        }

        /*
         * @note: was changed to INFO for Unex debug log error avoid
         */
        case LOG_LEVEL_DEBUG:
        {
            syslogLevel = LOG_INFO;
            break;
        }

        default:
            syslogLevel = LOG_ERR;
    }

    syslog(syslogLevel, "%s", pMsg);
}
#else

void logSendToSyslog(void *pUserCtx, const char *pMsg, const uint32_t size,
                     LogLevelId level)
{
    /* Not implemented. */

    UNUSED_PARAMETER(pUserCtx);
    UNUSED_PARAMETER(size);
    UNUSED_PARAMETER(pMsg);
    UNUSED_PARAMETER(level);
    UNUSED_PARAMETER(genericAdaLogCb);

    genericAdaLogCb(pUserCtx, pMsg, size, level);
}
#endif
