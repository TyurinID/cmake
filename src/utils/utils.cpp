/**
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 *******************************************************************************
 *
 *  @file utils.cpp
 *  @author Dmitry Larchenko <dmitry.larchenko@sredasolutions.com>
 *  @brief Redefinition of new and delete without exceptions
 *
 *  Defines new and delete without exceptions.
 *
 *******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <std/ada_sleep.h>
#include <malloc.h>

/* #define TRACE_MEMORY */

#ifdef TRACE_MEMORY
static uint32_t memoryAllocated = 0;

void *operator new(size_t size) throw()
{
    void *ptr = malloc(size);

    if (!ptr)
    {
        fprintf(stderr, "\nCannot allocate memory size %d\n", (int)size);
        AdaSleep::mSleep(500);
    }

    memoryAllocated += (uint32_t)malloc_usable_size(ptr);
    fprintf(stderr, "malloc(+%d = %" PRIu32 ")\n", (int)size, memoryAllocated);

    return ptr;
}


void operator delete(void *p) throw()
{
    memoryAllocated -= (uint32_t)malloc_usable_size(p);
    fprintf(stderr, "free(-%d = %" PRIu32 ")\n", (int)malloc_usable_size(p), memoryAllocated);
    free(p);
}

void *operator new[](size_t size) throw()
{
    void *ptr = malloc(size);

    if (!ptr)
    {
        fprintf(stderr, "\nCannot allocate[] memory size %d\n", (int)size);
        AdaSleep::mSleep(500);
    }

    memoryAllocated += (uint32_t)malloc_usable_size(ptr);
    fprintf(stderr, "malloc[](+%d = %" PRIu32 ")\n", (int)size, memoryAllocated);

    return ptr;
}

void operator delete[](void *p) throw()
{
    memoryAllocated -= (uint32_t)malloc_usable_size(p);
    fprintf(stderr, "free[](-%d = %" PRIu32 ")\n", (int)malloc_usable_size(p), memoryAllocated);
    free(p);
}

#else

void *operator new(size_t size) throw()
{
    void *ptr = malloc(size);

    if (!ptr)
    {
        fprintf(stderr, "\n\nCannot allocate memory size %d\n", (int)size);
        AdaSleep::mSleep(500);
    }

    return ptr;
}


void operator delete(void *p) throw()
{
    free(p);
}

void *operator new[](size_t size) throw()
{
    void *ptr = malloc(size);

    if (!ptr)
    {
        fprintf(stderr, "\n\nCannot allocate memory size %d\n", (int)size);
        AdaSleep::mSleep(500);
    }

    return ptr;
}

void operator delete[](void *p) throw()
{
    free(p);
}
#endif






#if 0 /* Probably we should enable this too on ARM */

extern "C" int __aeabi_atexit(void *object,
                              void (*destructor)(void *),
                              void *dso_handle)
{
    UNUSED_PARAMETER(object);
    UNUSED_PARAMETER(destructor);
    UNUSED_PARAMETER(dso_handle);
    return 0;
}

#endif
