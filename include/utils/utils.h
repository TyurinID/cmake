#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>
#include <assert.h>

#define UNUSED_VARIABLE(X)  ((void)(X))
#define UNUSED_PARAMETER(X) UNUSED_VARIABLE(X)
#define UNUSED_RETURN_VALUE(X) UNUSED_VARIABLE(X)


/** Leaves the minimum of the two 32-bit arguments */
#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif

/** Leaves the maximum of the two 32-bit arguments */
#define MAX(a, b) ((a) < (b) ? (b) : (a))

/** Leaves the absolute of the 32-bit argument */
#define ABS(a) ((a) < (0) ? (-(a)) : (a))

/** Build-time assertion checker */
#ifdef static_assert
#define STATIC_ASSERT(COND, MSG) static_assert(COND, MSG)
#else
#define STATIC_ASSERT(COND, MSG) \
    extern char static_assertion_failed[(COND) ? 1 : -1]
#endif

/**
 * Build-time array length calculator
 *
 * @param array Array with known number of elements
 */
#define ARRAY_SIZE(array) (sizeof(array) / sizeof ((array)[0]))

/** @brief Macro for performing integer division, making sure the result is rounded up.
 *
 * @details One typical use for this is to compute the number of objects with size B is needed to
 *          hold A number of bytes.
 *
 * @param[in]   A   Numerator.
 * @param[in]   B   Denominator.
 *
 * @return      Integer result of dividing A by B, rounded up.
 */
#define CEIL_DIV(A, B)      \
    (((A) + (B) -1) / (B))

#define ROUND_UP(n, d) (CEIL_DIV(n, d) * (d))
#define ROUND_DOWN(n, d) (((n) / (d)) * (d))

/* Unit scale conversion factors */
#define CENTI_PER_UNIT  100
#define MILLI_PER_UNIT  1000
#define MICRO_PER_MILLI 1000
#define NANO_PER_MICRO  1000
#define MICRO_PER_UNIT  1000000
#define NANO_PER_MILLI  1000000
#define NANO_PER_UNIT   1000000000

#define MAC_ADDR_FORMAT "%02X:%02X:%02X:%02X:%02X:%02X"
#define MAC_ADDR(addr) (addr)[0],(addr)[1], \
    (addr)[2],(addr)[3], \
    (addr)[4],(addr)[5]


/* Bit helper macros */
#define BIT(N) (1U << (N))
#define BITMASK(N) ((1U << (N)) - 1)

/* Speed conversion */
#define KMH_TO_CMS(kmh)   \
    ((kmh) * 100000.0 / 3600.0)

#define CMS_TO_KMH(cms)   \
    ((cms) * 3600.0 / 100000.0)


/*
 * ********************************************************************
 *
 * BigEndian conversion Functions
 *
 * ********************************************************************
 */

/* todo: add #ifdef for BE/LE */

/*
 * @brief Function for encoding an uint16 value in big-endian format.
 *
 * @param[in]   value            Value to be encoded.
 * @param[out]  p_encoded_data   Buffer where the encoded data will be written.
 *
 * @return      Number of bytes written.
 */
static inline uint8_t uint16BigEncode(uint16_t value, uint8_t *p_encoded_data)
{
    p_encoded_data[0] = (uint8_t) (value >> 8);
    p_encoded_data[1] = (uint8_t) (value & 0xFF);
    return sizeof(uint16_t);
}

/**@brief Function for decoding a uint16 value in big-endian format.
 *
 * @param[in]   p_encoded_data   Buffer where the encoded data is stored.
 *
 * @return      Decoded value.
 */
static inline uint16_t uint16BigDecode(const uint8_t *p_encoded_data)
{
    return (uint16_t)( (((uint16_t)((uint8_t *)p_encoded_data)[0]) << 8 ) |
                       (((uint16_t)((uint8_t *)p_encoded_data)[1])) );
}

/**@brief Function for decoding a uint32 value in big-endian format.
 *
 * @param[in]   p_encoded_data   Buffer where the encoded data is stored.
 *
 * @return      Decoded value.
 */
static inline uint32_t uint32BigDecode(const uint8_t *p_encoded_data)
{
    return ( (((uint32_t)((uint8_t *)p_encoded_data)[0]) << 24) |
             (((uint32_t)((uint8_t *)p_encoded_data)[1]) << 16) |
             (((uint32_t)((uint8_t *)p_encoded_data)[2]) << 8)  |
             (((uint32_t)((uint8_t *)p_encoded_data)[3]) << 0) );
}


/**@brief Function for decoding a int32 value in big-endian format.
 *
 * @param[in]   p_encoded_data   Buffer where the encoded data is stored.
 *
 * @return      Decoded value.
 */
static inline int32_t int32BigDecode(const uint8_t *p_encoded_data)
{
    return ( (((int32_t)((uint8_t *)p_encoded_data)[0]) << 24) |
             (((int32_t)((uint8_t *)p_encoded_data)[1]) << 16) |
             (((int32_t)((uint8_t *)p_encoded_data)[2]) << 8)  |
             (((int32_t)((uint8_t *)p_encoded_data)[3]) << 0) );
}

/*
 * @brief Function for encoding an uint32 value in big-endian format.
 *
 * @param[in]   value            Value to be encoded.
 * @param[out]  p_encoded_data   Buffer where the encoded data will be written.
 *
 * @return      Number of bytes written.
 */
static inline uint8_t uint32BigEncode(uint32_t value, uint8_t *p_encoded_data)
{
    p_encoded_data[0] = (uint8_t) (value >> 24);
    p_encoded_data[1] = (uint8_t) (value >> 16);
    p_encoded_data[2] = (uint8_t) (value >> 8);
    p_encoded_data[3] = (uint8_t) (value & 0xFF);

    return sizeof(uint32_t);
}

/*
 * @brief Function for encoding an int32 value in big-endian format.
 *
 * @param[in]   value            Value to be encoded.
 * @param[out]  p_encoded_data   Buffer where the encoded data will be written.
 *
 * @return      Number of bytes written.
 */
static inline uint8_t int32BigEncode(int32_t value, uint8_t *p_encoded_data)
{
    p_encoded_data[0] = (uint8_t) (value >> 24);
    p_encoded_data[1] = (uint8_t) (value >> 16);
    p_encoded_data[2] = (uint8_t) (value >> 8);
    p_encoded_data[3] = (uint8_t) (value & 0xFF);

    return sizeof(int32_t);
}

/**@brief Function for decoding a uint64 value in big-endian format.
 *
 * @param[in]   p_encoded_data   Buffer where the encoded data is stored.
 *
 * @return      Decoded value.
 */
static inline uint64_t uint64BigDecode(const uint8_t *p_encoded_data)
{
    return ( (((uint64_t)((uint8_t *)p_encoded_data)[0]) << 56) |
            (((uint64_t)((uint8_t *)p_encoded_data)[1]) << 48) |
            (((uint64_t)((uint8_t *)p_encoded_data)[2]) << 40)  |
            (((uint64_t)((uint8_t *)p_encoded_data)[3]) << 32) |
            (((uint64_t)((uint8_t *)p_encoded_data)[4]) << 24) |
            (((uint64_t)((uint8_t *)p_encoded_data)[5]) << 16) |
            (((uint64_t)((uint8_t *)p_encoded_data)[6]) << 8)  |
            (((uint64_t)((uint8_t *)p_encoded_data)[7]) << 0) );
}

/*
 * @brief Function for encoding an uint64 value in big-endian format.
 *
 * @param[in]   value            Value to be encoded.
 * @param[out]  p_encoded_data   Buffer where the encoded data will be written.
 *
 * @return      Number of bytes written.
 */
static inline uint8_t uint64BigEncode(uint64_t value, uint8_t *p_encoded_data)
{
    p_encoded_data[0] = (uint8_t) (value >> 56);
    p_encoded_data[1] = (uint8_t) (value >> 48);
    p_encoded_data[2] = (uint8_t) (value >> 40);
    p_encoded_data[3] = (uint8_t) (value >> 32);
    p_encoded_data[4] = (uint8_t) (value >> 24);
    p_encoded_data[5] = (uint8_t) (value >> 16);
    p_encoded_data[6] = (uint8_t) (value >> 8);
    p_encoded_data[7] = (uint8_t) (value & 0xFF);

    return sizeof(uint64_t);
}

/*
 * ********************************************************************
 *
 * Export Functions
 *
 * ********************************************************************
 */



#endif /* UTILS_H */
