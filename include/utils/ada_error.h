/**
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file:   ada_error.h
 * @author: Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 */

#ifndef ADA_ERROR_H
#define ADA_ERROR_H

#define ADA_ERROR_BASE            0
#define ADA_ERROR_STD_BASE        100
#define ADA_ERROR_FS_BASE         200

/** \defgroup AdaError     Error codes
@{ */
typedef enum AdaError
{
    ADA_SUCCESS                 = ADA_ERROR_BASE + 0, /**< Success */

    /*    Generic errors    */
    ADA_ERR_NO_MEMORY           = ADA_ERROR_BASE + 1, /**< Not enough memory */
    ADA_ERR_INVALID_PARAM       = ADA_ERROR_BASE + 2, /**< Invalid parameters */
    ADA_ERR_INVALID_STATE       = ADA_ERROR_BASE + 3, /**< Invalid state */
    ADA_ERR_INVALID_LENGTH      = ADA_ERROR_BASE + 4, /**< Invalid length */
    ADA_ERR_INVALID_MESSAGE     = ADA_ERROR_BASE + 5, /**< Invalid message */
    ADA_ERR_TIMEOUT             = ADA_ERROR_BASE + 6, /**< Timeout */
    ADA_ERR_NOT_IMPLEMENTED     = ADA_ERROR_BASE + 7, /**< Functionality is not implemented */
    ADA_ERR_NOT_INITED          = ADA_ERROR_BASE + 8, /**< Not initialized */
    ADA_ERR_ALRDY_INITED        = ADA_ERROR_BASE + 9, /**< Already initialized */
    ADA_ERR_IS_FULL             = ADA_ERROR_BASE + 10, /**< Object is full */
    ADA_ERR_GENERIC             = ADA_ERROR_BASE + 11, /**< Generic error */
    ADA_ERR_NO_RADIO            = ADA_ERROR_BASE + 12, /**< Radio features disabled */
    ADA_ERR_EMPTY               = ADA_ERROR_BASE + 13, /**< Object is empty */

    ADA_ERR_LAST,
    /*    End of generic errors    */

    /*  Std Module errors */
    ADA_ERR_STD_EMPTY_QUEUE     = ADA_ERROR_STD_BASE + 0,   /**< Std queue is empty */
    ADA_ERR_STD_NO_ELEMENT      = ADA_ERROR_STD_BASE + 1,   /**< No such element in queue */
    ADA_ERR_STD_ALRDY_EXIST     = ADA_ERROR_STD_BASE + 2,   /**< Element already exists */

    ADA_ERR_STD_LAST,
    /*  End of Std Module errors    */

    /*    FS errors    */
    ADA_ERR_FS_GENERIC           = ADA_ERROR_FS_BASE + 0, /**< Unexpected (not listed) error */
    ADA_ERR_FS_IO                = ADA_ERROR_FS_BASE + 1, /**< Read/write error */
    ADA_ERR_FS_NOT_APPLICABLE    = ADA_ERROR_FS_BASE + 2, /**< Operation not applicable error */
    ADA_ERR_FS_NO_OBJECT         = ADA_ERROR_FS_BASE + 3, /**< Not initialized object error */
    ADA_ERR_FS_OBJECT_EXISTS     = ADA_ERROR_FS_BASE + 4, /**< Already initialized object error */
    ADA_ERR_FS_NO_FILE           = ADA_ERROR_FS_BASE + 5, /**< No such file error */
    ADA_ERR_FS_FILE_EXISTS       = ADA_ERROR_FS_BASE + 6, /**< File exists error */
    ADA_ERR_FS_IS_A_DIR          = ADA_ERROR_FS_BASE + 7, /**< Target is a directory error */
    ADA_ERR_FS_MAX_FILES         = ADA_ERROR_FS_BASE + 8, /**< Too much files error */

    ADA_ERR_FS_LAST,
    /*    End of FS errors    */
} AdaError;
/** }@ */

/** Total number of generic error codes */
#define ADA_ERROR_TOTAL_NUM (ADA_ERR_LAST - ADA_ERROR_BASE)

/** Total number of STD error codes */
#define ADA_ERROR_STD_TOTAL_NUM (ADA_ERR_STD_LAST - ADA_ERROR_STD_BASE)


/**
 * Human-readable enum names of generic error codes.
 */
#define ADA_ERROR_ENUMVAL                                               \
    {                                                                   \
        "ADA_SUCCESS",                                                  \
        "ADA_ERR_NO_MEMORY",                                            \
        "ADA_ERR_INVALID_PARAM",                                        \
        "ADA_ERR_INVALID_STATE",                                        \
        "ADA_ERR_INVALID_LENGTH",                                       \
        "ADA_ERR_INVALID_MESSAGE",                                      \
        "ADA_ERR_TIMEOUT",                                              \
        "ADA_ERR_NOT_IMPLEMENTED",                                      \
        "ADA_ERR_NOT_INITED",                                           \
        "ADA_ERR_ALRDY_INITED",                                         \
        "ADA_ERR_IS_FULL",                                              \
        "ADA_ERR_GENERIC",                                              \
        "ADA_ERR_NO_RADIO",                                             \
        "ADA_ERR_EMPTY",                                                \
    }

/**
 * Human-readable enum names of STD wrapper eror codes.
 */
#define ADA_ERROR_STD_ENUMVAL                                   \
    {                                                           \
        "ADA_ERR_STD_EMPTY_QUEUE",                              \
        "ADA_ERR_STD_NO_ELEMENT",                               \
        "ADA_ERR_STD_ALRDY_EXIST",                              \
    }


/**
 * Human-readable enum names of file system error codes.
 */
#define ADA_ERROR_FS_ENUMVAL                                        \
    {                                                               \
        "ADA_ERR_FS_GENERIC",                                       \
        "ADA_ERR_FS_IO",                                            \
        "ADA_ERR_FS_NOT_APPLICABLE",                                \
        "ADA_ERR_FS_NO_OBJECT",                                     \
        "ADA_ERR_FS_OBJECT_EXISTS",                                 \
        "ADA_ERR_FS_NO_FILE",                                       \
        "ADA_ERR_FS_FILE_EXISTS",                                   \
        "ADA_ERR_FS_IS_A_DIR",                                      \
        "ADA_ERR_FS_MAX_FILES",                                     \
    }


/**
 * Human-readable description of generic error codes.
 */
#define ADA_ERROR_DESCRIPTION                                           \
    {                                                                   \
        "Success",                                                      \
        "Not enough memory",                                            \
        "Invalid parameters",                                           \
        "Invalid state",                                                \
        "Invalid length",                                               \
        "Invalid message",                                              \
        "Timeout",                                                      \
        "Functionality is not implemented",                             \
        "Not initialized",                                              \
        "Already initialized",                                          \
        "Object is full",                                               \
        "Generic error",                                                \
        "Radio transmissions are turned off",                           \
        "Object is empty",                                              \
    }


/**
 * Human-readable description of STD wrapper errors.
 */
#define ADA_ERROR_STD_DESCRIPTION                                  \
    {                                                                   \
        "Queue is empty",                                               \
        "Where is no such element in queue",                            \
        "Element already exists",                                       \
    }



/** Error checkers. These are always enabled unlike LOG_DEBUG_* macroses. */

/** Ensure that errorCode is always successful */
#define ADA_CHECK_ERROR_ALWAYS(errorCode)                \
    if (errorCode != ADA_SUCCESS)                        \
    {                                                    \
        LOG_PERROR("ADA_CHECK_ERROR_ALWAYS", errorCode); \
        LOG_DO_FATAL();                                  \
    }

/** Check condition. If it fails then print to log user-provided msg and stop execution  */
#define ADA_ASSERT_MSG_ALWAYS(expr, pMsg) do \
    { \
        if (!(expr)) \
        { \
            LOG_PRINT(LOG_STATIC_CTX, LOG_LEVEL_FATAL, __func__, \
                      __LINE__,  LOG_MODULE_ID, pMsg); \
            LOG_DO_FATAL(); \
            while (1) {; /** To avoid lint error */ \
            } \
        } \
    } while (0)

/** Check condition. If it fails then stop execution  */
#define ADA_ASSERT_ALWAYS(expr) \
    ADA_ASSERT_MSG_ALWAYS((expr), "Assertion failed")

/** Ensure that the code is never executed */
#define ADA_NEVER_REACHED_ALWAYS()   \
    ADA_ASSERT_MSG_ALWAYS(0, "It should be never reached")

/** Error checkers. These are enabled only in Debug build. */

#ifdef NDEBUG
    #define ADA_CHECK_ERROR(errorCode);
    #define ADA_ASSERT_MSG(expr, pMsg);
    #define ADA_ASSERT(expr);
    #define ADA_NEVER_REACHED();
#else
    #define ADA_CHECK_ERROR   ADA_CHECK_ERROR_ALWAYS
    #define ADA_ASSERT_MSG    ADA_ASSERT_MSG_ALWAYS
    #define ADA_ASSERT        ADA_ASSERT_ALWAYS
    #define ADA_NEVER_REACHED ADA_NEVER_REACHED_ALWAYS
#endif

#endif /** ADA_ERROR_H */
