/**
 *
 *       Copyright 2017 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file:   log_modules.h
 * @author: Dmitry Larchenko <dmitry.larchenko@sredasolutions.com>
 * @date: May 2, 2017
 *
 * Module IDs for logging API
 */

#ifndef LOG_MODULES_H
#define LOG_MODULES_H
#ifdef __cplusplus
extern "C"
{
#endif

#include "log_default_levels.h"

typedef enum LogModuleId
{
    LOG_MOD_LOG,            /**< Logging module */
    LOG_MOD_TEST,           /**< Unit/Integration test */
    LOG_MOD_STD,            /**< STD functions */
    LOG_MOD_FS_LOG,         /**< FS Log service */

    LOG_MOD_NUM
} LogModuleId;

#define LOG_MODULE_STRINGS \
    {                           \
        "Log",                  \
        "Test",                 \
        "Std",                  \
        "FsLog",                \
    }

extern const char *logModuleStr[];
#define LOG_MODULE_STR(moduleId) logModuleStr[moduleId]

#ifdef __cplusplus
}
#endif
#endif /* ifndef LOG_MODULES_H */
