/**
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */
/**
 * @file:   log_default_levels.h
 * @author: Dmitry Larchenko <dmitry.larchenko@sredasolutions.com>
 * @date: Nov 2, 2018
 *
 */
#ifndef LOG_DEFAULT_LEVELS_H
#define LOG_DEFAULT_LEVELS_H

#include "log_levels.h"

#define LOG_DEF_LEVEL_LOG                           STATIC_LOG_LEVEL_INFO
#define LOG_DEF_LEVEL_TEST                          STATIC_LOG_LEVEL_INFO
#define LOG_DEF_LEVEL_STD                           STATIC_LOG_LEVEL_INFO
#define LOG_DEF_LEVEL_FSL                           STATIC_LOG_LEVEL_INFO

#endif /* LOG_DEFAULT_LEVELS_H */
