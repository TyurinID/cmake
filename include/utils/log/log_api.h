/**
 *
 *       Copyright 2017 - 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file:   log_api.h
 * @author: Dmitry Larchenko <dmitry.larchenko@sredasolutions.com>
 * @date: May 2, 2017
 *
 * Logging API interface
 *
 * Each source file which is going to use logging API must define
 * LOG_MODULE_ID with one of the values defined in LogModuleId.
 *
 * Settings
 * ========
 * By default logging module is completely silent. Use macroses below
 * to enable it.
 *
 * LOG_ENABLE       -- turn ON logging macroses (error messages, etc.)
 * LOG_ENABLE_DEBUG -- turn ON debug macroses (asserts, hexdumps, etc.)
 *
 */

#ifndef LOG_API_H
#define LOG_API_H
#ifdef __cplusplus
extern "C"
{
#endif

#include "log_modules.h"
#include "log_levels.h"
#include "log_context.h"
#include <utils/ada_error.h>

#include <utils/utils.h>
#include <stdio.h>

#ifndef LOG_MODULE_ID
#error "Please define LOG_MODULE_ID before including this file"
#endif

#ifndef ADA_VERSION
#define ADA_VERSION "(no version)"
#endif /* ADA_VERSION */

#define LOG_DO_OUTPUT(pCtx, pLogBuf, logBufLen, logLevel) (pCtx)->logCb((pCtx)->pUserCtx, pLogBuf, \
                                                              logBufLen, logLevel)

#define LOG_DO_FATAL    logStopExecution

#define LOG_STATIC_CTX  (&_gLogCtx)

#define LOG_DO_SNPRINTF      snprintf
#define LOG_DO_SNPRINTF_ARGS snprintf

#define LOG_DEFAULT_LEVEL LOG_LEVEL_ALL

#define LOG_IS_ENABLED(pCtx, module, levelTest) \
    ( /*NULL != (pCtx) && */ (pCtx)->level > (levelTest) && (levelTest) <= LOG_MODULE_LEVEL)

/*
 * Text log template:
 * <LOG level>/<module>:<function name>:<line number> <LOG message>
 *
 * e.g.: "I/Log:logInit:54: Logging initialized."
 *
 */
#define LOG_PREFIX "%s/%s:%s:%u "
#define LOG_PREFIX_ARGS(level, func, line, module) LOG_LEVEL_STR(level), \
    LOG_MODULE_STR(module), \
    (func), ((unsigned int) line)


/*
 * *****************************************************************************
 * LOG_PRINT macrodefs
 * *****************************************************************************
 */
#ifdef LOG_ENABLE
#define LOG_PRINT_ARGS(pCtx, level, func, line, module, pFormat, ...) do    \
    {                                                                       \
            char tmpLogBuf[LOG_MAX_BUFFER_SIZE];                            \
            int tmpLogLen = LOG_MAX_BUFFER_SIZE - 1;                        \
                                                                            \
            tmpLogLen = LOG_DO_SNPRINTF_ARGS(tmpLogBuf,                     \
                                             (LOG_MAX_BUFFER_SIZE - 1),                 \
                                             LOG_PREFIX pFormat,                        \
                                             LOG_PREFIX_ARGS((level), (func), (line),   \
                                                             (module)),                 \
                                             ## __VA_ARGS__);                           \
            tmpLogBuf[LOG_MAX_BUFFER_SIZE - 1] = '\0';                      \
            LOG_DO_OUTPUT(pCtx, tmpLogBuf,                                  \
                          (uint32_t)(tmpLogLen < 0 ? 0 : tmpLogLen), level);   \
    } while (0)

#define LOG_PRINT(pCtx, level, func, line, module, pMsg) do                 \
    {                                                                          \
            char tmpLogBuf[LOG_MAX_BUFFER_SIZE];                            \
            int tmpLogLen = LOG_MAX_BUFFER_SIZE - 1;                        \
                                                                            \
            tmpLogLen = LOG_DO_SNPRINTF_ARGS(tmpLogBuf,                     \
                                             (LOG_MAX_BUFFER_SIZE - 1),                      \
                                             LOG_PREFIX pMsg,                                \
                                             LOG_PREFIX_ARGS((level), (func), (line),        \
                                                             (module)));                     \
            tmpLogBuf[LOG_MAX_BUFFER_SIZE - 1] = '\0';                      \
            LOG_DO_OUTPUT(pCtx, tmpLogBuf,                                  \
                          (uint32_t)(tmpLogLen < 0 ? 0 : tmpLogLen), level);                                                                 \
    } while (0)

/* End of LOG_PRINT definitions */
#else
/*
 * Disabling logs
 */
#define LOG_PRINT_ARGS(pCtx, level, func, line, module, pFormat, ...)
#define LOG_PRINT(pCtx, level, func, line, module, pMsg)

/* End of log disabling */
#endif

/*
 * *****************************************************************************
 * LOG macrodefs with levels
 * *****************************************************************************
 */

#if (STATIC_LOG_LEVEL_FATAL <= LOG_MODULE_LEVEL)
#define LOG_FATAL(pFormat, ...) \
    LOG_PRINT_ARGS(LOG_STATIC_CTX, LOG_LEVEL_FATAL, __func__, __LINE__, \
                   LOG_MODULE_ID, pFormat, ## __VA_ARGS__)
/* End of definitions for fatal level */
#else
#define LOG_FATAL_ARGS(pFormat, ...)
#endif


#if (STATIC_LOG_LEVEL_ALERT <= LOG_MODULE_LEVEL)
#define LOG_ALERT(pFormat, ...) \
    LOG_PRINT_ARGS(LOG_STATIC_CTX, LOG_LEVEL_ALERT, __func__, __LINE__, \
                   LOG_MODULE_ID, pFormat, ## __VA_ARGS__)
#else
#define LOG_ALERT_ARGS(pFormat, ...)
#endif


#if (STATIC_LOG_LEVEL_CRIT <= LOG_MODULE_LEVEL)

#define LOG_CRITICAL(pFormat, ...) \
    LOG_PRINT_ARGS(LOG_STATIC_CTX, LOG_LEVEL_CRIT, __func__, __LINE__, \
                   LOG_MODULE_ID, pFormat, ## __VA_ARGS__)
#else
#define LOG_CRITICAL(pFormat, ...)
#endif


#if (STATIC_LOG_LEVEL_ERROR <= LOG_MODULE_LEVEL)
#define LOG_ERROR(pFormat, ...) \
    LOG_PRINT_ARGS(LOG_STATIC_CTX, LOG_LEVEL_ERROR, __func__, __LINE__, \
                   LOG_MODULE_ID, pFormat, ## __VA_ARGS__)
#else
#define LOG_ERROR(pFormat, ...)
#endif


#if (STATIC_LOG_LEVEL_WARNING <= LOG_MODULE_LEVEL)
#define LOG_WARNING(pFormat, ...) \
    LOG_PRINT_ARGS(LOG_STATIC_CTX, LOG_LEVEL_WARNING, __func__, __LINE__, \
                   LOG_MODULE_ID, pFormat, ## __VA_ARGS__)
#else
#define LOG_WARNING(pFormat, ...)
#endif


#if (STATIC_LOG_LEVEL_NOTICE <= LOG_MODULE_LEVEL)
#define LOG_NOTICE(pFormat, ...) \
    LOG_PRINT_ARGS(LOG_STATIC_CTX, LOG_LEVEL_NOTICE, __func__, __LINE__, \
                   LOG_MODULE_ID, pFormat, ## __VA_ARGS__)
#else
#define LOG_NOTICE(pMsg, ...)
#endif

#if (STATIC_LOG_LEVEL_INFO <= LOG_MODULE_LEVEL)
#define LOG_INFO(pFormat, ...) \
    LOG_PRINT_ARGS(LOG_STATIC_CTX, LOG_LEVEL_INFO, __func__, __LINE__, \
                   LOG_MODULE_ID, pFormat, ## __VA_ARGS__);
#else
#define LOG_INFO(pMsg, ...)
#endif

#if (STATIC_LOG_LEVEL_DEBUG <= LOG_MODULE_LEVEL)
#define LOG_DEBUG(pMsg, ...) \
    LOG_PRINT_ARGS(LOG_STATIC_CTX, LOG_LEVEL_DEBUG, __func__, __LINE__, LOG_MODULE_ID, \
                   pMsg, ## __VA_ARGS__)
#else
#define LOG_DEBUG(pMsg, ...)
#endif

#define LOG_CUSTOM(_logLevel, pFormat, ...) \
    LOG_PRINT_ARGS(LOG_STATIC_CTX, _logLevel, __func__, __LINE__, \
                   LOG_MODULE_ID, pFormat, ## __VA_ARGS__);

/*
 * *****************************************************************************
 * LOG macrodefs debug level
 * *****************************************************************************
 */
#ifdef LOG_ENABLE_DEBUG
/*
 * Debug level logs definitions
 */
#define LOG_DEBUG_ASSERT_MSG(expr, pMsg) do \
    { \
        if (!(expr)) \
        { \
            LOG_PRINT(LOG_STATIC_CTX, LOG_LEVEL_FATAL, __func__, \
                      __LINE__,  LOG_MODULE_ID, pMsg); \
            LOG_DO_FATAL(); \
            while (1) {; /* To avoid lint error */ \
            } \
        } \
    } while (0)

#define LOG_DEBUG_ASSERT(expr) \
    LOG_DEBUG_ASSERT_MSG(LOG_STATIC_CTX, (expr), "Assertion failed")

#define LOG_DEBUG_NEVER_REACHED()   \
    LOG_DEBUG_ASSERT_MSG(LOG_STATIC_CTX, FALSE, "It should be never reached")

#define LOG_DEBUG_HEXDUMP(pData, len) do \
    { \
        if (LOG_IS_ENABLED(LOG_STATIC_CTX, LOG_MODULE_ID, LOG_LEVEL_DEBUG)) \
        { \
            logHexdump(LOG_STATIC_CTX, LOG_MODULE_ID, \
                       LOG_LEVEL_DEBUG, \
                       __func__, __LINE__,  pData, len); \
        } \
    } while (0)


/* End of debug level logs definitions*/
#else
/*
 * Disabling debug level logs
 */
#define LOG_DEBUG_ASSERT(expr)
#define LOG_DEBUG_ASSERT_MSG(expr, pMsg)
#define LOG_DEBUG_NOT_REACHED()
#define LOG_DEBUG_HEXDUMP(pData, len)
/* End of debug level */
#endif

/** Print human-readable error discription along with user-provided message to log */
#define LOG_PERROR(MSG, errorCode) \
    LOG_ERROR(MSG ": %d [%s]: %s", \
              errorCode, \
              logErrorEnumValue(errorCode), \
              logErrorDescription(errorCode))

/*
 * *****************************************************************************
 * Functions definitions
 * *****************************************************************************
 */

extern LogCtx _gLogCtx;

/**
 * Change logging level
 *
 * @param pCtx        Pointer to initialized logging context
 * @param level       New logging level
 */
static inline void logSetLevel(LogLevelId level)
{
    LOG_STATIC_CTX->level = level;
}

/** Set logging callback */
static inline void logSetCb(AdaLogCb pCb, void *pUserCtx)
{
    LOG_STATIC_CTX->logCb = pCb;
    LOG_STATIC_CTX->pUserCtx = pUserCtx;
}

/** Get human-readable error description for provided errorCode */
const char *logErrorDescription(AdaError errorCode);

/** Get human-readable enum value for provided errorCode */
const char *logErrorEnumValue(AdaError errorCode);

void logStopExecution(void);

/** Empty function used instead of real logging in case if logs are disabled with preprocessor.
 *  It helps to avoid "unused variable" warnings */
void logStub(const void *pUnused, ...);

/*
 * Text logs hex dump
 */
void logHexdump(LogCtx *pLogCtx, LogModuleId module, LogLevelId level,
                const char *pFunc, uint32_t line, const uint8_t *pData,
                uint32_t length);

/** Send message to syslog */
void logSendToSyslog(void *pUserCtx, const char *pMsg, const uint32_t size,
                                 LogLevelId level);

#ifdef __cplusplus
}
#endif

#endif /* LOG_API_H */
