/**
 *
 *       Copyright 2017 - 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */
/**
 * @file:   log_context.h
 * @author: Dmitry Larchenko <dmitry.larchenko@sredasolutions.com>
 * @date: May 2, 2017
 *
 */

#ifndef LOG_CONTEXT_H
#define LOG_CONTEXT_H

#include <stdint.h>
#include "log_levels.h"
#include "log_context.h"


#define LOG_MAX_BUFFER_SIZE (255U)
#define LOG_MAX_MESSAGE_SIZE (LOG_MAX_BUFFER_SIZE - 1)

#define LOG_HEXDUMP_COLS (8U)

/**
 * Called by logging module to log a debug message
 *
 * @param pUserCtx       user-provided context
 * @param pMsg           NULL-terminated string containing the message(s)
 */
typedef void (*AdaLogCb)(void *pUserCtx, const char *pMsg, const uint32_t size, LogLevelId level);

typedef struct LogCtx
{
    AdaLogCb   logCb;
    void       *pUserCtx;
    LogLevelId level;
} LogCtx;

#endif /* ifndef LOG_CONTEXT_H */
