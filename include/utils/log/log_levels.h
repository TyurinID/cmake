/**
 *
 *       Copyright 2017 - 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file:   log_levels.h
 * @author: Dmitry Larchenko <dmitry.larchenko@sredasolutions.com>
 * @date:   May 2, 2017
 */

#ifndef LOG_LEVELS_H
#define LOG_LEVELS_H

/**
 * @brief Static log level definitions (see rfc5424)
 */
#define STATIC_LOG_LEVEL_FATAL      0
#define STATIC_LOG_LEVEL_ALERT      1
#define STATIC_LOG_LEVEL_CRIT       2
#define STATIC_LOG_LEVEL_ERROR      3
#define STATIC_LOG_LEVEL_WARNING    4
#define STATIC_LOG_LEVEL_NOTICE     5
#define STATIC_LOG_LEVEL_INFO       6
#define STATIC_LOG_LEVEL_DEBUG      7

#define STATIC_LOG_LEVEL_NUM        8
#define STATIC_LOG_LEVEL_NONE       STATIC_LOG_LEVEL_FATAL
#define STATIC_LOG_LEVEL_ALL        STATIC_LOG_LEVEL_DEBUG

/* Log levels are compatible with unix syslog (see rfc5424) */
typedef enum LogLevelId
{
    LOG_LEVEL_FATAL     = STATIC_LOG_LEVEL_FATAL,   /**< Emergency: system is unusable */
    LOG_LEVEL_ALERT     = STATIC_LOG_LEVEL_ALERT,   /**< Alert: action must be taken immediately */
    LOG_LEVEL_CRIT      = STATIC_LOG_LEVEL_CRIT,    /**< Critical: critical conditions */
    LOG_LEVEL_ERROR     = STATIC_LOG_LEVEL_ERROR,   /**< Error: error conditions */
    LOG_LEVEL_WARNING   = STATIC_LOG_LEVEL_WARNING, /**< Warning: warning conditions */
    LOG_LEVEL_NOTICE    = STATIC_LOG_LEVEL_NOTICE,  /**< Notice: normal but significant condition */
    LOG_LEVEL_INFO      = STATIC_LOG_LEVEL_INFO,    /**< Informational: informational messages */
    LOG_LEVEL_DEBUG     = STATIC_LOG_LEVEL_DEBUG,   /**< Debug: debug-level messages */

    LOG_LEVEL_NUM       = STATIC_LOG_LEVEL_NUM,
    LOG_LEVEL_NONE      = STATIC_LOG_LEVEL_NONE,
    LOG_LEVEL_ALL       = STATIC_LOG_LEVEL_ALL,
} LogLevelId;

#define LOG_LEVEL_STRINGS \
    {                     \
        "FATAL",          \
        "ALERT",          \
        "CRITICAL",       \
        "ERROR",          \
        "WARNING",        \
        "NOTICE",         \
        "INFO",           \
        "DEBUG"           \
    }

#define LOG_LEVEL_FMT     \
    {                     \
        "FATAL",          \
        "ALERT",          \
        "CRIT",           \
        "EE",             \
        "W",              \
        "N",              \
        "I",              \
        "D"               \
    }

extern const char *logLevelFmt[];
#define LOG_LEVEL_STR(levelId) logLevelFmt[levelId]

#endif /* ifndef LOG_LEVELS_H */
