/*
 *
 *       Copyright 2019 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_math.h
 * @author  Ivan Tiurn <ivan.tiurin@sreda.solutions>
 *
 * @brief   Base math functions
 *
 */

#ifndef ADA_MATH_H
#define ADA_MATH_H

#include <cmath>

#include <std/ada_math_lut.h>
#include <utils/utils.h>
#include <utils/ada_error.h>
#include <ada_common.h>

/*
 * Use trigonometric look-up tables
 */
#define ADA_MATH_USE_LUT
#undef  ADA_MATH_USE_LUT_LINEAR_INTERPOLATION /* More accurate but slow */
#undef  ADA_MATH_USE_LUT_FULL_DOMAIN          /* If not defined, sin(x):       0 < x < 2 * PI,
                                                                 cos(x): -PI / 2 < x < 2 * PI,
                                                                 tan(x):       0 < x < 2 * PI,
                                                                 atan(x): any x.
                                                 If defined, x can be any. */

#define ADA_MATH_LONGITUDE_MAX       (1800000000)
#define ADA_MATH_ONE_TENTH_MICRODEGREE_LONG_EQUATOR_CM       (1.1132) /* 1 degree = 111.32 km */
#define ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM        (1.11) /* 1 degree = 111 km */
#define ADA_MATH_ONE_TENTH_MICRODEGREE_LONG_EQUATOR_CM_SQRD  (1.239214) /* 1.1132F^2 */
#define ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM_SQRD   (1.2321) /* 1.11F^2 */
#define ADA_MATH_RADIAN_COEFF (M_PI / 1800000000)
#define ADA_MATH_RADIAN_COEFF_HEADING ((M_PI) / 1800)
#define ADA_MATH_COORDINATES_DIFF_MIN_CM   (50)

#define ADA_MATH_ANGLE_0_DEGREE          (0)
#define ADA_MATH_ANGLE_90_DEGREE         (900)
#define ADA_MATH_ANGLE_180_DEGREE        (1800)
#define ADA_MATH_ANGLE_270_DEGREE        (2700)
#define ADA_MATH_ANGLE_360_DEGREE        (3600)

#define ADA_MATH_PARALLEL_TAN_DIFF      (0.001)


class AdaMath
{
public:
    inline static double lookupSin(double x)
    {
        assert(x >= LUT_SIN_X_MIN);
        assert(x <= LUT_SIN_X_MAX);

        const double idx = LUT_SIN_K * x + LUT_SIN_ADD;
        const unsigned idx0 = (unsigned)idx;

#ifdef ADA_MATH_USE_LUT_LINEAR_INTERPOLATION

        if (idx0 == LUT_SIN_IDX_MAX)
        {
            return lutSin[idx0];
        }

        const unsigned idx1 = idx0 + 1;

        return lutSin[idx0] * (idx1 - idx) + lutSin[idx1] * (idx - idx0);
#else
        return lutSin[idx0];
#endif
    }

    inline static double lookupTan(double x)
    {
        assert(x >= 0);    /* Using 0    instead of LUT_TAN_X_MIN because x is shifted */
        assert(x <= M_PI); /* Using M_PI instead of LUT_TAN_X_MAX because x is shifted */

        const double idx = LUT_TAN_K * x; /* Not using LUT_TAN_ADD because x is shifted */
        const unsigned idx0 = (unsigned)idx;

#ifdef ADA_MATH_USE_LUT_LINEAR_INTERPOLATION

        if (idx0 == LUT_TAN_IDX_MAX)
        {
            return lutTan[idx0];
        }

        const unsigned idx1 = idx0 + 1;

        return lutTan[idx0] * (idx1 - idx) + lutTan[idx1] * (idx - idx0);
#else
        return lutTan[idx0];
#endif
    }

    inline static double lookupAtan(double x)
    {
        assert(x >= LUT_ATAN_X_MIN);
        assert(x <= LUT_ATAN_X_MAX);

        const double idx = LUT_ATAN_K * x + LUT_ATAN_ADD;
        const unsigned idx0 = (unsigned)idx;

#ifdef ADA_MATH_USE_LUT_LINEAR_INTERPOLATION

        if (idx0 == LUT_ATAN_IDX_MAX)
        {
            return lutAtan[idx0];
        }

        const unsigned idx1 = idx0 + 1;

        return lutAtan[idx0] * (idx1 - idx) + lutAtan[idx1] * (idx - idx0);
#else
        return lutAtan[idx0];
#endif
    }

    /**
     * @brief This function computes sine
     * @param[in]   Value representing an angle expressed in radians.
     *              One radian is equivalent to 180/PI degrees.
     * @return      Sine of x radians
     */
    inline static double sin(double x)
    {
#ifdef ADA_MATH_USE_LUT_FULL_DOMAIN

        if (x < 0)
        {
            x = -x + M_PI;
        }

        x = x - (int)(x / 2 / M_PI) * 2 * M_PI;
#else
        assert(x >= 0);
        assert(x <= 2 * M_PI);
#endif

        return lookupSin(x);
    }

    /**
     * @brief This function computes cosine
     * @param[in]   Value representing an angle expressed in radians.
     *              One radian is equivalent to 180/PI degrees.
     * @return      Cosine of x radians
     */
    inline static double cos(double x)
    {
#ifdef ADA_MATH_USE_LUT_FULL_DOMAIN

        if (x < 0)
        {
            x = -x;
        }

        x = x - (int)(x / 2 / M_PI) * 2 * M_PI + M_PI / 2;
#else
        assert(x >= -M_PI / 2);
        assert(x <= 2 * M_PI);

        x = (x >= 3 * M_PI / 2) ? x - 3 * M_PI / 2 :
            x + M_PI / 2;
#endif

        return lookupSin(x);
    }

    /**
     * @brief This function computes tangent
     * @param[in]   Value representing an angle expressed in radians.
     *              One radian is equivalent to 180/PI degrees.
     * @return      Tangent of x radians
     */
    inline static double tan(double x)
    {
#ifdef ADA_MATH_USE_LUT_FULL_DOMAIN

        if (x < 0)
        {
            x = x - M_PI / 2;
            x = x - (int)(x / M_PI) * M_PI;
            x = x + M_PI;
        }
        else
        {
            x = x + M_PI / 2;
            x = x - (int)(x / M_PI) * M_PI;
        }

#else
        assert(x >= 0);
        assert(x <= 2 * M_PI);

        if (x >= 3 * M_PI / 2)
        {
            x = x - 3 * M_PI / 2;
        }
        else if (x >= M_PI / 2)
        {
            x = x - M_PI / 2;
        }
        else
        {
            x = x + M_PI / 2;
        }

#endif

        return lookupTan(x);
    }

    /**
     * @brief This function computes arc tangent
     * @param[in]   Value whose arc tangent is computed
     * @return      Principal arc tangent of x, in the interval [-pi/2,+pi/2]
     *              radians.
     *              One radian is equivalent to 180/PI degrees.
     */
    inline static double atan(double x)
    {
        if (x < LUT_ATAN_X_MIN)
        {
            x = LUT_ATAN_X_MIN;
        }
        else if (x > LUT_ATAN_X_MAX)
        {
            x = LUT_ATAN_X_MAX;
        }

        return lookupAtan(x);
    }


    /**
     * @brief This function calculates length of 1/10 microdegree of longitude in Cm
     * @param[in]   pos     - coordinates of intrested point area
     * @return      length in centimeters
     */
    inline static double calcLonLengthCm(const AdaPosition &pos)
    {
        return  ADA_MATH_ONE_TENTH_MICRODEGREE_LONG_EQUATOR_CM *
                cos(pos.lat * ADA_MATH_RADIAN_COEFF);
    }


    /**
     * @brief This function checks if target point is within circle
     * @param[in]   target    target point coordinates in 1/10 microdegree
     * @param[in]   circle    circle centre coordinates in 1/10 microdegree
     * @param[in]   radius_cm circle radius in centimeters
     * @return      true    target point is in circle
     * @return      false   target point is out of circle
     */
    inline static bool isInCircle(const AdaPosition target, const AdaPosition circle,
                            const uint32_t radius_cm)
    {
        return isInCircle(target, circle, radius_cm, ADA_MATH_ONE_TENTH_MICRODEGREE_LONG_EQUATOR_CM_SQRD *
                                cos(circle.lat * ADA_MATH_RADIAN_COEFF) *  cos(circle.lat * ADA_MATH_RADIAN_COEFF));
    }

    /**
     * @brief This function checks if target point is within circle
     * @param[in]   target          target point coordinates in 1/10 microdegree
     * @param[in]   circle          circle centre coordinates in 1/10 microdegree
     * @param[in]   radius_cm       circle radius in centimeters
     * @param[in]   lonLengthSqrtCm length of longitude 1/10 microdegree on current latitude in cm
     * @return      true    target point is in circle
     * @return      false   target point is out of circle
     */
    inline static bool isInCircle(const AdaPosition target, const AdaPosition circle,
                            const uint32_t radius_cm, const double lonLengthSqrtCm)
    {
        if ( uint64_t(double(target.lon - circle.lon) * double(target.lon - circle.lon)
                      * lonLengthSqrtCm)
             +
             uint64_t( double(target.lat - circle.lat) * double(target.lat - circle.lat)
                       * ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM_SQRD )
             <= (uint64_t)radius_cm * (uint64_t)radius_cm )
        {
            return true;
        }

        return false;
    }


    /**
     * @brief This function calculates coordinates of new point B from
     * originate point A coordinates, heading and radius in cm
     * @param[in]   pointA          point A coordinates in 1/10 microdegree
     * @param[in]   heading         heading from point A in 1/10 degree
     * @param[in]   radiusCm        radius in centimeters
     * @return      AdaPosition of point B in 1/10 microdegree
     */
    inline static AdaPosition coordFromPointAndHeading(const AdaPosition pointA, uint16_t heading,
                                         const uint32_t radiusCm)
    {
        return coordFromPointAndHeading(pointA, heading, radiusCm,
                                        calcLonLengthCm(pointA));
    }

    /**
     * @brief This function calculates coordinates of new point B from
     * originate point A coordinates, heading and radius in cm
     * @param[in]   pointA          point A coordinates in 1/10 microdegree
     * @param[in]   heading         heading from point A in 1/10 degree
     * @param[in]   radiusCm        radius in centimeters
     * @param[in]   lonLengthCm     length of longitude 1/10 microdegree on current latitude in cm
     * @return      AdaPosition of point B in 1/10 microdegree
     */
    inline static AdaPosition coordFromPointAndHeading(const AdaPosition pointA, uint16_t heading,
                                         const uint32_t radiusCm, const double lonLengthCm)
    {
        AdaPosition pointB = {};

        heading %= ADA_MATH_ANGLE_360_DEGREE;

        if (lonLengthCm < 0.001 * ADA_MATH_ONE_TENTH_MICRODEGREE_LONG_EQUATOR_CM)
        {
            pointB.lon = ADA_MATH_LONGITUDE_MAX;
        }
        else
        {
            pointB.lon =
                int32_t(static_cast<double>(pointA.lon) + static_cast<double>(radiusCm) *
                        sin(static_cast<double>(heading) * ADA_MATH_RADIAN_COEFF_HEADING)
                        / (lonLengthCm));
        }

        pointB.lat =
            int32_t(static_cast<double>(pointA.lat) + static_cast<double>(radiusCm) *
                    cos(static_cast<double>(heading) * ADA_MATH_RADIAN_COEFF_HEADING)
                    / (ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM));
        return pointB;
    }

    /**
     * @brief This function calculates distance between point A and B
     * @param[in]   pointA          line A point coordinates in 1/10 microdegree
     * @param[in]   pointB          line B point coordinates in 1/10 microdegree
     * @param[in]   lonLengthCm     length of longitude 1/10 microdegree on current latitude in cm
     * @return      distance between point A and B in centimeters
     */
    static uint32_t distBetweenPoints(AdaPosition pointA, AdaPosition pointB,
                                      double lonLengthSqrtCm)
    {
        return uint32_t(sqrt( (double(pointB.lon - pointA.lon) * double(pointB.lon - pointA.lon)
                               * lonLengthSqrtCm)
                              +
                              (double(pointB.lat - pointA.lat) * double(pointB.lat - pointA.lat)
                               * ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM_SQRD) ));
    }

    /**
     * @brief This function calculates distance between point A and B
     * @param[in]   pointA          line A point coordinates in 1/10 microdegree
     * @param[in]   pointB          line B point coordinates in 1/10 microdegree
     * @return      distance between point A and B in centimeters
     */
    static uint32_t distBetweenPoints(AdaPosition pointA, AdaPosition pointB)
    {
        double lonLengthSqrtCm = ADA_MATH_ONE_TENTH_MICRODEGREE_LONG_EQUATOR_CM_SQRD *
                cos(pointA.lat * ADA_MATH_RADIAN_COEFF) *  cos(pointA.lat * ADA_MATH_RADIAN_COEFF);

        return uint32_t(sqrt( (double(pointB.lon - pointA.lon) * double(pointB.lon - pointA.lon)
                               * lonLengthSqrtCm)
                              +
                              (double(pointB.lat - pointA.lat) * double(pointB.lat - pointA.lat)
                               * ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM_SQRD) ));
    }


    /**
     * @brief This function calculates heading from point to point
     * @param[in]   pointFrom       point "from" coordinates in 1/10 microdegree
     * @param[in]   pointTo         point "to" coordinates in 1/10 microdegree
     * @return      distance between point A and B in centimeters
     */
    static uint16_t headingFromPointToPoint(AdaPosition pointFrom, AdaPosition pointTo)
    {
        return headingFromPointToPoint(pointFrom, pointTo,
                                calcLonLengthCm(pointFrom));
    }

    /**
     * @brief This function calculates heading from point to point
     * @param[in]   pointFrom       point "from" coordinates in 1/10 microdegree
     * @param[in]   pointTo         point "to" coordinates in 1/10 microdegree
     * @param[in]   lonLengthCm     length of longitude 1/10 microdegree on current latitude in cm
     * @return      distance between point A and B in centimeters
     */
    static uint16_t headingFromPointToPoint(AdaPosition pointFrom, AdaPosition pointTo,
                                     const double lonLengthCm)
    {
        double y = (double(pointTo.lon - pointFrom.lon) * lonLengthCm);
        double x = (double(pointTo.lat - pointFrom.lat) * ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM);

        if (ABS(int32_t(x)) <= ADA_MATH_COORDINATES_DIFF_MIN_CM)
        {
            if (y >= 0)
            {
                return ADA_MATH_ANGLE_90_DEGREE;
            }
            else
            {
                return ADA_MATH_ANGLE_270_DEGREE;
            }
        }

        if (ABS(int32_t(y)) <= ADA_MATH_COORDINATES_DIFF_MIN_CM)
        {
            if (x >= 0)
            {
                return ADA_MATH_ANGLE_0_DEGREE;
            }
            else
            {
                return ADA_MATH_ANGLE_180_DEGREE;
            }

        }

        if (x > ADA_MATH_COORDINATES_DIFF_MIN_CM &&
                y > ADA_MATH_COORDINATES_DIFF_MIN_CM)
        {
            return uint16_t(atan(y / x) / ADA_MATH_RADIAN_COEFF_HEADING);
        }
        else if (x < -ADA_MATH_COORDINATES_DIFF_MIN_CM &&
                 y > ADA_MATH_COORDINATES_DIFF_MIN_CM)
        {
            return uint16_t(ADA_MATH_ANGLE_180_DEGREE - uint16_t(atan(-y / x) /
                                                ADA_MATH_RADIAN_COEFF_HEADING));
        }
        else if (x < -ADA_MATH_COORDINATES_DIFF_MIN_CM &&
                    y < -ADA_MATH_COORDINATES_DIFF_MIN_CM)
        {
            return uint16_t(ADA_MATH_ANGLE_180_DEGREE + uint16_t(atan(y / x) /
                                                ADA_MATH_RADIAN_COEFF_HEADING));
        }
        else if (x > ADA_MATH_COORDINATES_DIFF_MIN_CM &&
                 y < -ADA_MATH_COORDINATES_DIFF_MIN_CM)
        {
            uint16_t ret = uint16_t(ADA_MATH_ANGLE_360_DEGREE - uint16_t(atan(-y / x) /
                                                ADA_MATH_RADIAN_COEFF_HEADING));
            return ret != ADA_MATH_ANGLE_360_DEGREE ? ret : ADA_MATH_ANGLE_0_DEGREE;
        }

        return ADA_MATH_ANGLE_0_DEGREE;
    }

    /**
     * @brief Calculates difference between two headings
     * @param[in]   heading1    heading1 in 1/10 degree
     * @param[in]   heading2    heading2 in 1/10 degree
     * @return      difference between headings in 1/10 degree
     */
    static uint16_t diffHeadings(uint16_t heading1, uint16_t heading2)
    {
        heading1 %= ADA_MATH_ANGLE_360_DEGREE;
        heading2 %= ADA_MATH_ANGLE_360_DEGREE;

        int32_t diff = static_cast<int32_t>(heading1) - static_cast<int32_t>(heading2);

        if (diff < 0)
        {
            diff = -diff;
        }

        if (diff > ADA_MATH_ANGLE_180_DEGREE)
        {
            diff = ADA_MATH_ANGLE_360_DEGREE - diff;
        }

        return uint16_t(diff);
    }


    /**
     * @brief This function calculates intersection coordinates for two lines
     * given by points coordinates and headings
     * @param[in]   pointA          line A point coordinates in 1/10 microdegree
     * @param[in]   headingA        line A heading from point in 1/10 degree
     * @param[in]   pointB          line B point coordinates in 1/10 microdegree
     * @param[in]   headingB        line B heading from point in 1/10 degree
     * @param[out]  intersect       pointer to intersection coordinates
     * @return      true    - intersection calculated
     * @return      false   - intersection does not exist
     */
    inline static bool    getIntersection(AdaPosition pointA, uint16_t headingA,
                                    AdaPosition pointB, uint16_t headingB,
                                    AdaPosition &intersect)
    {
        return getIntersection(pointA, headingA, pointB, headingB, intersect,
                               calcLonLengthCm(pointA));
    }


    /**
     * @brief This function calculates intersection coordinates for two lines
     * given by points coordinates and headings
     * @param[in]   pointA          line A point coordinates in 1/10 microdegree
     * @param[in]   headingA        line A heading from point in 1/10 degree
     * @param[in]   pointB          line B point coordinates in 1/10 microdegree
     * @param[in]   headingB        line B heading from point in 1/10 degree
     * @param[out]  intersect       pointer to intersection coordinates
     * @param[in]   lonLengthCm     length of longitude 1/10 microdegree on current latitude in cm
     * @return      true    - intersection calculated
     * @return      false   - intersection does not exist
     */
    inline static bool    getIntersection(AdaPosition pointA, uint16_t headingA,
                                    AdaPosition pointB, uint16_t headingB,
                                    AdaPosition &intersect, const double lonLengthCm)
    {
        if (headingA >= ADA_MATH_ANGLE_360_DEGREE)      /* 360 degree => 0 degree */
        {
            headingA %= ADA_MATH_ANGLE_360_DEGREE;
        }

        if (headingB >= ADA_MATH_ANGLE_360_DEGREE)      /* 360 degree => 0 degree */
        {
            headingB %= ADA_MATH_ANGLE_360_DEGREE;
        }

        double coeffA = tan((double)headingA * ADA_MATH_RADIAN_COEFF_HEADING);
        double coeffB = tan((double)headingB * ADA_MATH_RADIAN_COEFF_HEADING);

        if (ABS(coeffA - coeffB) < ADA_MATH_PARALLEL_TAN_DIFF)
        {
            return false;
        }

        intersect.lat = int32_t(((  ((double)pointB.lon - (double)pointA.lon) * lonLengthCm
                                    + coeffA * (double)pointA.lat *
                                    ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM -
                                    coeffB * (double)pointB.lat * ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM)
                                 / (coeffA - coeffB)) / ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM);
        intersect.lon =
            int32_t( ((coeffA * (intersect.lat - pointA.lat) * ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM)
                      + pointA.lon * lonLengthCm) / lonLengthCm );
        return true;
    }


    /**
     * @brief This function check if heading from point "pointFrom"
     * head to area near point  "pointTo"
     * @param[in]   pointFrom   pointFrom coordinates in 1/10 microdegree
     * @param[in]   heading     heading in 1/10 degree
     * @param[in]   pointTo     pointTo coordinates in 1/10 microdegree
     * @param[in]   deltaAngle  possible angle deviation in 1/10 degree
     * @return      true
     * @return      false
     */
    static bool isHeadToPointDeg(AdaPosition pointFrom, uint16_t heading, AdaPosition pointTo,
                          uint16_t deltaAngle)
    {
        uint16_t headingPTP = headingFromPointToPoint(pointFrom, pointTo);

        if (diffHeadings(headingPTP, heading) <= int32_t(deltaAngle))
        {
            return true;
        }

        return false;
    }


    /**
     * @brief This function calculates distance from point to line segment
     * @param[in]   pointC      coordinates of point
     * @param[in]   pointA      coordinates of segment beginning
     * @param[in]   pointB      coordinates of segment end
     * @return      distance in centimeters
     */
    static uint32_t distPointToSegment(AdaPosition pointC,
                                     AdaPosition pointA, AdaPosition pointB)
    {
        return distPointToSegment(pointC, pointA, pointB, calcLonLengthCm(pointC));
    }

    /**
     * @brief This function calculates distance from point to line segment
     * @param[in]   pointC      coordinates of point
     * @param[in]   pointA      coordinates of segment beginning
     * @param[in]   pointB      coordinates of segment end
     * @param[in]   lonLengthCm     length of longitude 1/10 microdegree on current latitude in cm
     * @return      distance in centimeters
     */
    static uint32_t distPointToSegment(AdaPosition pointC,
                                     AdaPosition pointA, AdaPosition pointB,
                                     double lonLengthCm)
    {
       /*
        *       (Cx-Ax)(Bx-Ax) + (Cy-Ay)(By-Ay)
        *   r = -------------------------------
        *                     L^2
        *   L = sqrt( (Bx-Ax)^2 + (By-Ay)^2 )
        */


        double Bx_Ax = (pointB.lon - pointA.lon) * lonLengthCm;
        double By_Ay = (pointB.lat - pointA.lat) * ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM;
        double Cx_Ax = (pointC.lon - pointA.lon) * lonLengthCm;
        double Cy_Ay = (pointC.lat - pointA.lat) * ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM;

        double Lsqrt = (Bx_Ax) * (Bx_Ax) + (By_Ay) * (By_Ay);
        if (Lsqrt <= 0.0)
        {
            return static_cast<uint32_t> (sqrt(Cx_Ax * Cx_Ax + Cy_Ay * Cy_Ay));
        }

        double r = ( (Cx_Ax) * (Bx_Ax) + (Cy_Ay) * (By_Ay) ) / Lsqrt;


        if (r < 0.0)    /* point A is nearest */
        {
            return static_cast<uint32_t> ( sqrt((Cx_Ax) * (Cx_Ax) + (Cy_Ay) * (Cy_Ay)) );
        }
        else if (r > 1.0)    /* point B is nearest */
        {
            return static_cast<uint32_t> ( sqrt((Cx_Ax - Bx_Ax) * (Cx_Ax - Bx_Ax) + (Cy_Ay - By_Ay) * (Cy_Ay - By_Ay)) );
        }
        else
        {
            return static_cast<uint32_t> ( sqrt((Cx_Ax - r * (Bx_Ax)) * (Cx_Ax - r * (Bx_Ax)) +
                                                (Cy_Ay - r * (By_Ay)) * (Cy_Ay - r * (By_Ay))) );
        }
    }

    /**
     * @brief This function checks, if object is on lane segment.
     * Optimized for getLane().
     * @param[in]   object      coordinates of object
     * @param[in]   pointA      coordinates of segment beginning
     * @param[in]   pointB      coordinates of segment end
     * @param[in]   deltaCm     available distance from lane segment
     * @param[in]   lonLengthCm     length of longitude 1/10 microdegree on current latitude in cm
     * @return      true    - on lane segment
     * @return      false   - not on lane segment
     */
    static bool isObjectOnLaneSegment(AdaPosition object,
                                      AdaPosition pointA, AdaPosition pointB,
                                      uint32_t deltaCm,
                                      double lonLengthCm)
    {
        /*
         *       (Cx-Ax)(Bx-Ax) + (Cy-Ay)(By-Ay)
         *   r = -------------------------------
         *                     L^2
         *   L = sqrt( (Bx-Ax)^2 + (By-Ay)^2 )
         */

         double Bx_Ax = (pointB.lon - pointA.lon) * lonLengthCm;
         double By_Ay = (pointB.lat - pointA.lat) * ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM;
         double Cx_Ax = (object.lon - pointA.lon) * lonLengthCm;
         double Cy_Ay = (object.lat - pointA.lat) * ADA_MATH_ONE_TENTH_MICRODEGREE_LAT_AVERAGE_CM;


         double Lsqrt = (Bx_Ax) * (Bx_Ax) + (By_Ay) * (By_Ay);
         if (Lsqrt <= 0.0)
         {
             return sqrt(Cx_Ax * Cx_Ax + Cy_Ay * Cy_Ay) <= deltaCm;
         }

         double r = ( (Cx_Ax) * (Bx_Ax) + (Cy_Ay) * (By_Ay) ) / Lsqrt;


         if (r < 0.0 || r > 1.0)    /* point A or B is nearest */
         {
             return false;
         }
         else
         {
             return sqrt((Cx_Ax - r * (Bx_Ax)) * (Cx_Ax - r * (Bx_Ax)) +
                         (Cy_Ay - r * (By_Ay)) * (Cy_Ay - r * (By_Ay))) <= deltaCm;
         }
    }
};


#endif /* ADA_MATH_H */
