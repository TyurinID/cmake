/*
 *
 *       Copyright 2019 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_timer.h
 * @author  Igor Bykov <igor.bykov@sredasolutions.com>
 *
 * @brief   AdaTimer class
 *
 *  Timer class declaration
 *
 */

#ifndef ADA_TIMER_H
#define ADA_TIMER_H

#include <utils/ada_error.h>
#include <cstdint>

class AdaTimer {
    public:
    virtual ~AdaTimer() = default;

    typedef void (*TimerCallBack)(void *);

    /**
     * Create timer object
     *
     * @param pInst pointer to created object
     * @param name name string
     * @param timerCb callback function 
     * @param pArg callback function argument 
     * @param startDelayMs start delay in ms 
     * @param periodMs period in ms 
     * @param autoActivate flag of immediate activation 
     * @return AdaError
     */
    static AdaError create(AdaTimer **pInst, const char *name,
                           TimerCallBack timerCb, void *pArg,
                           uint32_t startDelayMs, uint32_t periodMs,
                           bool autoActivate = true);

   /**
     * Change timer parameters
     *
     * @param startDelayMs start delay in ms 
     * @param periodMs period in ms 
     * @return AdaError
     */
    virtual AdaError change(uint32_t startDelayMs, uint32_t periodMs) = 0;

   /**
     * Activate/deactivate timer
     *
     * @param v state  
     * @return AdaError
     */
    virtual AdaError activate(bool v) = 0; 

    /**
     * Get period
     *
     * @return period in ms
     */
    virtual uint32_t getPeriodMs() const = 0;

    /**
     * Self delete an obect
     */
    virtual void free() = 0;
};

#endif
