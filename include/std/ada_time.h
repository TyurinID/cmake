/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_time.h
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Time class
 *
 *  Allows geting current time in milliseconds from 1970 January 1st
 *
 */

#ifndef ADA_TIME_H
#define ADA_TIME_H
#include <cstdint>

typedef struct
{
    uint32_t min;
    uint32_t ms;
}   AdaTimeStruct;

class AdaTime
{
public:
    AdaTime();

    /**
     * @brief Form big endian timestamp 64 bits in milliseconds
     * @param pTimestamp    - pointer to buffer, where ts should be stored
     */
    static void     timestampBigEndian(uint8_t *pTimestamp);

    /**
     * @brief Get current time
     * @param pTime_    - structure to be filled
     */
    static void     getTime(AdaTimeStruct *pTime_);

    /**
     * @brief Get current time in milliseconds
     */
    static uint64_t getMs();

    /**
     * @brief Get current time in seconds
     */
    static uint32_t getS();

    /**
     * @brief Get current steady time in milliseconds
     */
    static uint64_t getSteadyMs();

    /**
     * Converts time from minutes of the year + milliseconds in current minute to
     * milliseconds since Epoch
     * @param[in]    moy        minutes of current year
     * @param[in]    ms         milliseconds in current minute
     * @return      milliseconds since Epoch
     */
    static uint64_t convertToMs(int32_t moy, uint16_t ms);

    /**
     * Converts time from tenths seconds in current or next hour
     * (according to timestampMs) to milliseconds since Epoch
     * @param[in]   tenthsSecInHour     tenths seconds in current or next hour
     * @param[in]   timestampMs
     * @return      milliseconds since Epoch
     */
    static uint64_t convertToMs(uint16_t tenthsSecInHour, uint64_t timestampMs);

    /**
     * Print current time in format [HH:MM:SS:ms] without \n
     */
    static void     printMs();

    /** Print timestamp into provided string buffer.
     * @param timestampMs - optional timestamp to use instead of system time */
    static void     sprintMs(char *pBuf, uint32_t len, uint64_t timestampMs = 0);

    /**
     * Print current time in format [HH:MM:SS:ms] with \n
     */
    static void     printLnMs();

    /**
     * @brief Get current time in microseconds
     */
    static uint64_t getUs();
};

#endif /* ADA_TIME_H */
