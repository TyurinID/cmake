/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_dynamic_queue.h
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Define class for dynamic queue
 *
 *      Queue carries data with certain size.
 */
#ifndef ADA_DYNAMIC_QUEUE_H
#define ADA_DYNAMIC_QUEUE_H

#include <std/list.h>
#include <std/ada_cond_var.h>
#include <std/ada_mutex.h>
#include <utils/ada_error.h>


class AdaDynQueue
{
public:
    explicit AdaDynQueue(uint32_t elementsMaxNum = 10);
    ~AdaDynQueue();

    /**
     * @brief Function waits for free space in queue and pushes data
     * @param[in]    pData      pointer to data to be pushed into queue
     * @param[in]    size       data size
     * @param[in]    timeoutMs  waiting time in ms (set 0 in case of infinite waiting)
     * @return      @ref AdaError
     */
    AdaError waitAndPush(uint32_t size, const uint8_t *pData, uint32_t timeoutMs = 0);

    /**
     * @brief If there is free space in queue function pushes data to queue otherwise returns ADA_ERR_IS_FULL
     * @param[in]    size_      data size
     * @param[in]    pData_     pointer to data to be pushed into queue
     * @return      @ref AdaError
     */
    AdaError push(uint32_t size, const uint8_t *pData);

    /**
     * @brief Function waits for data in queue and pops it.
     * @param[out]   pBuf       pointer to buffer for data storing
     * @param[in]    pSize      pointer to buffer size, will be overwritten by number of
     *                          poped bytes
     * @param[in]    timeMs     waiting time in ms (set 0 in case of infinite waiting)
     * @return      @ref AdaError
     */
    AdaError waitAndPop(uint8_t *pBuf, uint32_t *pSize, uint32_t timeMs = 0);

    /**
     * @brief Function checks for available data in queue and pops it.
     * @param[out]   pBuf       pointer to buffer for data storing
     * @param[in]    pSize      pointer to buffer size, will be overwritten by number of
     *                          poped bytes
     * @param[in]    timeMs     waiting time in ms (set 0 in case of infinite waiting)
     * @return      @ref AdaError
     */
    AdaError pop(uint8_t *pBuf, uint32_t *pSize);

    /**
     * @brief Function waits for data in queue and get it without copying.
     * @param[out]   pBuf       pointer to buffer for storing address to data
     * @param[in]    pSize      pointer will be overwritten by number of gotten bytes
     * @param[in]    timeMs     waiting time in ms (set 0 in case of infinite waiting)
     * @return      @ref AdaError
     */
    AdaError waitAndGet(uint8_t **pBuf, uint32_t *pSize, uint32_t timeMs = 0);

    /**
     * @brief Function checks for available data in queue and get it without copying.
     * @param[out]   pBuf       pointer to buffer for storing address to data
     * @param[in]    pSize      pointer will be overwritten by number of gotten bytes
     * @return      @ref AdaError
     */
    AdaError get(uint8_t **pBuf, uint32_t *pSize);

    /**
     * @brief Function waits for data in queue and returns its size.
     * @param[in]    timeoutMs     waiting time in ms (set 0 in case of infinite waiting)
     * @return  data size in bytes
     * @return  0  - waiting timeout
     * @return  -1 - error
     */
    int32_t  waitAndGetDataSize(uint32_t timeMs = 0);

    /**
     * @brief Function checks for available data in queue and returns its size.
     * @return  data size in bytes
     * @return  0 - no data is available
     */
    uint32_t getDataSize();

    /**
     * @brief Function waits for data in queue and delete it.
     * @param[in]   timeoutMs     waiting time in ms (set 0 in case of infinite waiting)
     * @return      @ref AdaError
     */
    AdaError waitAndDelete(uint32_t timeoutMs = 0);

    /**
     * @brief If there is data in queue function deletes upper element otherwise returns ADA_ERR_STD_EMPTY_QUEUE
     * @return      @ref AdaError
     */
    AdaError del();

    /**
     * @brief Set maximum number of elements
     * @param num
     */
    void     setElementsMaxNum(uint32_t num);

    /**
     * @brief Get maximum number of elements
     * @return maximum number of elements
     */
    uint32_t getElementsMaxNum()
    {
        return _elementsMaxNum;
    }

    /**
     * @brief Function to obtain the number of currently stored elements
     * @return      number of elements
     */
    uint32_t getElementsNum();


private:
    AdaDynQueue(const AdaDynQueue&);                 /**< Prevent copy-construction */
    AdaDynQueue& operator=(const AdaDynQueue&);      /**< Prevent assignment */

    AdaError waitAndGetGeneral(void *pBuf, uint32_t *pSize, uint32_t timeMs, bool shouldGet);
    AdaError getGeneral(void *pBuf, uint32_t *pSize, bool shouldGet);

    ListHead    _queueHead;
    uint32_t    _elementsMaxNum;
    uint32_t    _elementsNum;
    AdaCondVar  _condVar;
};

#endif /* ADA_DYNAMIC_QUEUE_H */
