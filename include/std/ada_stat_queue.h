/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 *******************************************************************************
 *
 *  @file   ada_stat_queue.h
 *  @author Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *  @brief  AdaStatQueue class API
 *
 *  Static Queue (FIFO) stores fixed max number of fixed size elements in heap
 *
 *
 *******************************************************************************
 */

#ifndef ADA_STATIC_QUEUE_H
#define ADA_STATIC_QUEUE_H

#include <utils/ada_error.h>
#include <cstdint>
#include <std/ada_cond_var.h>

template <typename T>
class AdaStatQueuePriv;

template <typename T>
class AdaStatQueue
{
public:    
    /**
     * @brief Static queue constructor allocates memory for all elements
     * @param[in]    queueSize      max number of elements to be stored in queue
     * @return      Ada_SUCCESS
     * @return      Ada_ERR_INVALID_PARAM
     * @return      Ada_ERR_NO_MEMORY
     */
    AdaStatQueue(uint32_t queueSize);

    /** Destructor */
    ~AdaStatQueue();

    /**
     * @brief Function waits for space in queue and push data to it
     * @param[in]    elem           data to be pushed into queue
     * @param[in]    timeoutUs      waiting time in us (set 0 in case of infinite waiting)
     * @return      Ada_SUCCESS
     * @return      Ada_ERR_TIMEOUT     - timeout during waiting for space in queue
     * @return      @ref AdaError
     */
    AdaError waitAndPush(const T &elem, uint32_t timeoutUs = 0);

    /**
     * @brief Function tries pushing data to queue
     * @param[in]    elem           data to be pushed into queue
     * @return      Ada_SUCCESS
     * @return      Ada_ERR_IS_FULL     - queue is full
     * @return      @ref AdaError
     */
    AdaError push(const T &elem);

    /**
     * @brief Function waits for some data in queue and gets number of elements
     * @param[in]    num          pointer for storing number of elements
     * @param[in]    timeoutUs      waiting time in us (set 0 in case of infinite waiting)
     * @return      Ada_SUCCESS
     * @return      Ada_ERR_TIMEOUT     - timeout during waiting for data in queue
     * @return      @ref AdaError
     */
    AdaError waitAndGetElemNum(uint32_t &num, uint32_t timeoutMs);

    /**
     * @brief Function gets number of elements stored in queue
     * @param[in]    num          pointer for storing number of elements
     * @return      Ada_SUCCESS
     */
    AdaError getElemNum(uint32_t &num);

    /**
     * @brief Function gets pointer to element in queue according to its number in queue
     * @param[in]    pElem          pointer for storing element pointer
     * @param[in]    elemCnt        elemnt number in queue (0 - first pushed)
     * @return      Ada_SUCCESS
     * @return      Ada_ERR_STD_NO_ELEMENT - empty queue or no such element in it
     */
    AdaError get(T **pElem, uint32_t elemCnt = 0);

    /**
     * @brief Function pops FIFO element from queue
     * @param[in]    pElem      pointer to element for storing
     * @return      Ada_SUCCESS
     * @return      Ada_ERR_STD_EMPTY_QUEUE
     */
    AdaError pop(T *pElem);

    /**
     * @brief Function deletes FIFO element in queue
     * @return      Ada_SUCCESS
     * @return      Ada_ERR_STD_EMPTY_QUEUE
     */
    AdaError pop();

    /**
     * @brief Function deletes all elements in queue
     */
    void clear();

private:
    AdaStatQueuePriv<T> *_pPriv;
    uint32_t    _queueSize;     /* Maximum number of elements in queue */
    uint32_t    _numElems;      /* Current number of elements in queue */
    uint32_t    _pushCnt;       /* Counter for push new element */
    uint32_t    _popCnt;        /* Counter for pop FI element */
    T           *_pBuffer;
    AdaCondVar  _condVar;
};

#include <common/std/ada_stat_queue.cpp>

#endif /* ADA_STATIC_QUEUE_H */
