/**
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 *******************************************************************************
 *
 *  @file ada_task.h
 *  @author Dmitry Larchenko <dmitry.larchenko@sredasolutions.com>
 *  @brief Task interface
 *
 *  Defines AdaTask class
 *
 *******************************************************************************
 */

#ifndef ADA_TASK_H
#define ADA_TASK_H

#include <cstdint>
#include <utils/ada_error.h>

typedef void *(AdaTaskRunnable)(void *);

enum AdaTaskPriority{
    ADA_TASK_PRIORITY_HIGHEST,
    ADA_TASK_PRIORITY_HIGH,
    ADA_TASK_PRIORITY_MEDIUM_HIGH,
    ADA_TASK_PRIORITY_MEDIUM,
    ADA_TASK_PRIORITY_MEDIUM_LOW,
    ADA_TASK_PRIORITY_LOW,
    ADA_TASK_PRIORITY_LOWEST,
};

class AdaTaskPrivate;
class AdaTask
{
public:
    explicit AdaTask(AdaTaskRunnable *pRunnable,
                     const char *pTaskName = nullptr,
                     AdaTaskPriority priority = ADA_TASK_PRIORITY_MEDIUM);
    virtual ~AdaTask();

    /**
     * @brief Start thread and wait till it running
     * @return     @ref AdaError
     */
    virtual AdaError start();

    /**
     * @brief Virtual stop function (Should be implemented)
     */
    virtual void stop() = 0;

    /**
     * @brief Checks if task is running
     * @return true     - task is running
     * @return false    - task is stoped
     */
    bool isRunning();

    /**
     * @brief Wait for task end after stop()
     */
    void join();

    /**
     * @brief Main task process
     * @param pArg  - pointer to this class
     * @return
     */
    static void *run(void *pArg);

private:
    AdaTask(const AdaTask&);                 /**< Prevent copy-construction */
    AdaTask& operator=(const AdaTask&);      /**< Prevent assignment */

    AdaTaskPrivate *_pPriv;
};

#endif /* ADA_TASK_H */
