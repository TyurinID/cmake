/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_cond_var.h
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Condition variable class
 *
 *
 */
#ifndef ADA_COND_VAR_H
#define ADA_COND_VAR_H

#include <cstdint>
#include <utils/ada_error.h>

class AdaCondVarPrivate;
class AdaCondVar
{
public:
    explicit AdaCondVar(const char *pName = nullptr);
    ~AdaCondVar();

    /**
     * @brief Lock condition variable mutex
     */
    void    lockMutex();

    /**
     * @brief Unlock condition variable mutex
     */
    void    unlockMutex();

    /**
     * @brief Infinite wait for signal() or broadcast()
     */
    void    wait();

    /**
     * @brief Wait for signal() or broadcast() with timeout
     * @param   useconds    - timeout in microseconds
     */
    AdaError timedWait(uint32_t useconds); /* For Unex platform: supported time granularity by now is 1 millisecond so the parameter should be more than 1000 */

    /**
     * @brief Signal
     */
    void    signal();

    /**
     * @brief Broadcast
     */
    void    broadcast();
private:
    AdaCondVarPrivate *mPriv;
};


#endif /* ADA_COND_VAR_H */
