/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_mutex.h
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Mutex class
 *
 */
#ifndef ADA_MUTEX_H
#define ADA_MUTEX_H

#ifdef __CRATON__
#include <tx_posix.h>
#else                   /* __CRATON__ */
#include <pthread.h>
#endif                  /* __CRATON__ */

#include <cstdint>

class AdaMutexPrivate;
class AdaMutex
{
    friend class AdaCondVar;
public:
    explicit AdaMutex();
    ~AdaMutex();

    /**
     * @brief Lock mutex
     */
    void lock();

    /**
     * @brief Unlock mutex
     */
    void unlock();
private:
    AdaMutexPrivate *mPriv;
};

class AdaLockGuard
{
public:
    /**
     * @brief Lock guard mutex
     * @param mutex - mutex to be used as guard
     */
    AdaLockGuard(AdaMutex &mutex) : _mutex(mutex)
    {
        mutex.lock();
    }
    ~AdaLockGuard()
    {
        _mutex.unlock();
    }
    AdaLockGuard() = delete;
private:
    AdaMutex& _mutex;
};

#endif /* ADA_MUTEX_H */
