/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_sleep.h
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Sleep class
 *
 */
#ifndef ADA_SLEEP_H
#define ADA_SLEEP_H

#include <cstdint>

class AdaSleep
{
public:
    AdaSleep();

    /**
     * @brief Sleep for uSec microseconds
     * @param uSec
     */
    static void uSleep(uint64_t uSec);

    /**
     * @brief Sleep for sec seconds
     * @param sec
     */
    static void sSleep(uint32_t sec)
    {
        mSleep(sec * 1000);
    }

    /**
     * @brief Sleep for mSec milliseconds
     * @param mSec
     */
    static inline void mSleep(uint32_t mSec)
    {
        uSleep(mSec * 1000);
    }
};

#endif /* ADA_SLEEP_H */
