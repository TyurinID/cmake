/**
 *
 *       Copyright 2017 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */
/**
 * @file    list.h
 * @author  Pavel Vasilyev <pavel.vasilyev@sredasolutions.com>
 *
 * Contains implementation of Double Linked List
 *
 * Created on March 7, 2017, 12:20 AM
 */

#ifndef LIST_H
#define LIST_H

#include <cstddef>

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

/**
 * ListHead structure
 */
typedef struct ListHead
{
    struct ListHead *prev; /**< Pointer to previous list item */
    struct ListHead *next; /**< Pointer to next list item */
} ListHead;

/**
 * Initialize list head
 * @param head pointer to head of the list
 */
static inline void listInitHead(ListHead *head)
{
    head->next = head;
    head->prev = head;
}

/**
 * @brief Initialize list entry.
 *
 * List entry in contrast to head intialized with NULL.
 *
 * @param pEntry pointer to head of the list
 */
static inline void listInitEntry(ListHead *pEntry)
{
    pEntry->next = nullptr;
    pEntry->prev = nullptr;
}

/**
 * @brief Push new entry before specified entry.
 *
 * If specified entry is the head of the list, pushes new entry to the back
 * of the list, otherwise works as instert before specified entry
 *
 * @param head entry before which new entry is pushed
 * @param new_entry entry to be pushed
 */
static inline void listPushBack(ListHead *head, ListHead *new_entry)
{
    head->prev->next = new_entry;
    new_entry->next = head;
    new_entry->prev = head->prev;
    head->prev = new_entry;
}

/**
 * @brief Push new entry after specified entry.
 *
 * If specified entry is the head of the list, pushes new entry to the back
 * of the list, otherwise works as instert before specified entry
 *
 * @param head entry before which new entry is pushed
 * @param new_entry entry to be pushed
 */
static inline void listPushFront(ListHead *head, ListHead *new_entry)
{
    head->next->prev = new_entry;
    new_entry->next = head->next;
    new_entry->prev = head;
    head->next = new_entry;
}

/**
 * @brief Delete specified entry
 * @param entry pointer to entry to be deleted
 */
static inline void listDelete(ListHead *entry)
{
    if (NULL != entry->prev)
    {
        entry->prev->next = entry->next;
    }

    if (NULL != entry->next)
    {
        entry->next->prev = entry->prev;
    }

    entry->prev = entry->next = nullptr;
}

/**
 * @brief Get next entry after the specified entry
 * @param pHead pointer to head of the list
 * @param pEntry pointer to entry which next entry shall be returned
 * @return next entry if exists, NULL if the next entry is head
 */
static inline ListHead *listNext(ListHead *pHead, ListHead *pEntry)
{
    if (pEntry->next != pHead)
    {
        return pEntry->next;
    }

    return nullptr;
}

/**
 * @brief For-each loop implementation for list data-structure.
 *
 * Allows to read items during loop.
 * Insertion and deletion is NOT allowed here.
 *
 * @param head pointer to list head
 * @param entry temporary pointer that holds current entry in the loop
 */
#define listForEachEntry(head, entry) \
    for (entry = (head)->next; entry != (head); entry = entry->next)

/**
 * @brief Safe version of for-each loop implementation for list data-structure
 *
 * Allows any operation with list: read, insert, delete.
 *
 * @param head pointer to list head
 * @param entry temporary pointer that holds current entry in the loop
 * @param tmp temporary pointer for safe iteration
 * @return
 */
#define listForEachEntrySafe(head, entry, tmp) \
    for (entry = (head)->next, tmp = entry->next; entry != (head); \
         entry = tmp, tmp = entry->next)

/**
 * @brief Clear list by removing links to entries.
 * @param head pointer to head of the list
 */
static inline void listClear(ListHead *head)
{
    ListHead *entry;
    ListHead *pTmpEntry;

    listForEachEntrySafe(head, entry, pTmpEntry)
    {
        listDelete(entry);
    }
}

/**
 * @brief Check whether the list is empty or not
 * @param head pointer to head of the list
 * @return TRUE if list is empty, FALSE otherwise
 */
static inline bool listIsEmpty(ListHead *head)
{
    return head->next == head ? true : false;
}

/**
 * @brief Check whether the entry is in the list or not
 * @param pHead pointert to head of the list
 * @param pEntry pointer to entry to check
 * @return TRUE if pEntry in the list, FALSE otherwise
 */
static inline bool listContains(ListHead *pHead, ListHead *pEntry)
{
    ListHead *pTmpEntry;
    bool found = false;

    if (nullptr == pEntry->next || nullptr == pEntry->prev)
    {
        return true;
    }

    listForEachEntry(pHead, pTmpEntry)
    {
        if (pTmpEntry == pEntry)
        {
            found = true;
            break;
        }
    }

    return found;
}

/**
 * @brief Counts elements in the list
 * @param pHead pointer to head of the list
 * @return number of elements in the list
 */
static inline uint32_t listCount(ListHead *pHead)
{
    ListHead *pNextListEntry = pHead;
    uint32_t size = 0;

    pNextListEntry = listNext (pHead, pNextListEntry);

    while (NULL != pNextListEntry)
    {
        size++;
        pNextListEntry = listNext (pHead, pNextListEntry);
    }

    return size;
}

#ifdef __cplusplus
}
#endif

#endif /* LIST_H */
