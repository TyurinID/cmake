/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_fs.h
 * @author  Pavel Matyunin <pavel.matyunin@sredasolutions.com>
 *
 * @brief   FileIO API
 *
 */
#ifndef ADA_FS_H
#define ADA_FS_H

#define ADA_FS_MODE_BINARY               "b"
#define ADA_FS_MODE_READ                 "r"
#define ADA_FS_MODE_WRITE                "w"
#define ADA_FS_MODE_READWRITE            "a+"

#define ADA_FS_FILENAME_TAILMASK_SIZE    5

#define ADA_FS_UNIT_KILO                 1000
#define ADA_FS_UNIT_MEGA                 (ADA_FS_UNIT_KILO * ADA_FS_UNIT_KILO)
#define ADA_FS_MAX_FILESIZE_MB           (5 * ADA_FS_UNIT_MEGA)

#define ADA_FS_SYNC_PERIOD_SIZE          (100 * ADA_FS_UNIT_KILO)

#define ADA_FS_MICROSD_ROOT              "A:/"
#define ADA_FS_INTERNAL_ROOT             "B:/"

#include <std/ada_mutex.h>
#include <utils/ada_error.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

class AdaFs
{
public:
    AdaFs();
    ~AdaFs();

    /**
     * @brief Function to open one specific file
     * @note file modes (1) - ADA_FS_MODE_READ(or/and)WRITE - read or write in plaintext mode
     * @note file modes (2) - (one of mode in (1)) (space) ADA_FS_MODE_BINARY - in binary mode
     * @param   pFilePath                - path to the file
     * @param   pFileName                - file name
     * @param   pFileMode                - file mode
     * @param   fragmenting              - split writing data to fixed-size files
     * @param   forceOpen                - ignore fragmented operations (default = 0)
     * @return  ADA_SUCCESS
     * @return  ADA_ERR_FS_OBJECT_EXISTS - object has been initialized and it needs to be closed first
     * @return  ADA_ERR_NO_MEMORY        - RAM memory (possibly) exceeded
     * @return  ADA_ERR_FS_GENERIC       - any other not specified error (no access, etc)
     * @return  ADA_ERR_FS_MAX_FILES     - maximum amount of files for current mask exceeded
     * @return  ADA_ERR_INVALID_PARAM    - incorrect parameters
     */
    AdaError    open( const char *pFilePath, const char *pFileName, const char *pFileMode,
                      uint8_t fragmenting, uint8_t forceOpen = 0);

    /**
     * @brief Function to finish any operations with object
     * @return  ADA_SUCCESS
     * @return  ADA_ERR_FS_NO_OBJECT - object is not ready to use
     */
    AdaError    close();

    /**
     * @brief Function to read data from a single file
     * @param[out]   pData                - pointer to data set
     * @param[out]   pSize                - size of data set in bytes
     * @param[in]    offset               - read file from offset (default - 0 (= from beginning))
     * @param[in]    maxSize              - maximum size of data set in bytes (default - 0 (= to EOF)).
     * @return  ADA_SUCCESS
     * @return  ADA_ERR_FS_NO_OBJECT      - object is not ready to use
     * @return  ADA_ERR_FS_NOT_APPLICABLE - target file was opened in write-only mode
     * @return  ADA_ERR_INVALID_PARAM     - not a valid parameter
     * @return  ADA_ERR_FS_IO             - IO-operation error (???)
     */
    AdaError    read(uint8_t *pData, uint32_t *pSize, uint32_t offset = 0, uint32_t maxSize = 0);

    /**
     * @brief Function to write data to file
     * @param   pData                     - data set
     * @param   size                      - size of data set in bytes
     * @return  ADA_SUCCESS
     * @return  ADA_ERR_FS_NO_OBJECT      - object is not ready to use
     * @return  ADA_ERR_FS_NOT_APPLICABLE - target file was opened in read-only mode
     * @return  ADA_ERR_FS_MAX_FILES      - maximum amount of files for current mask exceeded
     * @return  ADA_ERR_FS_IO             - IO-operation error (storage memory exceeded)
     * @return  ADA_ERR_FS_GENERIC        - any other not specified error (no access, etc)
     */
    AdaError    write(const uint8_t *pData, uint32_t size);

    /**
     * @brief Function to sync IO on device
     * @return  ADA_SUCCESS
     * @return  ADA_ERR_FS_IO             - IO-operation error (storage memory exceeded)
     * @return  ADA_ERR_FS_GENERIC        - any other not specified error (no access, etc)
     */
    AdaError    sync();

    /**
     * @brief Function to rewind file pointer. Should be used when we need to rewrite file
     * @return  ADA_SUCCESS
     * @return  ADA_ERR_FS_NO_OBJECT      - object is not ready to use
     * @return  ADA_ERR_FS_GENERIC        - any other not specified error (no access, etc)
     */
    AdaError    rewind();

    /**
     * @brief Function to get size of currently opened file
     * @return File size in bytes
     */
    uint32_t    getSize();

private:
    FILE *_pFd;
    char *_pMode;
    int16_t _status;
    uint16_t _fileId;
    uint32_t _filePos;
    uint8_t _fragmented;
    char *_pFileActualPath;
    char *_pFileActualName;
    uint8_t _fileModeLength;
    size_t _fileActualPathLength;
    size_t _fileActualNameLength;

    AdaMutex _mutex;

    void       inClose();
    AdaError   next();
    AdaError   safeOpen(uint8_t forceOpen = 0);
    AdaError   findLast();
    void       destroyAllocs();
};


#endif /* ADA_FS_H */
