/**
 *
 *       Copyright 2019 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */
 
/**
 * @file    test.h
 * @author  Ivan Ivanov <ivan.ivanov@sredasolutions.com>
 *
 * @brief   Just an example
 *
 */

#ifndef AUTODATA2_EMBEDDED_TEST_H
#define AUTODATA2_EMBEDDED_TEST_H
#define UNUSED_VARIABLE(X)  ((void)(X))
#define UNUSED_PARAMETER(X) UNUSED_VARIABLE(X)
#define UNUSED_RETURN_VALUE(X) UNUSED_VARIABLE(X)

int testf(void);
#endif //AUTODATA2_EMBEDDED_TEST_H


