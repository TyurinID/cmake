/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 * @file    ada_common.h
 * @author  Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *
 * @brief   Common types
 *
 */

#ifndef ADA_COMMON_H
#define ADA_COMMON_H

#include <stdint.h>
#include <inttypes.h>
#include <time.h>
/**
 * A.119 DF_PosConfidenceEllipse (A.67 + A.35)
 *
 * DF that provides the horizontal position accuracy in a shape of ellipse with a
 * predefined confidence level (e.g. 95 %). The centre of the ellipse shape corresponds
 * to the reference position point for which the position accuracy is evaluated.
 */
struct AdaPositionConfidence {
    uint16_t semiMajorConfidence;
    uint16_t semiMinorConfidence;
    uint16_t semiMajorOrientation;
};

/**
 * A.124 DF_ReferencePosition (A.41 + A.44 + A.119 - A.103)
 *
 * The geographical position of a position or of an ITS-S.
 * It represents a geographical point position.
 */
struct AdaPosition
{
    int32_t lat;  /* INTEGER (-900000000..900000001 "1/10 micro degree") */
    int32_t lon;  /* INTEGER (-1799999999..1800000001 "1/10 micro degree")*/
};

struct AdaPositionExt : AdaPosition
{
    AdaPositionConfidence conf;
};

typedef void (*RxJsonCb)(const uint8_t *pJsonStr, uint32_t size, void *pArg);
typedef void (*RxUperCb)(const uint8_t *pUperArr, uint32_t size, void *pArg);

/* ************************************************************************************************************ */
/* ************************************************* DENM data ************************************************ */
/* ************************************************************************************************************ */

/**
 * A.102 DF_ActionID (A.77 + A.68)
 *
 * Identifier used to describe a protocol action taken by an ITS-S.
 * For example, it describes an action taken by an ITS-S to trigger a new DENM
 * as defined in ETSI EN 302 637-3 [i.0] after detecting an event.
 */
struct AdaActionId
{
    uint32_t origStationId;
    uint16_t sequenceNum;
};

enum class AdaInformationQuality : uint16_t
{
    UNAVAILABLE	= 0,
    LOWEST	    = 1,
    LOW         = 2,
    MEDIUM_LOW  = 3,
    MEDIUM      = 4,
    MEDIUM_HIGH = 5,
    HIGH        = 6,
    HIGHEST	    = 7
};

enum class AdaCauseCodeType : uint8_t
{
    RESERVED            = 0,
    TRAFFIC_CONDITION   = 1,
    ACCIDENT            = 2,
    ROADWORKS           = 3,
    HAZARD_LOC_SURFACE  = 9,
    PEDESTRIAN          = 12,
    WRONG_WAY_DRIVING   = 14,
    COLLISION_RISK      = 97,
    SIGNAL_VIOLATION    = 98,
    DANGEROUS_SITUATION = 99,

    ROAD_CLOSED         = 0xFF - 1, /* Тип для HMI Environment */
};

struct AdaCauseCode {
    AdaCauseCodeType causeCode;
    uint16_t         subCauseCode;
};

/**
 * A.99 DE_WrongWayDrivingSubCauseCode
 * Encoded value of the sub cause codes of the event type "wrongWayDriving"
 */
enum class WrongWayDrivingSubCauseCode : uint8_t
{
    UNAVAILABLE,
    WRONG_LANE,
    WRONG_DIRECTION
};

union AdaSubCauseCodeType
{
    WrongWayDrivingSubCauseCode wwd;
};

enum class AdaRelDistance : uint8_t
{
    LESS_THAN_50M,
    LESS_THAN_100M,
    LESS_THAN_200M,
    LESS_THAN_500M,
    LESS_THAN_1000M,
    LESS_THAN_5KM,
    LESS_THAN_10KM,
    OVER_10KM
};

enum class AdaRelTrafficDir : uint8_t
{
    ALL_TRAFFIC_DIRECTIONS = 0,
    UPSTREAM_TRAFFIC       = 1,
    DOWNSTREAM_TRAFFIC     = 2,
    OPPOSITE_TRAFFIC       = 3
};

/**
 * @brief A.94 DE_VehicleRole
 * 
 * Role played by a vehicle at a point in time.
 */
enum class AdaVehicleRole : uint8_t
{
    DEFAULT           = 0,  /**< Default vehicle role as indicated by the vehicle type */
    PUBLIC_TRANSPORT  = 1,  /**< Vehicle is used to operate public transport service */
    SPECIAL_TRANSPORT = 2,  /**< Vehicle is used for special transport purpose, e.g. oversized trucks */
    DANGER_GOODS      = 3,  /**< Vehicle is used for dangerous goods transportation */
    ROADWORKS         = 4,  /**< Vehicle is used to realize roadwork or road maintenance mission */
    RESCUE            = 5,  /**< Vehicle is used for rescue purpose in case of an accident, e.g. as a towing service */
    EMERGENCY         = 6,  /**< Vehicle is used for emergency mission, e.g. ambulance, fire brigade */
    SAFETY_CAR        = 7,  /**< Vehicle is used for public safety, e.g. patrol */
    AGRICULTURE       = 8,  /**< Vehicle is used for agriculture, e.g. farm tractor */
    COMMERCIAL        = 9,  /**< Vehicle is used for transportation of commercial goods */
    MILITARY          = 10, /**< Vehicle is used for military purpose */
    ROAD_OPERATOR     = 11, /**< Vehicle is used in road operator missions */
    TAXI              = 12  /**< Vehicle is used to provide an authorized taxi service */
};

enum class AdaStationType : uint8_t
{
    UNKNOWN,
    PEDESTRIAN,
    CYCLIST,
    MOPED,
    MOTOCYCLE,
    PASSENGER_CAR,
    BUS,
    LIGHT_TRUCK,
    HEAVY_TRUCK,
    TRAILER,
    SPEC_VEHICLE,
    TRAM,
    RSU = 15
};

struct AdaSpeed
{
    uint16_t speedValue;
    uint8_t  speedConf;
};

struct AdaHeading
{
    uint16_t headingValue;
    uint8_t  headingConf;
};

struct AdaPathPoint
{
    int32_t deltaLatitude;
    int32_t deltaLongitude;
    /* deltaElevation */
};

#define ADA_DENM_MAX_TRACE_POINTS_NUM   (12)

struct AdaTrace
{
    uint8_t      pointCount;
    AdaPathPoint point[ADA_DENM_MAX_TRACE_POINTS_NUM];
};

enum class AdaRoadSide : uint8_t
{
    LEFT,
    RIGHT,
    FORWARD,
    BACKWARD
};

enum class AdaRoadClass : uint8_t
{
    URBAN_NO_STRCT_SEPAR_TO_OPP_LANES,
    URBAN_WITH_STRCT_SEPAR_TO_OPP_LANES,
    NON_URBAN_NO_STRCT_SEPAR_TO_OPP_LANES,
    NON_URBAN_WITH_STRCT_SEPAR_TO_OPP_LANES
};

struct AdaRoadworks
{
    uint16_t closedLanes;
    uint16_t speedLimit;
};

struct AdaItsHeader
{
    uint8_t  version;
    uint32_t messageId;
    uint32_t stationId;
};

/* ************************************************************************************************************ */
/* ************************************************** GPS data ************************************************ */
/* ************************************************************************************************************ */
typedef enum
{
    ADA_MODE_NA,     /* N/A */
    ADA_MODE_NO_FIX, /* No fix */
    ADA_MODE_TIME_ONLY, /* System time syncronized with GPS time */
    ADA_MODE_2D,     /* A 2D (two dimensional) position fix */
    ADA_MODE_3D,     /* A 3D (three dimensional) position fix */
} AdaGpsMode;

typedef struct
{
    AdaGpsMode     mode;
    AdaPosition    position;            /* 1/10 microdegree */
    uint16_t       heading;             /* 1/10 degree */
    uint16_t       speed;               /* INTEGER(0..16383) cm/sec */
    time_t         time;
    uint16_t       semiMajorConfidence; /* oneCentimeter(1), outOfRange(4094), unavailable(4095) */
    uint16_t       semiMinorConfidence; /* oneCentimeter(1), outOfRange(4094), unavailable(4095) */
    uint16_t       semiMajorOrient; /*wgs84North(0), wgs84East(900), wgs84South(1800), wgs84West(2700), unavailable(3601) */
} AdaGpsData;


/* ************************************************************************************************************ */
/* ************************************************** CAM data ************************************************ */
/* ************************************************************************************************************ */

typedef enum {
    ADA_STATION_TYPE_UNKNOWN    = (0),
    ADA_STATION_TYPE_PEDESTRIAN = (1),
    ADA_STATION_TYPE_CYCLIST    = (2),
    ADA_STATION_TYPE_MOPED      = (3),
    ADA_STATION_TYPE_MOTOCYCLE  = (4),
    ADA_STATION_TYPE_PASSENGER_CAR = (5),
    ADA_STATION_TYPE_BUS        = (6),
    ADA_STATION_TYPE_LIGTH_TRUCK = (7),
    ADA_STATION_TYPE_HEAVY_TRUCK = (8),
    ADA_STATION_TYPE_TRAILER    = (9),
    ADA_STATION_TYPE_SPEC_VEHICLE = (10),
    ADA_STATION_TYPE_TRAM       = (11),
    ADA_STATION_TYPE_RSU        = (15),
    ADA_STATION_TYPE_LAST
}   AdaCamStationType;

typedef enum {
    ADA_VEHICLE_ROLE_DEFAULT        = (0),
    ADA_VEHICLE_ROLE_PUBLIC_TR      = (1),
    ADA_VEHICLE_ROLE_SPECIAL_TR     = (2),
    ADA_VEHICLE_ROLE_DANG_GOODS     = (3),
    ADA_VEHICLE_ROLE_ROADWORKS      = (4),
    ADA_VEHICLE_ROLE_RESQUE         = (5),
    ADA_VEHICLE_ROLE_EMERGENCY      = (6),
    ADA_VEHICLE_ROLE_SAFETY_CAR     = (7),
    ADA_VEHICLE_ROLE_AGRICULTURE    = (8),
    ADA_VEHICLE_ROLE_COMMERCIAL     = (9),
    ADA_VEHICLE_ROLE_MILITARY       = (10),
    ADA_VEHICLE_ROLE_ROAD_OPER      = (11),
    ADA_VEHICLE_ROLE_TAXI           = (12),
    ADA_VEHICLE_ROLE_LAST
}   AdaCamVehicleRole;

typedef struct Ada3dPosition
{
    int32_t lat;  /* INTEGER (-900000000..900000001 "1/10 micro degree")   */
    int32_t lon;  /* INTEGER (-1799999999..1800000001 "1/10 micro degree") */
    int32_t alt;  /* INTEGER (0..800001 "centimeter") */
}Ada3dPoint;


/*
 * In Vehicle Signal Status
 */
enum class AdaInVehTurnSignals : uint8_t
{
    OFF,
    LEFT,
    RIGHT,
    ALARM
};

struct AdaInVehSignals
{
    AdaInVehTurnSignals     turnLights;
    bool                    siren;
};


enum class AdaSegOrIntersect : uint8_t
{
    UNKNOWN = 0,
    SEGMENT,
    INTERSECTION,

    LAST
};

enum class AdaMapLaneDir : uint8_t
{
    NO_TRAVEL,
    INGRESS,
    EGRESS,
    BIDIRECTIONAL,
};

enum class AdaMapLaneType : uint8_t
{
    UNKNOWN,
    VEHICLE,
    CROSSWALK,
    BIKE_LANE,
    SIDEWALK,
    MEDIAN,
    STRIPING,
    TRACKED_VEHICLE,
    PARKING
};

enum AdaMapTurn
{
    ADA_MAP_TURN_FORWARD = 0,
    ADA_MAP_TURN_LEFT,
    ADA_MAP_TURN_RIGHT,

    ADA_MAP_TURN_LAST
};

enum class AdaMapIntersectStatus : uint8_t
{
    UNAVAILABLE,
    UNMAN_INTERSEC,       /* no information from SPAT */
    TRAFFIC_LIGHT_INTERSEC,
    RAILROAD_CROSS        /* look for  LaneAttributes-TrackedVehicle in MAP */
};

enum class AdaSpatPhaseState : uint8_t
{
    UNAVAIL = 0,
    DARK = 1,
    STOP_AND_REMAIN = 3,
    PERMISSIVE_MOVEMENT_ALLOWED = 5
};


enum class AdaMapManeuverRule : uint8_t    /* Look for AllowedManeuvers */
{
    UNAVAILABLE,
    NO_ENTRY,       /**< there is opportunity to go this way, but it strictly prohibited */
    STOP,           /**< after making a full stop, may proceed */
    YIELD,          /**< yield sign */
    CAUTION,        /**< proceed past stop line with caution */
    NO_STOPPING_ALLOWED,   /**< the vehicle should not stop at the stop line */
};

/**
 * @brief The structure stores topological location information
 * @note  All fields could be used only in case partType is determined (not UNKNOWN)
 */
struct AdaLanePosData
{
    AdaSegOrIntersect     partType;     /**< Intersection or Segment; UNKNOWN in case map part is not found */
    uint32_t              partId;       /**< Intersection or Segment unuque identeficator */
    AdaMapLaneDir         laneDir;      /**< Ingress or Egress */
    AdaMapLaneType        laneType;     /**< Lane could be for Vehicles, Pedestrians and others */
    uint8_t               laneNum;      /**< Lane index number in Intersection or Segment */
    uint8_t               nodeFromNum;  /**< Node index in lane */
    uint8_t               nodeToNum;    /**< Node index in lane */
    AdaPosition           stopLinePos;  /**< Stop line coordinates for this lane */
    AdaMapIntersectStatus status;       /**< For Intersection. Current status of intersection signal groupes */
    uint16_t              speedLimit;   /**< Maximun speed allowed in this area. km/h */
    AdaMapManeuverRule    maneuverRule; /**< For Intersection. Priority rule */
    uint8_t               maneuverApprchNum;    /**< For Intersection. The approach number to which current maneuver leads */
    uint8_t               signalGroup;  /**< For Intersection. Unique for this intersect ID signal groupe number */
    AdaSpatPhaseState     phaseState;   /**< For Intersection. Signal groupe state relevant for this station */
    uint64_t              phaseEndTime; /**< For Intersection. Signal groupe state end time. Milliseconds UTC */
    bool                  isWrongWay;   /**< Wrong way driving flag */
    uint8_t               laneNumFromRoadside;  /**< from 1 ...; (255) unavailable */
    uint8_t               leftLaneNum;      /**< 0..; (254) oncoming traffic or roadside; (255) unavailable */
    uint8_t               rightLaneNum;     /**< 0..; (254) oncoming traffic or roadside; (255) unavailable */
    uint8_t               numLanesSameDir;  /**< Number of lanes on this approach or segment directed to the same direction */
    uint8_t               apprchNum;        /**< For Intersection. Current approach number */
    uint8_t               oncomingLaneNum;  /**< Current oncoming lane number. (255) unavailable */
};

struct AdaAcceleration3D
{
    int16_t     lonAcc;     /* acceleration in 0.1 m/s^2 */
    uint8_t     lonConf;    /* in 0.1 m/s^2; (101) out of range; (102) unavailable */
    int16_t     latAcc;     /* acceleration in 0.1 m/s^2 */
    uint8_t     latConf;    /* in 0.1 m/s^2; (101) out of range; (102) unavailable */
    int16_t     vertAcc;    /* acceleration in 0.1 m/s^2 */
    uint8_t     vertConf;   /* in 0.1 m/s^2; (101) out of range; (102) unavailable */
};

enum AdaSpeedLimitType
{
    ADA_SPEED_LIM_VEHICLE_MAX_SPEED_DAY,
    ADA_SPEED_LIM_VEHICLE_MAX_SPEED_NIGHT,
    ADA_SPEED_LIM_TRUCK_MAX_SPEED_DAY,
    ADA_SPEED_LIM_TRUCK_MAX_SPEED_NIGHT,
    ADA_SPEED_LIM__SIZE
};

AdaMapTurn AdaSignalsToMapTurn(AdaInVehSignals signals);

#endif // ADA_COMMON_H
