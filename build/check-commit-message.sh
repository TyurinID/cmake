#!/usr/bin/env bash

#
#
#       Copyright 2018 Sreda Software Solutions. All rights reserved.
#
#       The copyright notice above does not evidence any actual or
#       intended publication of such source code. The code contains
#       Sreda Software Solutions Confidential Proprietary Information.
#

set -uo pipefail

SUBJ_PATTERN='MR ![0-9]+ \(#[0-9]+[#0-9 ,]*\) .*'
SUBJ=$(git log -n 1 --pretty=format:%s)

if echo "$SUBJ" | grep -E -q "$SUBJ_PATTERN";
then
    echo "Valid commit message"
else
    echo
    echo "ERROR: Invalid commit message: $SUBJ"
    echo ""
    echo '       Message should match pattern: MR !N_MERGE_REQUEST (#N_TASK_ID[, #N_TASK_ID]) MESSAGE'
    echo "             where N_MERGE_REQUEST -- merge request id"
    echo "                   N_TASK_ID       -- task id (may be more than one) "     
    echo "                   MESSAGE         -- short description of your changes "
    echo ------------------------------
    exit -1
fi
