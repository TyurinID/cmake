#!/usr/bin/env bash

#
#
#       Copyright 2017 Sreda Software Solutions. All rights reserved.
#
#       The copyright notice above does not evidence any actual or
#       intended publication of such source code. The code contains
#       Sreda Software Solutions Confidential Proprietary Information.
#
#

# Generate test code coverage report for Sreda Solutions Mesh
# Requires LCOV tool 

set -euo pipefail
cd out/

NAME=coverage_report
OUT_DIR=./artifacts/coverage

rm -r -- $OUT_DIR || true
mkdir -p $OUT_DIR
lcov --zerocounters --directory .
lcov --capture --initial --directory . --output-file $OUT_DIR/$NAME

make test || true

lcov --no-checksum --directory . --capture --output-file $OUT_DIR/$NAME.info
cd $OUT_DIR 
genhtml $NAME.info

