#!/usr/bin/env bash

#
#
#       Copyright 2017 Sreda Software Solutions. All rights reserved.
#
#       The copyright notice above does not evidence any actual or
#       intended publication of such source code. The code contains
#       Sreda Software Solutions Confidential Proprietary Information.
#

set -euo pipefail

CURRENT_DIR=`realpath .`

SRC_HEADERS_DIR=$CURRENT_DIR/doc/latex/headers
TMP_DIR=$CURRENT_DIR/doc/latex/tmp
OUT_DIR=$CURRENT_DIR/out/artifacts/latex/headers

rm -r -- $OUT_DIR 2> /dev/null || true
mkdir -p $OUT_DIR 2> /dev/null

rm -r -- $TMP_DIR 2> /dev/null || true
mkdir -p $TMP_DIR 2> /dev/null

pushd $TMP_DIR

for i in `find $SRC_HEADERS_DIR -name \*.tex`;
do
    pdflatex $i
    i=$(basename $i)
    a=${i%.tex}
    convert -density 600 $TMP_DIR/$a.pdf -quality 100 $TMP_DIR/$a.png
done;

for i in `find $TMP_DIR -name \*.png`;
do
    cp $i $OUT_DIR
done;

popd

rm -r -- $TMP_DIR 2> /dev/null || true
