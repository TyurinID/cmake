#
#
#       Copyright 2018 Sreda Software Solutions. All rights reserved.
#
#       The copyright notice above does not evidence any actual or
#       intended publication of such source code. The code contains
#       Sreda Software Solutions Confidential Proprietary Information.
#
#

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(TOOLCHAIN_PREFIX arm-linux-gnueabihf)

set(CMAKE_C_COMPILER ${TOOLCHAIN_PREFIX}-gcc)
set(CMAKE_ASM_COMPILER ${CMAKE_C_COMPILER})
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PREFIX}-g++)

set(CMAKE_FIND_ROOT_PATH /usr/${TOOLCHAIN_PREFIX})
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(CMAKE_C_COMPILE_OPTIONS_SYSROOT /usr/${TOOLCHAIN_PREFIX}/libhf)
set(CMAKE_CXX_COMPILE_OPTIONS_SYSROOT /usr/${TOOLCHAIN_PREFIX}/libhf)

set(THREADS_PTHREAD_ARG
     "0"
     CACHE STRING "Result from TRY_RUN" FORCE)
