# System configuration
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR craton)
set(CMAKE_CROSSCOMPILING 1)

# I could not find an easy way to pass ${UNEX_WEAK_LIB_PATH} into C/CXXFLAGS of "TryCompile"
# module. Without this var linker will not work. I tried the following:
# - If I keep this variable in toolchain file it always redefines PROJECT_SOURCE_DIR to
#   out/CMakeFiles/CMakeTmp which makes UNEX_WEAK_LIB_PATH invalid.
# - If I keep it in "initial cache file" -- "TryCompile" will not receive this variable at all.
#
# So I disabled compiler checks with the foollowing commands:
set(CMAKE_C_COMPILER_FORCED 1)
set(CMAKE_CXX_COMPILER_FORCED 1)

# SDK and Compliler path configuration
if (EXISTS "$ENV{UNEX_SDK_PATH}")
    set(SDK_ROOT "$ENV{UNEX_SDK_PATH}")
else()
    message(FATAL_ERROR "UNEX_SDK_PATH is not set")
endif(EXISTS "$ENV{UNEX_SDK_PATH}")

if (EXISTS "$ENV{UNEX_TOOLCHAIN_PATH}")
    set(TOOLCHAIN_ROOT "$ENV{UNEX_TOOLCHAIN_PATH}")
else()
    message(FATAL_ERROR "UNEX_TOOLCHAIN_PATH is not set")
endif(EXISTS "$ENV{UNEX_TOOLCHAIN_PATH}")

if (EXISTS "$ENV{UNEX_LIB_PATH}")
    set(LIB_ROOT "$ENV{UNEX_LIB_PATH}")
else()
    message(FATAL_ERROR "UNEX_LIB_PATH is not set")
endif(EXISTS "$ENV{UNEX_LIB_PATH}")

#Check for the usage of OBU location simulation
if (${USE_LOCATION_SIMULATION})
   	message(STATUS "OBU location simulation is in use")
	add_compile_options(
		-DUSE_LOCATION_SIMULATION
		)
endif(${USE_LOCATION_SIMULATION})

# Setting compiler
set(CMAKE_C_COMPILER "${TOOLCHAIN_ROOT}/bin/arm-none-eabi-gcc")
set(CMAKE_CXX_COMPILER "${TOOLCHAIN_ROOT}/bin/arm-none-eabi-g++")


set(CMAKE_FIND_ROOT_PATH "${TOOLCHAIN_ROOT}/bin/")

# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# Setting compiler flags
add_compile_options(
        -MD -MP
        -D__CRATON__=1
        -D__THREADX__=1
        -fshort-enums
        -fpack-struct=4
        -D__CRATON_ARM=1
        -mthumb
        -march=armv7-r
        -mfloat-abi=hard
        -mfpu=vfpv3-d16
        -D__CRATON_NO_ARC=1
        -I${SDK_ROOT}/include
        -I${SDK_ROOT}/release/arm/include
        -I${TOOLCHAIN_ROOT}/include
)

set(UNEX_WEAK_LIB_PATH "${PROJECT_SOURCE_DIR}/libs/unex-weak/weak.obj" CACHE STRING "" FORCE)

set(CMAKE_C_LINK_FLAGS
        "${UNEX_WEAK_LIB_PATH}"
        "${SDK_ROOT}/board/pcb201v1/arm/lib/ref-sys.o"
        "-mthumb -L${SDK_ROOT}/release/arm/lib -L${LIB_ROOT}/lib"
        "-march=armv7-r"
        "-lcraton"
        "-lm"
        "-mfloat-abi=hard -mfpu=vfpv3-d16"
        "-T craton-sc-arm.ld -nostartfiles"
        "-D__CRATON__=1 -D__THREADX__=1 -fshort-enums -fpack-struct=4 -D__CRATON_ARM=1 -D__CRATON_NO_ARC=1"
        )

string(REGEX REPLACE ";" " " CMAKE_C_LINK_FLAGS "${CMAKE_C_LINK_FLAGS}")
string(REGEX REPLACE ";" " " CMAKE_CXX_LINK_FLAGS "${CMAKE_C_LINK_FLAGS}")

function(create_img executable)
  add_custom_command(TARGET ${executable}
    POST_BUILD
    WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
    COMMAND ${TOOLCHAIN_ROOT}/bin/arm-none-eabi-size
        ${executable}
    COMMAND ${TOOLCHAIN_ROOT}/bin/arm-none-eabi-objcopy
        -O binary ${executable} ${executable}.bin
    COMMAND ${SDK_ROOT}/build/arm-bin-to-uimage
        ${executable}.bin ${executable}.img
    )
endfunction(create_img)
