#!/usr/bin/env bash

#
#
#       Copyright 2018 Sreda Software Solutions. All rights reserved.
#
#       The copyright notice above does not evidence any actual or
#       intended publication of such source code. The code contains
#       Sreda Software Solutions Confidential Proprietary Information.
#

set -uo pipefail

MASTER="origin/master"
NUM_COMMITS=$(git rev-list --count HEAD ^$MASTER)

if [ "$NUM_COMMITS" -eq "1" ];
then
    echo "OK"
elif [ "$NUM_COMMITS" -eq "0" ];
then
    echo "OK. On master branch"
else
    echo
    echo "ERROR: Invalid number of commits: $NUM_COMMITS"
    echo ""
    echo "   There should be only one commit to be merged into $MASTER"
    echo "   Please squash your commits into a single commit."
    echo ""
    echo " --------------------------------------------------------- "
    exit -1
fi
