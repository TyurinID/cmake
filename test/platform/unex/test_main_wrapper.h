#ifndef TEST_MAIN_WRAPPER_H
#define TEST_MAIN_WRAPPER_H

#ifdef __CRATON__
#ifdef __cplusplus
#define ADA_EXTERN_C extern "C"
extern "C"
{
#else
#define ADA_EXTERN_C
#endif

#include <tx_posix.h>
#include <craton/user.h>
#include <craton/reboot.h>
#include <utils/utils.h>

#ifdef __cplusplus
}
#endif


static const char *_testMainFileName;

/*static void *mainThread(void *pArg) */
static void mainThread(ULONG pArg)
{
    static const int TIMEOUT_BEFORE_REBOOT = 4;
    static const int NUM_REPEAT_START_STOP_MSG = 3;
    static const ULONG TIMEOUT_REPEAT_START_STOP_MSG_TICKS = 0.3 * TX_TICK_RATE;
    atlk_rc_t rc;
    int testFailed;

    (void) pArg;
    size_t fnameLen = strlen(_testMainFileName);
    size_t len = MIN(fnameLen, static_cast<size_t>(50));

    for (int i = 0; i < NUM_REPEAT_START_STOP_MSG; i++)
    {
        tx_thread_sleep(TIMEOUT_REPEAT_START_STOP_MSG_TICKS);
        LOG_NOTICE("=== STARTING TEST %s ===", &_testMainFileName[fnameLen - len]);
    }

    testFailed = main(0, NULL);

    if (!testFailed)
    {
        for (int i = 0; i < NUM_REPEAT_START_STOP_MSG; i++)
        {
            tx_thread_sleep(TIMEOUT_REPEAT_START_STOP_MSG_TICKS);
            LOG_NOTICE("=== TEST PASSED ===");
        }
    }

    for (int i = 0; i < TIMEOUT_BEFORE_REBOOT; i++)
    {
        UINT rcSleep;
        printf("Reboot in %d ...\n", TIMEOUT_BEFORE_REBOOT - i);
        rcSleep = tx_thread_sleep(1 * TX_TICK_RATE);
        assert(rcSleep == TX_SUCCESS);
    }

    rc = system_reboot(SYSTEM_REBOOT_SOC);
    assert(rc == ATLK_OK);
}


#ifndef ADA_TEST_MAIN_WRAPPER_DIRECT_CALL

static uint8_t _testMainStack[512 * 1024];
static TX_THREAD _testMainThread;

#define ADA_TEST_MAIN_WRAPPER_PRIORITY (40)

#define ADA_DECLARE_MAIN_TEST_WRAPPER() \
    ADA_EXTERN_C \
    void craton_user_init(void) \
    { \
        ULONG trv;                       \
        _testMainFileName = __FILE__;  \
        trv = tx_thread_create(&_testMainThread, (CHAR*) "AdaTestMain",  \
                               mainThread, 0,                   \
                               _testMainStack, sizeof(_testMainStack),        \
                               ADA_TEST_MAIN_WRAPPER_PRIORITY, ADA_TEST_MAIN_WRAPPER_PRIORITY, \
                               TX_NO_TIME_SLICE, TX_AUTO_START); \
        ADA_ASSERT_ALWAYS(trv == TX_SUCCESS); \
    }

#else

#define ADA_DECLARE_MAIN_TEST_WRAPPER() \
    ADA_EXTERN_C \
    void craton_user_init(void) \
    { \
        _testMainFileName = __FILE__;  \
        mainThread(0);  \
        return; \
    }

#endif

#else /* __CRATON__ */
#define ADA_DECLARE_MAIN_TEST_WRAPPER()
#endif /* __CRATON__ */

#endif /* TEST_MAIN_WRAPPER_H */
