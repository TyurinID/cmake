#!/usr/bin/env python3

#
#
#       Copyright 2018 Sreda Software Solutions. All rights reserved.
#
#       The copyright notice above does not evidence any actual or
#       intended publication of such source code. The code contains
#       Sreda Software Solutions Confidential Proprietary Information.
#
#


import logging
import os
import argparse
import shutil
import threading
import socketserver
import sys
import socket

ENV_UNEX_BOOT_IMAGE_FILE_PATH_NAME = 'UNEX_BOOT_IMAGE_FILE_PATH'
DEFAULT_UNEX_BOOT_IMAGE_FILE_PATH = '/srv/tftp/test.img'
HOST, PORT = "0.0.0.0", 514
TEST_TIMEOUT_INIT_SEC = 50
TEST_TIMEOUT_RUNNING_SEC = 15
TEST_MSG_PATTERN_START = '=== STARTING TEST'
TEST_MSG_PATTERN_END = '=== TEST PASSED ==='

TEST_INIT = 0
TEST_RUNNING = 1
TEST_FAILED = 2
TEST_FINISHED_OK = 3

FORMAT = '%(asctime)s %(levelname)-5s %(module)s:%(lineno)d: %(message)s'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)
LOG = logging.getLogger(__name__)

gCond = threading.Condition()
gState = TEST_INIT
gTag = ''

LOG.debug("Test runner for UNEX")

class SyslogUDPHandler(socketserver.BaseRequestHandler, threading.Thread):

    def handle(self):
        data = bytes.decode(self.request[0].strip())
        socket = self.request[1]
        msg = str(data)

        global gState
        global gTag
        global gCond

        if gState == TEST_INIT and TEST_MSG_PATTERN_START in msg:
            tag = '/' + gTag

            if tag in msg:
                LOG.debug("'%s' found in msg", tag)
                gState = TEST_RUNNING
            else:
                LOG.error("'%s' NOT found in msg", tag)
                gState = TEST_FAILED

            gCond.acquire()
            gCond.notify()
            gCond.release()

        if gState == TEST_RUNNING and TEST_MSG_PATTERN_END in msg:
            gState = TEST_FINISHED_OK
            gCond.acquire()
            gCond.notify()
            gCond.release()

        LOG.debug("%s: %s", self.client_address[0], msg)


    def run():
        try:
            #server = socketserver.UDPServer((HOST,PORT), SyslogUDPHandler)
            serve_forever(poll_interval=0.5)
        except (IOError, SystemExit):
            raise
        except KeyboardInterrupt:
            print ("Crtl+C Pressed. Shutting down.")

class ServerThread(threading.Thread):
    def run(self):
        self.server = None
        try:
            self.server = socketserver.UDPServer((HOST,PORT), SyslogUDPHandler)
            self.server.serve_forever(poll_interval=0.5)

        except (IOError, SystemExit) as e:
            global gState

            gCond.acquire()
            gState = TEST_FAILED
            LOG.error("ERROR: **%s**" % e)
            gCond.notify()
            gCond.release()

            LOG.info("HINT: please make sure that your run this script with `sudo`")

            return

        except KeyboardInterrupt:
            print ("Crtl+C Pressed. Shutting down.")

    def stop(self):
        if self.server:
            self.server.shutdown()



def prepare_test(src, dst):
    shutil.copyfile(src, dst)

def main_finish(ret, boot_img_path):
    LOG.info("Removing %s ..." % boot_img_path)
    os.remove(boot_img_path)
    sys.exit(ret)

def positive_int(value):
    ivalue = int(value)
    if ivalue <= 0:
         raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue

def main():
    parser = argparse.ArgumentParser(description='Runs test on UNEX and checks result')
    parser.add_argument('test_image', metavar="TEST_IMAGE", type=argparse.FileType('rb'),
            help="Path to test image file (*.img)")
    parser.add_argument('tag', metavar="TAG", type=str,
            help="ID string to look for in log messages to identify this test")
    parser.add_argument('-i', action='store_true',
            help="Run in interactive mode")
    parser.add_argument('--test-timeout-s', metavar='TIMEOUT_S', type=positive_int,
            default=TEST_TIMEOUT_RUNNING_SEC,
            help="Expected test run time, positive value in seconds. Default: %d" \
                % TEST_TIMEOUT_RUNNING_SEC)
    args = parser.parse_args()

    if not args.test_image.name.endswith(".img"):
        LOG.error("Image file should end with .img")
        sys.exit(-1)

    boot_img_path = DEFAULT_UNEX_BOOT_IMAGE_FILE_PATH

    if ENV_UNEX_BOOT_IMAGE_FILE_PATH_NAME in os.environ:
        boot_img_path = os.environ[ENV_UNEX_BOOT_IMAGE_FILE_PATH_NAME]
        LOG.info("Boot image file path changed to [%s]" % boot_img_path)
    else:
        LOG.info("Use default Boot image file path [%s]" % boot_img_path)

    global gTag
    gTag = args.tag
    test_run_timeout = args.test_timeout_s

    prepare_test(args.test_image.name, boot_img_path)

    server = ServerThread()
    server.start()
    LOG.info("Syslog server started")
    print("\n")

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(bytes(" ", "utf-8"), (HOST, PORT))

    try:
        if args.i:
            input(">> Please reboot your UNEX and  press Enter to continue...")
        else:
            LOG.info("*** Please reboot your UNEX NOW!")

        global gState
        global gCond


        while gState != TEST_FINISHED_OK and gState != TEST_FAILED:
            timeout = TEST_TIMEOUT_INIT_SEC

            if gState == TEST_RUNNING:
                timeout = test_run_timeout

            gCond.acquire()
            if gCond.wait(timeout) == False:
                LOG.error("Test timeout")
                gState = TEST_FAILED
                gCond.release()

    except KeyboardInterrupt:
        LOG.info("Ctrl+C was pressed")
        server.stop()
        main_finish(-1, boot_img_path)

    server.stop()

    if gState != TEST_FINISHED_OK:
        LOG.error("Test FAILED")
        main_finish(-1, boot_img_path)

    LOG.info("Test OK")
    main_finish(0, boot_img_path)


main()


