/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */



/*
 *******************************************************************************
 *
 *  @file   stat_queue_test.c
 *  @author Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *  @brief  Tests for static queue class.
 *
 *******************************************************************************
 */

#include <std/ada_stat_queue.h>
#include <utils/log/log_modules.h>
#define LOG_MODULE_ID       LOG_MOD_TEST
#define LOG_MODULE_LEVEL    LOG_DEF_LEVEL_TEST
#include <utils/log/log_api.h>

#include <std/ada_mutex.h>
#include <std/ada_sleep.h>
#include <std/ada_cond_var.h>

#define ELEM_SIZE           1024
#define QUEUE_SIZE_1        50
#define STAGE1_POP_NUM      30
#define QUEUE_TIMEOUT_US    100000

static pthread_attr_t attr_th;
static pthread_t thread;
static AdaMutex mutex;
static AdaCondVar condVar;

static bool stage1 = false;
static bool stage2 = false;
static bool stage3 = false;
static bool stage4 = false;

void testFuncParams(void);
void testPushPop(void);
void *pusherThread(void *attr);

static void setParam(bool &param, bool val);
static void waitParam(bool &param);

int main(int, const char *[])
{
    for (uint16_t i = 0; i < 20; i++)
    {
        testFuncParams();
        testPushPop();
        AdaSleep::uSleep(100);
    }

    return 0;
}


void testFuncParams(void)
{
    fprintf(stderr, "== testFuncParams \n");
    uint32_t elem[50] = {};
    AdaStatQueue<uint32_t> queue(3);
    ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.waitAndPush(elem[1], 50));
    ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.waitAndPush(elem[2], 50));
    uint32_t num = 0;
    ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.getElemNum(num));
    ADA_ASSERT_ALWAYS(num == 2);
    ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.waitAndPush(elem[3], 50));
    ADA_ASSERT_ALWAYS(ADA_ERR_TIMEOUT == queue.waitAndPush(elem[4], 50));
    ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.getElemNum(num));
    ADA_ASSERT_ALWAYS(num == 3);
    ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.pop());
    ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.getElemNum(num));
    ADA_ASSERT_ALWAYS(num == 2);
    ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.pop());
    ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.getElemNum(num));
    ADA_ASSERT_ALWAYS(num == 1);
    ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.pop());
}


typedef struct {
    uint8_t buff[ELEM_SIZE] = {};
} element;

void testPushPop(void)
{
    fprintf(stderr, "== testPushPop \n");
    AdaStatQueue<element> queue(QUEUE_SIZE_1);
    uint32_t num = 0;
    stage1 = false;
    stage2 = false;
    stage3 = false;
    stage4 = false;

    pthread_attr_init(&attr_th);
    pthread_create(&thread, &attr_th, pusherThread, &queue);
    /* stage 1 */
    AdaError err = queue.waitAndGetElemNum(num, 10);
    LOG_PERROR("waitAndGetElemNum()", err);
    ADA_ASSERT_ALWAYS(err == ADA_ERR_TIMEOUT);
    setParam(stage1, true);
    /* stage 2 */
    waitParam(stage2);
    ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.waitAndGetElemNum(num, QUEUE_TIMEOUT_US));
    ADA_ASSERT_ALWAYS(num == QUEUE_SIZE_1);
    setParam(stage4, true);
    element *pElem = NULL;
    element elm = {};

    for (uint8_t i = 0; i < num; i++)
    {
        ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.get(&pElem, i));
        LOG_INFO("pElem->buff[] = %d", pElem->buff[ELEM_SIZE - 1]);
        ADA_ASSERT_ALWAYS(pElem->buff[ELEM_SIZE - 1] == i);
    }

    /* stage 3 */
    LOG_INFO("=stage 3=");


    for (uint8_t i = 0; i < STAGE1_POP_NUM; i++)
    {
        ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.pop());
        LOG_INFO("pElem->buff[] = %d", pElem->buff[ELEM_SIZE - 1]);
    }

    waitParam(stage3);

    /* stage 4 */
    for (uint8_t i = STAGE1_POP_NUM; i < QUEUE_SIZE_1 + STAGE1_POP_NUM; i++)
    {
        ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.pop(&elm));
        LOG_INFO("elm.buff[] = %d", elm.buff[ELEM_SIZE - 1]);
        ADA_ASSERT_ALWAYS(elm.buff[ELEM_SIZE - 1] == i);
    }

    ADA_ASSERT_ALWAYS(ADA_SUCCESS == queue.getElemNum(num));
    ADA_ASSERT_ALWAYS(num == 0);
    pthread_cancel(thread);
    pthread_join(thread, NULL);
}


void *pusherThread(void *attr)
{
    AdaStatQueue <element> *pQueue = (AdaStatQueue <element>*)attr;
    element elm = {};
    LOG_INFO("=stage 1=");
    waitParam(stage1);

    uint8_t k = 0;

    for (k = 0; k < QUEUE_SIZE_1 + 1; k++)
    {
        elm.buff[ELEM_SIZE - 1] = k;
        AdaError ret = pQueue->waitAndPush(elm, 10000);
        LOG_INFO("elm->buff[] = %d", elm.buff[ELEM_SIZE - 1]);

        if (ret == ADA_ERR_TIMEOUT)
        {
            break;
        }

        ADA_ASSERT_ALWAYS(ret == ADA_SUCCESS);
    }

    ADA_ASSERT_ALWAYS(k == QUEUE_SIZE_1);

    LOG_INFO("=stage 2=");
    setParam(stage2, true);
    waitParam(stage4);

    /* stage 3 */
    for (uint8_t i = QUEUE_SIZE_1; i < QUEUE_SIZE_1 + STAGE1_POP_NUM; i++)
    {
        elm.buff[ELEM_SIZE - 1] = i;
        ADA_ASSERT_ALWAYS(ADA_SUCCESS == pQueue->waitAndPush(elm, 100000));
        LOG_INFO("elm->buff[] = %d", elm.buff[ELEM_SIZE - 1]);
    }

    LOG_INFO("=stage 4=");
    setParam(stage3, true);
    return NULL;
}


static void setParam(bool &param, bool val)
{
    condVar.lockMutex();
    param = val;
    condVar.signal();
    condVar.unlockMutex();
}

static void waitParam(bool &param)
{
    condVar.lockMutex();

    while (param == false)
    {
        ADA_CHECK_ERROR_ALWAYS(condVar.timedWait(1000000));
    }

    condVar.unlockMutex();
}


#include <platform/unex/test_main_wrapper.h>
ADA_DECLARE_MAIN_TEST_WRAPPER();
