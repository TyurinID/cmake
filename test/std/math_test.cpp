/*
 *
 *       Copyright 2019 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */



/**
 *******************************************************************************
 *
 *  @file   math_test.cpp
 *  @author Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *  @brief  Tests for ada_math.h
 *
 *******************************************************************************
 */

#include <std/ada_math.h>
#include <ada_common.h>

#include <utils/log/log_modules.h>
#define LOG_MODULE_ID       LOG_MOD_TEST
#define LOG_MODULE_LEVEL    LOG_DEF_LEVEL_TEST
#include <utils/log/log_api.h>

void testCalcLonLengthCm();
void testIsInCircle();
void testCoordFromPointAndHeading();
void testDistBetweenPoints();
void testHeadingFromPointToPoint();
void testGetIntersection();
void testHeadingToPointDeg();
void testPointToSegment();

int main(int, const char *[])
{
    testCalcLonLengthCm();
    testIsInCircle();
    testCoordFromPointAndHeading();
    testDistBetweenPoints();
    testHeadingFromPointToPoint();
    testGetIntersection();
    testHeadingToPointDeg();
    testPointToSegment();
    return 0;
}


void testCalcLonLengthCm()
{
    LOG_INFO("\n\n == testCalcLonLengthCm ==");
    double lengthOfLongitude = 0;
    AdaPosition pos = {0, 0};
    lengthOfLongitude = AdaMath::calcLonLengthCm(pos);
    LOG_INFO("calcLonLengthCm(lat = %" PRIi32 ") res %f cm", pos.lat, lengthOfLongitude);
    ADA_ASSERT_ALWAYS(ABS(lengthOfLongitude - 1.1131946) <= 0.01);
    pos.lat = 155555550;
    lengthOfLongitude = AdaMath::calcLonLengthCm(pos);
    LOG_INFO("calcLonLengthCm(lat = %" PRIi32 ") res %f cm", pos.lat, lengthOfLongitude);
    ADA_ASSERT_ALWAYS(ABS(lengthOfLongitude - 1.0726775) <= 0.01);
    pos.lat = 600000009;
    lengthOfLongitude = AdaMath::calcLonLengthCm(pos);
    LOG_INFO("calcLonLengthCm(lat = %" PRIi32 ") res %f cm", pos.lat, lengthOfLongitude);
    ADA_ASSERT_ALWAYS(ABS(lengthOfLongitude - 0.5579998) <= 0.01);
    pos.lat = 880000000;
    lengthOfLongitude = AdaMath::calcLonLengthCm(pos);
    LOG_INFO("calcLonLengthCm(lat = %" PRIi32 ") res %f cm", pos.lat, lengthOfLongitude);
    ADA_ASSERT_ALWAYS(ABS(lengthOfLongitude - 0.0419) <= 0.01);
    LOG_INFO(" == done ==");
}

void testIsInCircle()
{
    LOG_INFO("\n\n == testIsInCircle ==");
    AdaPosition intersectionCentre = {600012090, 302094760};
    uint32_t radiusCm = 5000;
    AdaPosition car1_in = {600012090, 302087200};
    AdaPosition car2_out = {600012090, 302019650};
    AdaPosition car3_in = {600015040, 302094760};
    AdaPosition car4_in = {600011250, 302101950};
    AdaPosition car5_in = {600009080, 302093310};
    AdaPosition car6_out = {600017970, 302098250};
    AdaPosition car7_out = {600003140, 302105810};
    ADA_ASSERT_ALWAYS(true == AdaMath::isInCircle(car1_in, intersectionCentre, radiusCm));
    ADA_ASSERT_ALWAYS(false == AdaMath::isInCircle(car2_out, intersectionCentre, radiusCm));
    ADA_ASSERT_ALWAYS(true == AdaMath::isInCircle(car3_in, intersectionCentre, radiusCm));
    ADA_ASSERT_ALWAYS(true == AdaMath::isInCircle(car4_in, intersectionCentre, radiusCm));
    ADA_ASSERT_ALWAYS(true == AdaMath::isInCircle(car5_in, intersectionCentre, radiusCm));
    ADA_ASSERT_ALWAYS(false == AdaMath::isInCircle(car6_out, intersectionCentre, radiusCm));
    ADA_ASSERT_ALWAYS(false == AdaMath::isInCircle(car7_out, intersectionCentre, radiusCm));
    radiusCm = 500000;
    AdaPosition car8_in = {599929540, 302708390};
    AdaPosition car9_out = {599895200, 303161260};
    ADA_ASSERT_ALWAYS(true == AdaMath::isInCircle(car8_in, intersectionCentre, radiusCm));
    ADA_ASSERT_ALWAYS(false == AdaMath::isInCircle(car9_out, intersectionCentre, radiusCm));
    LOG_INFO(" == done ==");
}

void testCoordFromPointAndHeading()
{
    LOG_INFO("\n == testCoordFromPointAndHeading == ");
    AdaPosition carTarget = {600012090, 302094760};
    uint16_t heading = 0;
    AdaPosition pointB = {};
    pointB = AdaMath::coordFromPointAndHeading(carTarget, heading, 5000);
    ADA_ASSERT_ALWAYS(pointB.lon / 100 == carTarget.lon / 100);
    ADA_ASSERT_ALWAYS(pointB.lat / 100 == 6000165);
    heading = 900;
    pointB = AdaMath::coordFromPointAndHeading(carTarget, heading, 5000);
    ADA_ASSERT_ALWAYS(pointB.lon / 100 == 3021037);
    ADA_ASSERT_ALWAYS(pointB.lat / 100 == carTarget.lat / 100);
    heading = 1800;
    pointB = AdaMath::coordFromPointAndHeading(carTarget, heading, 10000);
    ADA_ASSERT_ALWAYS(pointB.lon / 100 == carTarget.lon / 100);
    ADA_ASSERT_ALWAYS(pointB.lat / 100 == 6000030);
    heading = 2700;
    pointB = AdaMath::coordFromPointAndHeading(carTarget, heading, 65000);
#ifdef ADA_MATH_USE_LUT
    ADA_ASSERT_ALWAYS(pointB.lon / 100 == 3019781);
#else
    ADA_ASSERT_ALWAYS(pointB.lon / 100 == 3019781);
#endif
    ADA_ASSERT_ALWAYS(pointB.lat / 100 == carTarget.lat / 100);
    heading = 450;
    pointB = AdaMath::coordFromPointAndHeading(carTarget, heading, 700);
    ADA_ASSERT_ALWAYS(pointB.lon / 100 == 3020956);
    ADA_ASSERT_ALWAYS(pointB.lat / 100 == 6000125);
    carTarget = {-2990, -606504000};
    heading = 3300;
    pointB = AdaMath::coordFromPointAndHeading(carTarget, heading, 50000);
    ADA_ASSERT_ALWAYS(pointB.lon / 100 == -6065264);
#ifdef ADA_MATH_USE_LUT
    ADA_ASSERT_ALWAYS(pointB.lat / 100 == 359);
#else
    ADA_ASSERT_ALWAYS(pointB.lat / 100 == 360);
#endif
    LOG_INFO("%" PRIi32 " => %" PRIi32 " \n%" PRIi32 " => %" PRIi32 "\n", carTarget.lat, pointB.lat,
             carTarget.lon, pointB.lon);
    LOG_INFO(" == done ==");
}


void testDistBetweenPoints()
{
    LOG_INFO("\n\n == testDistBetweenPoints == ");
    AdaPosition pointA = {600015240, 302061060};
    AdaPosition pointB = {600011300, 302110090};
    ADA_ASSERT_ALWAYS(AdaMath::distBetweenPoints(pointA, pointB) / 100 == 276);
    LOG_INFO(" == done ==");
}

#define HEADING_TEST_CASES_NUM      (7)

void testHeadingFromPointToPoint()
{
    LOG_INFO("\n == testHeadingFromPointToPoint == ");

    struct testCase
    {
        AdaPosition pos;
        uint16_t    expHeadingLut;
        uint16_t    expHeadingNoLut;
    };

    AdaPosition initPos = {599340560, 303084200};

    testCase testCase[HEADING_TEST_CASES_NUM] =
    {
        {{599346240, 303083230}, 3572, 3552},       /* 0 degree */
        {{599343920, 303095300}, 587, 588},         /* 45 degree */
        {{599340780, 303102280}, 886, 886},         /* 90 degree */
        {{599332590, 303094120}, 1491, 1481},       /* 165 degree */
        {{599336840, 303103240}, 1115, 1114},       /* 115 degree */
        {{599334020, 303064460}, 2363, 2365},       /* 225 degree */
        {{599343630, 303061560}, 2852, 2852},       /* 300 degree */
    };


    for (uint32_t i = 0; i < HEADING_TEST_CASES_NUM; i++)
    {
        LOG_INFO("heading = %" PRIu16, AdaMath::headingFromPointToPoint(initPos, testCase[i].pos));
#ifdef ADA_MATH_USE_LUT
        ADA_ASSERT(testCase[i].expHeadingLut ==
                   AdaMath::headingFromPointToPoint(initPos, testCase[i].pos));
#else
        ADA_ASSERT(testCase[i].expHeadingNoLut ==
                   AdaMath::headingFromPointToPoint(initPos, testCase[i].pos));
#endif
    }

    LOG_INFO(" == done ==");
}

void testGetIntersection()
{
    LOG_INFO("\n\n == testGetIntersection == ");
    AdaPosition pointA = {513157230, 123814850};
    uint16_t headingA = 0;
    AdaPosition pointB = {513166340, 123797090};
    uint16_t headingB = 900;
    AdaPosition intersect = {};
    ADA_ASSERT_ALWAYS(true == AdaMath::getIntersection(pointA, headingA, pointB, headingB,
                                                       intersect));
    ADA_ASSERT_ALWAYS(intersect.lon / 100 == 1238148);
    ADA_ASSERT_ALWAYS(intersect.lat / 100 == 5131663);
    ADA_ASSERT_ALWAYS(true == AdaMath::getIntersection(pointA, 3600, pointB, 150, intersect));
    ADA_ASSERT_ALWAYS(true == AdaMath::getIntersection(pointA, 3430, pointB, 10, intersect));
    ADA_ASSERT_ALWAYS(true == AdaMath::getIntersection(pointA, 500, pointB, 650, intersect));
    ADA_ASSERT_ALWAYS(true == AdaMath::getIntersection(pointA, 500, pointB, 505, intersect));
    LOG_INFO("Intersect %" PRIi32 " %" PRIi32, intersect.lat, intersect.lon);
    ADA_ASSERT_ALWAYS(false == AdaMath::getIntersection(pointA, 500, pointB, 500, intersect));
    LOG_INFO(" == done ==");
}

#define HEAD_TO_POINT_NUM_CASES     ()

void testHeadingToPointDeg()
{
    LOG_INFO("\n\n == testHeadingToPointDeg == ");
    AdaPosition posFrom = {599340560, 303084200};
    uint16_t headingT = 0;

    AdaPosition pointTP[7] =
    {
        {599353260, 303084400},     /*[0] North */
        {599358000, 303133760},     /*[1] 45 degree */
        {599341900, 303136440},     /*[2] 90 degree */
        {599326220, 303083520},     /*[3] 180 degree */
        {599337950, 303078210},     /*[4] 240 degree */
        {599343050, 303078970},     /* 320 degree */
        {599398970, 303240480},     /* 45 degree */
    };

    headingT = 0;
    ADA_ASSERT_ALWAYS(true == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[0], 5));
    headingT = 100;
    ADA_ASSERT_ALWAYS(false == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[0], 50));
    headingT = 650;
    ADA_ASSERT_ALWAYS(true == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[0], 660));
    headingT = 3010;
    ADA_ASSERT_ALWAYS(true == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[0], 660));
#ifdef ADA_MATH_USE_LUT
    headingT = 2930;
#else
    headingT = 2940;
#endif
    ADA_ASSERT_ALWAYS(false == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[0], 660));
    headingT = 1790;
    ADA_ASSERT_ALWAYS(false == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[0], 660));
    headingT = 1810;
    ADA_ASSERT_ALWAYS(false == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[0], 660));
    headingT = 540;
    ADA_ASSERT_ALWAYS(true == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[1], 50));
    headingT = 900;
    ADA_ASSERT_ALWAYS(true == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[2], 50));
    headingT = 1800;
    ADA_ASSERT_ALWAYS(true == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[3], 50));
    headingT = 300;
    ADA_ASSERT_ALWAYS(true == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[3], 1600));
    headingT = 2700;
    ADA_ASSERT_ALWAYS(true == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[4], 411));
    headingT = 2700;
    ADA_ASSERT_ALWAYS(false == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[4], 410));
    headingT = 3100;
    ADA_ASSERT_ALWAYS(true == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[5], 50));
    headingT = 3100;
    ADA_ASSERT_ALWAYS(false == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[5], 5));
    headingT = 550;
    ADA_ASSERT_ALWAYS(true == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[6], 50));
    headingT = 532;
#ifdef ADA_MATH_USE_LUT
    ADA_ASSERT_ALWAYS(true == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[6], 10));
#else
    ADA_ASSERT_ALWAYS(true == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[6], 1));
#endif
    headingT = 532;
    ADA_ASSERT_ALWAYS(false == AdaMath::isHeadToPointDeg(posFrom, headingT, pointTP[6], 1));
    headingT = 900;
    ADA_ASSERT_ALWAYS(true == AdaMath::isHeadToPointDeg(posFrom, headingT, posFrom, 1));
    LOG_INFO(" == done ==");
}

void testPointToSegment()
{
    LOG_INFO("\n\n == testPointToSegment == ");
    AdaPosition A = {551789340, 613526740};
    AdaPosition B = {551796280, 613526450};
    AdaPosition C = {551794970, 613526800};
    int32_t dist = static_cast<int32_t>(AdaMath::distPointToSegment(C, A, B));
    LOG_INFO("distance to segment = %" PRIu32 " cm", dist);
    ADA_ASSERT_ALWAYS(ABS(dist - 188) <= 5);

    C = {551794530, 613525720};
    dist = static_cast<int32_t>(AdaMath::distPointToSegment(C, A, B));
    LOG_INFO("distance to segment = %" PRIu32 " cm", dist);
    ADA_ASSERT_ALWAYS(ABS(dist - 511) <= 5);

    C = {551787150, 613530360};
    dist = static_cast<int32_t>(AdaMath::distPointToSegment(C, A, B));
    LOG_INFO("distance to segment = %" PRIu32 " cm", dist);
    ADA_ASSERT_ALWAYS(ABS(dist - 3351) <= 5);

    C = {551788820, 613526720};
    dist = static_cast<int32_t>(AdaMath::distPointToSegment(C, A, B));
    LOG_INFO("distance to segment = %" PRIu32 " cm", dist);
    ADA_ASSERT_ALWAYS(ABS(dist - 577) <= 5);

    C = {551806660, 613525000};
    dist = static_cast<int32_t>(AdaMath::distPointToSegment(C, A, B));
    LOG_INFO("distance to segment = %" PRIu32 " cm", dist);
    ADA_ASSERT_ALWAYS(ABS(dist - 11558) <= 5);

    C = {551793020, 613602300};
    dist = static_cast<int32_t>(AdaMath::distPointToSegment(C, A, B));
    LOG_INFO("distance to segment = %" PRIu32 " cm", dist);
    ADA_ASSERT_ALWAYS(ABS(dist - 48233) <= 5);

    LOG_INFO(" == done ==");
}


#include <platform/unex/test_main_wrapper.h>
ADA_DECLARE_MAIN_TEST_WRAPPER();
