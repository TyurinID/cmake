/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 *******************************************************************************
 *
 *  @file   ada_task_test.cpp
 *  @author Dmitry Larchenko <dmitry.larchenko@sredasolutions.com>
 *  @brief  Tests for AdaTask class.
 *
 *******************************************************************************
 */

#include <std/ada_time.h>
#include <cstdio>
#include <unistd.h>
#include <inttypes.h>
#include <std/ada_mutex.h>
#include <std/ada_cond_var.h>

#include <std/ada_task.h>
#include <std/ada_sleep.h>
#include <utils/log/log_modules.h>
#define LOG_MODULE_ID       LOG_MOD_TEST
#define LOG_MODULE_LEVEL    LOG_DEF_LEVEL_TEST
#include <utils/log/log_api.h>

#define TEST_RUN_TIME_S  (3)
#define TEST_NUM_THREADS (20)
#define TEST_EXPECTED_CYCLE_COUNTER TEST_RUN_TIME_S
#define TEST_NUM_FAST_CYCLES (10)

class sharedCnt
{
public:
    sharedCnt() : _cnt(0)
    {
    }

    int operator++(int)
    {
        _mut.lock();
        uint32_t ret = _cnt++;
        _mut.unlock();
        return int(ret);
    }
    uint32_t get()
    {
        _mut.lock();
        uint32_t ret = _cnt;
        _mut.unlock();
        return ret;
    }

private:
    uint32_t _cnt;
    AdaMutex _mut;
};

static AdaMutex sharedMutex;
static sharedCnt shCnt;
static AdaCondVar condVar;
static bool canContinue = false;


/* Test threads context switching */
static void testThreadSwitching();

class TestTask : public AdaTask
{
public:
    int          _n;
    volatile int _testCycleCounter;

    TestTask(int n) : AdaTask(TestTask::testTaskMain, "TestTask")
    {
        _n = n;
        _testCycleCounter = 0;
    }

    void stop()
    {
        LOG_INFO("Stop requested for [%d]", _n);
    }

    static void *testTaskMain(void *pArg)
    {
        AdaTask *pTask = (AdaTask*) pArg;
        TestTask *pTestTask = dynamic_cast<TestTask*>(pTask);

        for (int i = 0; i < TEST_RUN_TIME_S; i++)
        {
            LOG_INFO("Task [%d] running", pTestTask->_n);
            AdaSleep::sSleep(1);

            pTestTask->_testCycleCounter++;
        }

        return NULL;
    }
};

class FastExitTask : public AdaTask
{
public:
    FastExitTask() : AdaTask(FastExitTask::run, "FastExitTask")
    {
        LOG_DEBUG("init");
    }

    static void *run(void*)
    {
        LOG_DEBUG("Run");
        return NULL;
    }

    void stop()
    {

    }
};

/* Test thread which exits immediately. It used to crash */
static void testFastThread()
{
    FastExitTask task;

    for (int i = 0; i < TEST_NUM_FAST_CYCLES; i++)
    {
        task.start();
        task.join();
    }
}

/* Test multiple simultanious threads */
static void testMultipleThreads()
{
    TestTask *pTasks[TEST_NUM_THREADS];

    for (int i = 0; i < TEST_NUM_THREADS; i++)
    {
        pTasks[i] = new TestTask(i);
        ADA_ASSERT_ALWAYS(pTasks[i] != nullptr);

        ADA_CHECK_ERROR_ALWAYS(pTasks[i]->start());
    }

    /* Wait less than thread run time to test that join() works correctly */
    AdaSleep::sSleep(MAX(0, TEST_RUN_TIME_S - 1));

    for (int i = 0; i < TEST_NUM_THREADS; i++)
    {
        pTasks[i]->stop();
        pTasks[i]->join();

        ADA_ASSERT_MSG_ALWAYS(
            pTasks[i]->isRunning() == false, "Expecting stopped thread after join()");
        ADA_ASSERT_ALWAYS(pTasks[i]->_testCycleCounter == TEST_EXPECTED_CYCLE_COUNTER);
        delete pTasks[i];
    }
}

/*
 * Time changing after 1ms test.
 */
int main(int, const char *[])
{
    testMultipleThreads();
    testFastThread();
    AdaSleep::sSleep(2);
    testThreadSwitching();
    return 0;
}

static void testThreadSwitching()
{
    fprintf(stderr, "\n\n== testThreadSwitching ==\n");

    class TestThreadLow : public AdaTask
    {
public:
        TestThreadLow() :
            AdaTask(TestThreadLow::run, "taskLow", ADA_TASK_PRIORITY_LOWEST)
        {
        }

        void stop()
        {
            fprintf(stderr, "Stop requested for TestThreadLow" "\n");
        }

        static void *run(void*)
        {
            condVar.lockMutex();
            fprintf(stderr, "TestThread << low >> timedWait(), sharedCnt = %" PRIu32 "\n",
                    shCnt.get());

            while (!canContinue)
            {
                condVar.timedWait(1000);
            }

            condVar.signal();
            condVar.unlockMutex();
            fprintf(stderr, "TestThread << low >> START, sharedCnt = %" PRIu32 "\n", shCnt.get());

            while (shCnt++ < 1000 * 1000)
            {
                if (shCnt.get() == 500 * 1000)
                {
                    fprintf(stderr, "TestThread << low >> uSleep(10), sharedCnt = %" PRIu32 "\n",
                            shCnt.get());
                    AdaSleep::uSleep(10);
                }
            }

            fprintf(stderr, "TestThread << low >> END, sharedCnt = %" PRIu32 "\n", shCnt.get());
            return nullptr;
        }
    };

    class TestThreadHigh : public AdaTask
    {
public:
        TestThreadHigh() :
            AdaTask(TestThreadHigh::run, "taskHigh", ADA_TASK_PRIORITY_LOW)
        {
        }

        void stop()
        {
            fprintf(stderr, "Stop requested for TestThreadHigh" "\n");
        }

        static void *run(void*)
        {
            condVar.lockMutex();
            fprintf(stderr, "TestThread << HIGH >> timedWait(), sharedCnt = %" PRIu32 "\n",
                    shCnt.get());

            while (!canContinue)
            {
                condVar.timedWait(1000);
            }

            condVar.signal();
            condVar.unlockMutex();
            fprintf(stderr, "TestThread << HIGH >> START, sharedCnt = %" PRIu32 "\n", shCnt.get());
#ifdef __CRATON__
            AdaSleep::mSleep(500);
#endif
            fprintf(stderr, "TestThread << HIGH >> for(), sharedCnt = %" PRIu32 "\n", shCnt.get());
            ADA_ASSERT_ALWAYS(shCnt.get() < 500 * 1000);

            for (uint16_t i = 0; i < 50; i++)
            {
                fprintf(stderr, "TestThread << HIGH >> uSleep(), sharedCnt = %" PRIu32 "\n",
                        shCnt.get());
                AdaSleep::uSleep(10);
            }

            fprintf(stderr, "TestThread << HIGH >> END, sharedCnt = %" PRIu32 "\n", shCnt.get());
            return nullptr;
        }
    };

    TestThreadHigh thHigh;
    TestThreadLow thLow;
    fprintf(stderr, "TestThread << main >> low.start() \n");
    thLow.start();
    fprintf(stderr, "TestThread << main >> high.start() \n");
    thHigh.start();
    condVar.lockMutex();
    fprintf(stderr, "TestThread << main >> canContinue, sharedCnt = %" PRIu32 "\n", shCnt.get());
    canContinue = true;
    condVar.broadcast();
    condVar.unlockMutex();
    thLow.join();
    thHigh.join();
    LOG_INFO("== test is passed ==\n\n");
}

#include <platform/unex/test_main_wrapper.h>
ADA_DECLARE_MAIN_TEST_WRAPPER();
