/*
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */



/**
 *******************************************************************************
 *
 *  @file   dynamic_queue_test.c
 *  @author Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *  @brief  Tests for dynamic queue class.
 *
 *******************************************************************************
 */

#include <std/ada_dynamic_queue.h>
#include <std/ada_sleep.h>
#include <cassert>
#include <cstdio>
#include <unistd.h>
#include <cstdint>
#include <cstdlib>
#ifdef __CRATON__
extern "C" {
#include <tx_posix.h>
}
#else
#include <pthread.h>
#include <semaphore.h>
#endif

#include <utils/log/log_modules.h>
#define LOG_MODULE_ID LOG_MOD_TEST
#define LOG_MODULE_LEVEL LOG_DEF_LEVEL_TEST
#include <utils/log/log_api.h>

#define TEST_PUSH_POP_NUM_THREADS 2
#define TEST_BUF_SIZE 10000
#define TEST_NUM_CHANGED_ELEMENTS 1000
#define TEST_FUNC_PARAM_BUF_SIZE 16384
#define TEST_FUNC_PARAM_QUEUE_SIZE 65535
#define TEST_FUNC_PARAM_QUEUE_SIZE_SMALL 64

static pthread_t _thread1;
static pthread_t _thread2;
static sem_t _sem;

static void testFuncParams(void);
static void testPushPop(void);


static void testFuncParams(void)
{
    LOG_DEBUG("Test ...");
    {
        AdaDynQueue queue(TEST_FUNC_PARAM_QUEUE_SIZE);

        assert(500 == queue.getElementsMaxNum());
    }

    LOG_DEBUG("Test ...");
    {
        AdaDynQueue queue(0);

        assert(1 == queue.getElementsMaxNum());
    }

    LOG_DEBUG("Test ...");
    {
        AdaDynQueue queue;

        queue.setElementsMaxNum(TEST_FUNC_PARAM_QUEUE_SIZE);
        assert(500 == queue.getElementsMaxNum());
        queue.setElementsMaxNum(0);
        assert(1 == queue.getElementsMaxNum());
    }

    LOG_DEBUG("Test ...");
    {
        AdaDynQueue queue(TEST_FUNC_PARAM_QUEUE_SIZE_SMALL);
        uint8_t *pBuf = (uint8_t*) calloc(TEST_FUNC_PARAM_BUF_SIZE, sizeof(uint8_t));
        AdaError ret = ADA_SUCCESS;

        assert(pBuf != NULL);
        ret = queue.waitAndPush(10000, pBuf, 1);
        assert(ADA_SUCCESS == ret);
        ret = queue.waitAndPush(2000000, pBuf, 1);
        assert(ADA_ERR_INVALID_PARAM == ret);
        ret = queue.waitAndPush(10000, NULL, 1);
        assert(ADA_ERR_INVALID_PARAM == ret);

        free(pBuf);
    }
    LOG_DEBUG("Test ...");
    {
        AdaDynQueue queue(TEST_FUNC_PARAM_QUEUE_SIZE_SMALL);
        uint8_t *pBuf = (uint8_t*) calloc(TEST_FUNC_PARAM_BUF_SIZE, sizeof(uint8_t));
        int32_t ret2 = 0;

        assert(pBuf != NULL);
        assert(ADA_SUCCESS == queue.waitAndPush(TEST_FUNC_PARAM_BUF_SIZE, pBuf, 1));
        ret2 = queue.waitAndGetDataSize();
        assert(TEST_FUNC_PARAM_BUF_SIZE == ret2);
        uint32_t bytesPoped = TEST_FUNC_PARAM_BUF_SIZE;
        assert(ADA_ERR_INVALID_PARAM == queue.waitAndPop(NULL, &bytesPoped, 1));
        assert(ADA_ERR_INVALID_PARAM == queue.waitAndPop(NULL, 0, 1));
        bytesPoped = TEST_FUNC_PARAM_BUF_SIZE - 1;
        assert(ADA_ERR_INVALID_LENGTH == queue.waitAndPop(pBuf, &bytesPoped, 1));
        bytesPoped = TEST_FUNC_PARAM_BUF_SIZE;
        assert(ADA_SUCCESS == queue.waitAndPop(pBuf, &bytesPoped, 1));
        assert(TEST_FUNC_PARAM_BUF_SIZE == bytesPoped);

        free(pBuf);
    }

    LOG_DEBUG("Passed.");
}

static void *threadFunc1(void*arg)
{
    AdaDynQueue *queue = (AdaDynQueue*)arg;

    uint16_t counter = 0;
    uint16_t perioder = 0;
    const uint32_t bufSize = TEST_BUF_SIZE;
    const uint32_t bufNum = bufSize / sizeof(uint16_t);
    uint16_t *pBuf16 =  (uint16_t*) calloc(bufNum, sizeof(uint16_t));

    LOG_DEBUG("Thread #1 starts");

    for (int j = 0; j < TEST_NUM_CHANGED_ELEMENTS; j++)
    {
        AdaError ret;

        for (size_t i = 0; i < bufNum; i++)
        {
            pBuf16[i] = (uint16_t)(counter + perioder * 1000);
        }

        counter++;
        perioder++;
        perioder = (perioder % 2);
        ret = queue->waitAndPush(bufSize, (uint8_t*) pBuf16);

        assert(ret == ADA_SUCCESS);
    }

    free(pBuf16);
    sem_post(&_sem);
    LOG_DEBUG("Thread #1 ends");

    return NULL;
}

static void *threadFunc2(void*arg)
{
    AdaDynQueue *queue = (AdaDynQueue*)arg;

    uint16_t counter = 0;
    uint16_t perioder = 0;
    int32_t bufSize = 0;
    const uint32_t bufNum = TEST_BUF_SIZE / sizeof(uint16_t);
    uint16_t *pBuf16 =  (uint16_t*) calloc(bufNum, sizeof(uint16_t));

    LOG_DEBUG("Thread #2 starts");

    for (int j = 0; j < TEST_NUM_CHANGED_ELEMENTS; j++)
    {
        bufSize = queue->waitAndGetDataSize(1000);
        assert(bufSize > 0);
        assert(ADA_SUCCESS == queue->waitAndPop((uint8_t*) pBuf16, (uint32_t *)&bufSize, 1));

        for (size_t i = 0; i < bufNum; i++)
        {
            assert(pBuf16[i] == (counter + perioder * 1000));
        }

        counter++;
        perioder++;
        perioder = (perioder % 2);
    }

    free(pBuf16);
    sem_post(&_sem);
    LOG_DEBUG("Thread #2 ends");

    return NULL;
}

static void testPushPop(void)
{
    AdaDynQueue myQueue(64);

    pthread_attr_t attr1;
    pthread_attr_t attr2;

    sem_init(&_sem, 0, TEST_PUSH_POP_NUM_THREADS);
    sem_wait(&_sem);
    sem_wait(&_sem);

    LOG_DEBUG("Creating threads");
    pthread_attr_init(&attr1);
    pthread_attr_init(&attr2);
    pthread_create(&_thread1, &attr1, threadFunc1, &myQueue);
    pthread_create(&_thread2, &attr2, threadFunc2, &myQueue);

    LOG_DEBUG("Joining threads");
    pthread_join(_thread1, NULL);
    pthread_join(_thread2, NULL);

    LOG_DEBUG("Waiting for semaphore in case if join() has failed (as it happens on UNEX)");
    sem_wait(&_sem);
    sem_wait(&_sem);
    sem_destroy(&_sem);
    LOG_DEBUG("All threads successfully finished");
}

int main(int, const char *[])
{
    testFuncParams();
    testPushPop();
    return 0;
}

#include <platform/unex/test_main_wrapper.h>
ADA_DECLARE_MAIN_TEST_WRAPPER();
