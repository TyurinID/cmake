/**
 *
 *       Copyright 2018 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/**
 *******************************************************************************
 *
 *  @file test/std/ADA_fs_test.cpp
 *  @author Pavel Matyunin <pavel.matyunin@sredasolutions.com>
 *
 *  Tests for FileIO operations
 *
 *******************************************************************************
 */

#include <stdio.h>

#include <time.h>
#include <errno.h>
#include <dirent.h>
#include <assert.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#ifdef __CRATON__
#include <tx_api.h>
#include <tx_posix.h>
#include <craton/user.h>
#else                       /* __CRATON__ */
#include <pthread.h>
#ifndef __CYGWIN__
#include <sys/syscall.h>
#endif                      /* __CYGWIN__ */
#endif                      /* __CRATON__ */

#include <std/ada_fs.h>
#include <utils/ada_error.h>

#include <std/ada_cond_var.h>
#include <utils/log/log_modules.h>
#define LOG_MODULE_ID       LOG_MOD_TEST
#define LOG_MODULE_LEVEL    LOG_DEF_LEVEL_TEST
#include <utils/log/log_api.h>

#define ADA_FS_TEST_DIR                                 "test_dir"

#define ADA_FS_TEST_FILE_NAME                           "testing_file"
#define ADA_FS_TEST_FILE_PATH                           ADA_FS_TEST_DIR "/" ADA_FS_TEST_FILE_NAME
#define ADA_FS_TEST_REMOVE_FILES                        1

#define ADA_FS_TEST_TABLE1_SIZE                         10
#define ADA_FS_TEST_TABLE2_SIZE                         15

#define ADA_FS_TEST_FILE_NAME_OPENCLOSE                 ADA_FS_TEST_FILE_NAME "_openclose"
#define ADA_FS_TEST_FILE_DIR                            "file_dir"

#define ADA_FS_TEST_FILE_NAME_READ                      ADA_FS_TEST_FILE_NAME "_read"
#define ADA_FS_TEST_READ_OFFSET                         4
#define ADA_FS_TEST_READ_MAXIMUM_SIZE                   2
#define ADA_FS_TEST_READ_OVER_SIZE                      (ADA_FS_TEST_TABLE1_SIZE + 5)

#define ADA_FS_TEST_FILE_NAME_WRITE                     ADA_FS_TEST_FILE_NAME "_write"

#define ADA_FS_TEST_FILE_NAME_WRITEFRAGMENTED           ADA_FS_TEST_FILE_NAME "_frag"
#define ADA_FS_TEST_WRITEFRAGMENTED_LIMIT               2 * ADA_FS_MAX_FILESIZE_MB + 1

#define ADA_FS_TEST_FILE_NAME_WRONGOPS                  ADA_FS_TEST_FILE_NAME "_wrong"
#define ADA_FS_TEST_WRONGOPS_READ_OVER_OFFSET           (ADA_FS_TEST_TABLE1_SIZE + 5)

#define ADA_FS_TEST_FILE_NAME_MULTIOPS                  ADA_FS_TEST_FILE_NAME "_multiops"

#define ADA_FS_TEST_FILE_NAME_MULTITHREAD               ADA_FS_TEST_FILE_NAME "_multithreaded"
#define ADA_FS_TEST_THREAD_COUNT                        10

/* Anyhow we need to synchronize our threads */
pthread_mutex_t runtimeMutex;
uint16_t threadBarrier = ADA_FS_TEST_THREAD_COUNT;

/* For thread IDs */
uint16_t threadId = 0;

/*
 * Just to clean a mess
 */
void test_cleanup()
{
    char *path;
    DIR *pDir = NULL;
    struct dirent *pDirFileName = NULL;
    uint32_t fileNameSize;
    uint32_t definedStrSize = strlen(ADA_FS_TEST_DIR) + 1;

    pDir = opendir(ADA_FS_TEST_DIR);

    if (!pDir)
    {
        return;
    }

    unlink( ADA_FS_TEST_DIR "/" ADA_FS_TEST_FILE_DIR "/" ADA_FS_TEST_FILE_DIR "/"
            ADA_FS_TEST_FILE_NAME_OPENCLOSE);
    rmdir(ADA_FS_TEST_DIR "/" ADA_FS_TEST_FILE_DIR "/" ADA_FS_TEST_FILE_DIR);
    rmdir(ADA_FS_TEST_DIR "/" ADA_FS_TEST_FILE_DIR);

    while ((pDirFileName = readdir(pDir)))
    {
        if ((strcmp(pDirFileName->d_name, ".") != 0) && (strcmp(pDirFileName->d_name, "..") != 0))
        {
            fileNameSize = (uint32_t)strlen(pDirFileName->d_name);
            path = (char*)calloc(definedStrSize + fileNameSize + 1, sizeof(char));
            ADA_ASSERT_ALWAYS(path != NULL);

            memcpy(&path[0], ADA_FS_TEST_DIR "/", definedStrSize * sizeof(char));
            memcpy(&path[definedStrSize], pDirFileName->d_name, fileNameSize * sizeof(char));
            unlink(path);

            free(path);
        }
    }

    closedir(pDir);

    rmdir(ADA_FS_TEST_DIR);
}

/*
 * Just to start tests
 */
void test_init()
{
    test_cleanup();
}

/*
 * Based on rand() data generator
 */
uint8_t *generateTable(uint32_t size)
{
    uint32_t i;
    uint8_t *obj = (uint8_t*)calloc(size, sizeof(uint8_t));

    if (!obj)
    {
        return NULL;
    }

    srand((uint32_t)time(NULL));

    for (i = 0; i < size; i++)
    {
        obj[i] = (uint8_t)(32 + rand() % 95);
    }

    return obj;
}

/*
 * A range of open&close tests
 * 1 - open a file in write-only mode, output is not fragmented.
 * 2 - open a file in write-only mode, output is fragmented.
 * 3 - open a file (is A DIRECTORY) in write-only mode, output is not fragmented.
 * 4 - open a file with a specified path in write-only mode, output is not fragmented.
 * 5 - open a file in write-only mode, output is not fragmented, file exists (should not create third file).
 * Test passed if there are no errors.
 */
static void test_openclose()
{
    AdaFs obj;
    int status;

    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_OPENCLOSE, ADA_FS_MODE_WRITE,
                               0) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);

    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_OPENCLOSE, ADA_FS_MODE_WRITE,
                               1) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);

    status = chdir(ADA_FS_TEST_DIR);
    ADA_ASSERT_ALWAYS(status == 0);
    ADA_ASSERT_ALWAYS(mkdir( ADA_FS_TEST_FILE_DIR, S_IRWXU | S_IRWXG | S_IRWXO) == 0);
    status = chdir("..");
    ADA_ASSERT_ALWAYS(status == 0);

    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_DIR, ADA_FS_MODE_WRITE,
                               0) == ADA_ERR_FS_IS_A_DIR);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_ERR_FS_NO_OBJECT);

    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_OPENCLOSE, ADA_FS_MODE_WRITE,
                               0, 1) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);
}

/*
 * A read test
 * Open prepared file, read data, compare, close file.
 * 1 - read whole file.
 * 2 - read file from offset ADA_FS_TEST_READ_OFFSET position to the end.
 * 3 - read first ADA_FS_TEST_READ_MAXIMUM_SIZE bytes from file.
 * 4 - read file from offset ADA_FS_TEST_READ_OFFSET position to ADA_FS_TEST_READ_MAXIMUM_SIZE.
 * 5 - try to read ADA_FS_TEST_READ_OVER_SIZE from file.
 * Test passed if the contents of file and expected result are the same.
 */
static void test_read()
{
    AdaFs obj;
    uint8_t *testTable = generateTable(ADA_FS_TEST_TABLE1_SIZE);
    ADA_ASSERT_ALWAYS(testTable != NULL);

    uint8_t readBuf[ADA_FS_TEST_TABLE1_SIZE];
    uint32_t readBufBytes;

    FILE *file;
    ADA_ASSERT_ALWAYS((file = fopen( ADA_FS_TEST_DIR "/" ADA_FS_TEST_FILE_NAME_READ,
                                     ADA_FS_MODE_WRITE ADA_FS_MODE_BINARY)) != NULL);
    ADA_ASSERT_ALWAYS(fwrite(testTable, sizeof(uint8_t), ADA_FS_TEST_TABLE1_SIZE,
                             file) == ADA_FS_TEST_TABLE1_SIZE);
    fclose(file);

    memset(readBuf, 0, ADA_FS_TEST_TABLE1_SIZE);
    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_READ,
                               ADA_FS_MODE_READ ADA_FS_MODE_BINARY, 0) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.read(readBuf, &readBufBytes) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(readBufBytes == ADA_FS_TEST_TABLE1_SIZE);
    ADA_ASSERT_ALWAYS(memcmp(readBuf, testTable, readBufBytes) == 0);

    memset(readBuf, 0, ADA_FS_TEST_TABLE1_SIZE);
    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_READ,
                               ADA_FS_MODE_READ ADA_FS_MODE_BINARY, 0) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.read(readBuf, &readBufBytes, ADA_FS_TEST_READ_OFFSET) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(readBufBytes == ADA_FS_TEST_TABLE1_SIZE - ADA_FS_TEST_READ_OFFSET);
    ADA_ASSERT_ALWAYS(memcmp(readBuf, &testTable[ADA_FS_TEST_READ_OFFSET], readBufBytes) == 0);

    memset(readBuf, 0, ADA_FS_TEST_TABLE1_SIZE);
    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_READ,
                               ADA_FS_MODE_READ ADA_FS_MODE_BINARY, 0) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.read(readBuf, &readBufBytes, 0,
                               ADA_FS_TEST_READ_MAXIMUM_SIZE) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(readBufBytes == ADA_FS_TEST_READ_MAXIMUM_SIZE);
    ADA_ASSERT_ALWAYS(memcmp(readBuf, testTable, readBufBytes) == 0);

    memset(readBuf, 0, ADA_FS_TEST_TABLE1_SIZE);
    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_READ,
                               ADA_FS_MODE_READ ADA_FS_MODE_BINARY, 0) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.read(readBuf, &readBufBytes, ADA_FS_TEST_READ_OFFSET,
                               ADA_FS_TEST_READ_MAXIMUM_SIZE) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(readBufBytes == ADA_FS_TEST_READ_MAXIMUM_SIZE);
    ADA_ASSERT_ALWAYS(memcmp(readBuf, &testTable[ADA_FS_TEST_READ_OFFSET], readBufBytes) == 0);

    memset(readBuf, 0, ADA_FS_TEST_TABLE1_SIZE);
    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_READ,
                               ADA_FS_MODE_READ ADA_FS_MODE_BINARY, 0) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.read(readBuf, &readBufBytes, 0,
                               ADA_FS_TEST_READ_OVER_SIZE) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(readBufBytes == ADA_FS_TEST_TABLE1_SIZE);
    ADA_ASSERT_ALWAYS(memcmp(readBuf, testTable, readBufBytes) == 0);

    free(testTable);
}

/*
 * A write test
 * Open file, write some data, close file.
 * Test passed if the contents of file and expected result are the same.
 */
static void test_write()
{
    AdaFs obj;
    uint8_t *testTable = generateTable(ADA_FS_TEST_TABLE1_SIZE);
    ADA_ASSERT_ALWAYS(testTable != NULL);

    uint8_t readBuf[ADA_FS_TEST_TABLE1_SIZE];

    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_WRITE,
                               ADA_FS_MODE_WRITE ADA_FS_MODE_BINARY, 0) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.write(testTable, ADA_FS_TEST_TABLE1_SIZE) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);

    FILE *file;
    ADA_ASSERT_ALWAYS((file = fopen( ADA_FS_TEST_DIR "/" ADA_FS_TEST_FILE_NAME_WRITE,
                                     ADA_FS_MODE_READ ADA_FS_MODE_BINARY)) != NULL);
    ADA_ASSERT_ALWAYS(fread(readBuf, sizeof(uint8_t), ADA_FS_TEST_TABLE1_SIZE,
                            file) == ADA_FS_TEST_TABLE1_SIZE);
    fclose(file);
    ADA_ASSERT_ALWAYS(memcmp(readBuf, testTable, ADA_FS_TEST_TABLE1_SIZE) == 0);

    free(testTable);
}

/*
 * A fragmented write test
 * Open file, write huge amount of data (split into the portions), close file.
 * Test passed if there are no unexpected errors.
 */
static void test_write_fragmented()
{
    AdaFs obj;
    AdaError res = ADA_SUCCESS;

    uint8_t *testTable = generateTable(ADA_FS_TEST_TABLE1_SIZE);
    ADA_ASSERT_ALWAYS(testTable != NULL);

    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_WRITEFRAGMENTED,
                               ADA_FS_MODE_WRITE ADA_FS_MODE_BINARY, 1) == ADA_SUCCESS);

    for (uint64_t i = 0; (i < ADA_FS_TEST_WRITEFRAGMENTED_LIMIT) && (res != ADA_ERR_FS_MAX_FILES);
         i += ADA_FS_TEST_TABLE1_SIZE)
    {
        res = obj.write(testTable, ADA_FS_TEST_TABLE1_SIZE);
        ADA_ASSERT_ALWAYS((res == ADA_SUCCESS) || (res == ADA_ERR_FS_MAX_FILES));
    }

    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);

    free(testTable);
}

/*
 * A wrong operation sequence test
 * Various tries of wrong usage of this module.
 * Test passed if return results are as expected.
 */
static void test_wrong_ops()
{
    AdaFs obj;

    uint8_t readBuf[ADA_FS_TEST_TABLE1_SIZE];
    uint32_t readBufBytes;

    uint8_t *testTable = generateTable(ADA_FS_TEST_TABLE1_SIZE);
    ADA_ASSERT_ALWAYS(testTable != NULL);

    ADA_ASSERT_ALWAYS(obj.write(testTable, ADA_FS_TEST_TABLE1_SIZE) == ADA_ERR_FS_NO_OBJECT);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_ERR_FS_NO_OBJECT);
    ADA_ASSERT_ALWAYS(obj.write(testTable, ADA_FS_TEST_TABLE1_SIZE) == ADA_ERR_FS_NO_OBJECT);
    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_WRONGOPS, ADA_FS_MODE_WRITE,
                               0) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_WRONGOPS, ADA_FS_MODE_WRITE,
                               0) ==
                      ADA_ERR_FS_OBJECT_EXISTS);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.write(testTable, ADA_FS_TEST_TABLE1_SIZE) == ADA_ERR_FS_NO_OBJECT);

    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_WRONGOPS, ADA_FS_MODE_READ,
                               0) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.write(testTable, ADA_FS_TEST_TABLE1_SIZE) == ADA_ERR_FS_NOT_APPLICABLE);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);

    FILE *file;
    ADA_ASSERT_ALWAYS((file = fopen( ADA_FS_TEST_DIR "/" ADA_FS_TEST_FILE_NAME_WRONGOPS,
                                     ADA_FS_MODE_WRITE ADA_FS_MODE_BINARY)) != NULL);
    ADA_ASSERT_ALWAYS(fwrite(testTable, sizeof(uint8_t), ADA_FS_TEST_TABLE1_SIZE,
                             file) == ADA_FS_TEST_TABLE1_SIZE);
    fclose(file);

    memset(readBuf, 0, ADA_FS_TEST_TABLE1_SIZE);
    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_WRONGOPS,
                               ADA_FS_MODE_READ ADA_FS_MODE_BINARY, 0) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.read(readBuf, &readBufBytes,
                               ADA_FS_TEST_WRONGOPS_READ_OVER_OFFSET) == ADA_ERR_INVALID_PARAM);
    ADA_ASSERT_ALWAYS(obj.read(readBuf, NULL) == ADA_ERR_INVALID_PARAM);
    ADA_ASSERT_ALWAYS(obj.read(NULL, &readBufBytes) == ADA_ERR_INVALID_PARAM);
    ADA_ASSERT_ALWAYS(obj.read(NULL, NULL) == ADA_ERR_INVALID_PARAM);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);

    free(testTable);
}

/*
 * A repeated (multiple) usage test
 * Trying to call any operation many times to provoke any leak errors.
 * Test passed if there are no unexpected errors.
 */
static void test_multiple_ops()
{
    AdaFs obj;

    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_MULTIOPS, ADA_FS_MODE_WRITE,
                               0) == ADA_SUCCESS);
    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_MULTIOPS, ADA_FS_MODE_WRITE,
                               0) ==
                      ADA_ERR_FS_OBJECT_EXISTS);
    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_MULTIOPS, ADA_FS_MODE_WRITE,
                               0) ==
                      ADA_ERR_FS_OBJECT_EXISTS);
    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);
}

/*
 * A writing thread for multithreading test
 */
void *write_thread(void *args)
{
    AdaFs *pObj = (AdaFs*)args;
    uint16_t id;

    pthread_mutex_lock(&runtimeMutex);
    id = threadId;
    threadId++;
    pthread_mutex_unlock(&runtimeMutex);

    printf("Thread %d started\n", id);

    uint32_t msgLen = 20;
    char msg[] = "[00000]Test message\n";
    snprintf(msg, (size_t)msgLen + 1, "[%05d]Test message\n", id);

    ADA_ASSERT_ALWAYS((uint32_t)pObj->write((uint8_t*)msg, msgLen) == ADA_SUCCESS);
    usleep(10);
    ADA_ASSERT_ALWAYS((uint32_t)pObj->write((uint8_t*)msg, msgLen) == ADA_SUCCESS);

    printf("Thread %d finished\n", id);

    pthread_mutex_lock(&runtimeMutex);
    threadBarrier--;
    pthread_mutex_unlock(&runtimeMutex);

    return NULL;
}

/*
 * A multithreading safety test
 * One object is going to be used by many threads in parallel mode
 * Test passed if there are no unexpected errors.
 */
static void test_thread_safety()
{
    AdaFs obj;
    AdaCondVar cndvar;

    ADA_ASSERT_ALWAYS(obj.open(ADA_FS_TEST_DIR, ADA_FS_TEST_FILE_NAME_MULTITHREAD,
                               ADA_FS_MODE_WRITE,
                               0) == ADA_SUCCESS);

    uint16_t i;
    int32_t result_code;
    pthread_t threads[ADA_FS_TEST_THREAD_COUNT];

    for (i = 0; i < ADA_FS_TEST_THREAD_COUNT; i++)
    {
        #ifdef __CRATON__
        threads[i] = (pthread_t)i;
        #else
        threads[i] = i;
        #endif

        result_code = pthread_create(&threads[i], NULL, write_thread, (void*)&obj);
        ADA_ASSERT_ALWAYS(!result_code);
    }

    #ifndef __CRATON__

    for (i = 0; i < ADA_FS_TEST_THREAD_COUNT; i++)
    {
        result_code = pthread_join(threads[i], NULL);
        ADA_ASSERT_ALWAYS(!result_code);
    }

    #else

    while (threadBarrier > 0)
    {
        usleep(10);
    }

    #endif

    ADA_ASSERT_ALWAYS(obj.close() == ADA_SUCCESS);
}

int main(int, const char *[])
{
    sleep(2);
    test_init();
    printf("test_openclose\n");
    test_openclose();
    printf("test_read\n");
    test_read();
    printf("test_write\n");
    test_write();
    printf("test_write_fragmented\n");
    test_write_fragmented();
    printf("test_wrong_ops\n");
    test_wrong_ops();
    printf("test_multiple_ops\n");
    test_multiple_ops();

    printf("test_thread_safety\n");
    test_thread_safety();

    printf("tests finished\n");

    #if ADA_FS_TEST_REMOVE_FILES
    test_cleanup();
    #endif /* ADA_FS_TEST_REMOVE_FILES */

    return 0;
}

/* If we start this test from another thread we assert: obj.open() != ADA_SUCCES.
 * Tell test_main_wrapper to call main() directly from craton_user_init() */
#define ADA_TEST_MAIN_WRAPPER_DIRECT_CALL

#include <platform/unex/test_main_wrapper.h>
ADA_DECLARE_MAIN_TEST_WRAPPER();
