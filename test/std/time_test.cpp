


/*
 *******************************************************************************
 *
 *  @file   time_test.cpp
 *  @author Ivan Tiurin <ivan.tiurin@sredasolutions.com>
 *  @brief  Tests for AdaTime class.
 *
 *******************************************************************************
 */

#include <std/ada_time.h>
#include <cstdio>
#include <unistd.h>
#include <inttypes.h>
#include <time.h>

#include <std/ada_sleep.h>
#include <utils/log/log_modules.h>
#define LOG_MODULE_ID       LOG_MOD_TEST
#define LOG_MODULE_LEVEL    LOG_DEF_LEVEL_TEST
#include <utils/log/log_api.h>

#define TENTHS_SECONDS_IN_HOUR_36_MIN_35_SEC_300_MS (21953)
#define TENTHS_SECONDS_IN_HOUR_59_MIN_59_SEC_900_MS (35999)
#define TENTHS_SECONDS_IN_HOUR_0_MIN_0_SEC_100_MS   (1)
#define TEST_TIMESTAMP_MS                           (1546519510777);   /* 3 JAN 2019 12:45:10:777 */
#define TEST_RES_HOUR_36_MIN_35_SEC_300_MS          (1546522595300)
#define TEST_RES_HOUR_59_MIN_59_SEC_900_MS          (1546520399900)
#define TEST_RES_HOUR_0_MIN_0_SEC_100_MS            (1546520400100)

struct YearTimestamp
{
    uint16_t year;
    uint64_t ms;
};

#define NUM_YEARS       (4)
const static YearTimestamp testTime[] = { {2019, 1575807121333},
                                          {2020, 1607343121333},
                                          {2021, 1638965521333},
                                          {2022, 1670501521333} };

void  testConvertToMs();

/*
 * Time changing after 1ms test.
 */
int main(int, const char *[])
{
    #ifdef __CRATON__
    sleep(5);
    #endif

    uint8_t tstmp1[8];
    uint8_t tstmp2[8];
    AdaTimeStruct tm = {};
    AdaTimeStruct tm2 = {};
    AdaTime::timestampBigEndian(tstmp1);
    AdaTime::getTime(&tm);
    AdaTime::printMs();
    ADA_ASSERT_ALWAYS(tm.min != 0);
    AdaSleep::uSleep(1000);
    AdaTime::getTime(&tm2);
    AdaTime::printMs();
    AdaTime::timestampBigEndian(tstmp2);
    ADA_ASSERT_ALWAYS(tm2.ms != tm.ms);
    ADA_ASSERT_ALWAYS(tstmp2[7] != tstmp1[7]);

    LOG_INFO("Curr time\n S=%" PRIu32 "\nms=%" PRIu64, AdaTime::getS(), AdaTime::getMs());

    testConvertToMs();

    LOG_INFO("\n--test convertToMs(2)--");
    uint64_t timestampMs = TEST_TIMESTAMP_MS;   /* 3 JAN 2019 12:45:10:777 */
    uint16_t tenthsSecInHour = TENTHS_SECONDS_IN_HOUR_36_MIN_35_SEC_300_MS;
    uint64_t timestamp = AdaTime::convertToMs(tenthsSecInHour, timestampMs);
    LOG_INFO("36 min, 35.3 s of current or next hour:");
    LOG_INFO("should be: %" PRIu64, TEST_RES_HOUR_36_MIN_35_SEC_300_MS);
    LOG_INFO("func res : %" PRIu64, timestamp);
    ADA_ASSERT_ALWAYS(timestamp == TEST_RES_HOUR_36_MIN_35_SEC_300_MS);

    tenthsSecInHour = TENTHS_SECONDS_IN_HOUR_59_MIN_59_SEC_900_MS;
    timestamp = AdaTime::convertToMs(tenthsSecInHour, timestampMs);
    LOG_INFO("59 min, 59.9 s of current or next hour:");
    LOG_INFO("should be: %" PRIu64, TEST_RES_HOUR_59_MIN_59_SEC_900_MS);
    LOG_INFO("func res : %" PRIu64, timestamp);
    ADA_ASSERT_ALWAYS(timestamp == TEST_RES_HOUR_59_MIN_59_SEC_900_MS);

    tenthsSecInHour = TENTHS_SECONDS_IN_HOUR_0_MIN_0_SEC_100_MS;
    timestamp = AdaTime::convertToMs(tenthsSecInHour, timestampMs);
    LOG_INFO("0 min, 0.1 s of current or next hour:");
    LOG_INFO("should be: %" PRIu64, TEST_RES_HOUR_0_MIN_0_SEC_100_MS);
    LOG_INFO("func res : %" PRIu64, timestamp);
    ADA_ASSERT_ALWAYS(timestamp == TEST_RES_HOUR_0_MIN_0_SEC_100_MS);

    return 0;
}

void  testConvertToMs()
{
    LOG_INFO("\n--test convertToMs(1)--");
    int32_t moy = 491772;
    uint16_t ms = 1333;
    uint64_t timestamp = AdaTime::convertToMs(moy, ms);
    LOG_INFO("convertToMs = %" PRIu64, timestamp);

    time_t seconds = time(NULL);
    assert(seconds != -1);
    struct tm timeCurrent = {};
    assert(NULL != gmtime_r(&seconds, &timeCurrent));
    uint64_t timestampTest = 0;

    uint32_t testYear = uint32_t(timeCurrent.tm_year + 1900);

    if (timeCurrent.tm_mon == 0)
    {
        testYear--;
    }

    uint8_t idx = 0;

    for (; idx < NUM_YEARS; idx++)
    {
        if (testYear == testTime[idx].year)
        {
            break;
        }
    }

    if (idx != NUM_YEARS)
    {
        timestampTest = testTime[idx].ms;
    }
    else
    {
        fprintf(stderr, "No test for this year !!!");
        return;
    }

    LOG_INFO("test ms     = %" PRIu64, timestampTest);
    assert(timestampTest == timestamp);
}


#include <platform/unex/test_main_wrapper.h>
ADA_DECLARE_MAIN_TEST_WRAPPER();
