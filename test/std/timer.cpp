/**
 *
 *       Copyright 2019 Sreda Software Solutions. All rights reserved.
 *
 *       The copyright notice above does not evidence any actual or
 *       intended publication of such source code. The code contains
 *       Sreda Software Solutions Confidential Proprietary Information.
 *
 */

/*
 *******************************************************************************
 *
 *  @file test/std/timer.cpp
 *  @author Igor Bykov <igor.bykov@sredasolutions.com>
 *
 *  Timer test
 *
 *******************************************************************************
 */

/*#define __CRATON__*/
#ifdef __CRATON__

#include <utils/log/log_modules.h>
#define LOG_MODULE_ID LOG_MOD_TEST
#define LOG_MODULE_LEVEL LOG_DEF_LEVEL_TEST
#include <utils/log/log_api.h>

#include <std/ada_timer.h>
#include <std/ada_sleep.h>
#include <cinttypes>

uint32_t inc = 1;
int32_t count;

void funcCb(void *arg)
{
    count += *reinterpret_cast<uint32_t *>(arg);
}

void testApi()
{
    AdaTimer *pTimer;
    ADA_ASSERT(AdaTimer::create(nullptr, "Test", funcCb, &inc, 0, 100) != ADA_SUCCESS);
    ADA_ASSERT(AdaTimer::create(&pTimer, "Test", nullptr, &inc, 0, 100) != ADA_SUCCESS);
    ADA_ASSERT(AdaTimer::create(&pTimer, "Test", funcCb, &inc, 0, 0) != ADA_SUCCESS);
}

void test()
{
    AdaTimer *pTimer;
    ADA_ASSERT(AdaTimer::create(&pTimer, "Test", funcCb, &inc, 0, 100) == ADA_SUCCESS);

    AdaSleep::sSleep(1);

    pTimer->free();
    ADA_ASSERT((count + 1) / 10 == 1);
}


int main(int, const char *[])
{
    testApi();
    test();

    return 0;
}

#include <platform/unex/test_main_wrapper.h>
ADA_DECLARE_MAIN_TEST_WRAPPER();
#else

int main()
{
    return 0;
}

#endif /* __CRATON__ */
