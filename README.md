-------------------------------------------
C Code Style: https://docs.google.com/document/d/1QEiVkRZX5-mgsST5SAXZvjwH5zqh4P7SQ8M9_I25YWU/preview
Gitlab Guide: https://docs.google.com/document/d/14sOBzf25plXube675B_hRl9rt3Dz8G-MD0nfMx3uusM/preview

How to build
-------------------------------------------

Execute the following commands:

 $ mkdir out
 $ cd out
 $ cmake ..
 $ make

By default Debug configuration is built
        -ggdb3 -O0 flags.

For Release type:

 $ cmake -D CMAKE_BUILD_TYPE=Release ..

Release: -ggdb3 -O1 flags, dynamic asserts.

How to build with UNEX SDK
-------------------------------------------

Execute the following commands:

    $ export UNEX_TOOLCHAIN_PATH=/PATH/TO/gcc-arm-none-eabi-4_8-2014q3-atk-1.0.0
    $ export UNEX_SDK_PATH=/PATH/TO/sdk-4.10.3-sc-i686-linux-gnu
    $ export UNEX_LIB_PATH=/PATH/TO/sdk-eu-1029
    $ mkdir out-unex ; cd out-unex
    $ cmake -C ../build/targets/unex.txt ..
    $ make

Also specify Debug/Release build type if needed.

How to build with CLANG SANITIZERS
-------------------------------------------

    $ mkdir out-sa ; cd out-sa
    $ cmake -C ../build/targets/sa-address.txt ..
    $ make

There are other sanitizers besides "address":
$ ls build/targets/sa-*
build/targets/sa-address.txt
build/targets/sa-thread.txt
build/targets/sa-memory.txt
build/targets/sa-undefined.txt
